package com.example.driverapp.activities.activity.models.response


import com.google.gson.annotations.SerializedName

data class GetDriverDetailsDataModel(
    @SerializedName("data")
    var `data`: Data?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: Int?
) {
    data class Data(
        @SerializedName("adminVerificationStatus")
        var adminVerificationStatus: String?,
        @SerializedName("emirateNumber")
        var emirateNumber: String?,

        @SerializedName("passportNumber")
        var passportNumber: String?,
        @SerializedName("dob")
        var dob: String?,
        @SerializedName("countryCode")
        var countryCode: String?,
        @SerializedName("createdAt")
        var createdAt: String?,
        @SerializedName("deleteStatus")
        var deleteStatus: Boolean?,
        @SerializedName("deviceToken")
        var deviceToken: String?,
        @SerializedName("deviceType")
        var deviceType: String?,
        @SerializedName("documents")
        var documents: List<Any?>?,
        @SerializedName("drivingLicence")
        var drivingLicence: String?,
        @SerializedName("dutyStatus")
        var dutyStatus: Boolean?,
        @SerializedName("email")
        var email: String?,
        @SerializedName("firstName")
        var firstName: String?,
        @SerializedName("username")
        var username: String?,

        @SerializedName("address")
        var address: String?,
        @SerializedName("_id")
        var id: String?,
        @SerializedName("jwtToken")
        var jwtToken: String?,
        @SerializedName("lastName")
        var lastName: String?,
        @SerializedName("latitude")
        var latitude: String?,
        @SerializedName("location")
        var location: Location?,
        @SerializedName("longitude")
        var longitude: String?,
        @SerializedName("manufacturingDate")
        var manufacturingDate: String?,
        @SerializedName("mobileNumber")
        var mobileNumber: String?,
        @SerializedName("nationalIdentity")
        var nationalIdentity: String?,
        @SerializedName("notificationStatus")
        var notificationStatus: Boolean?,
        @SerializedName("policeVerification")
        var policeVerification: Boolean?,
        @SerializedName("profilePic")
        var profilePic: String?,
        @SerializedName("vehicleDocument")
        var vehicleDocument: String?,
        @SerializedName("status")
        var status: String?,
        @SerializedName("trackingStatus")
        var trackingStatus: Boolean?,
        @SerializedName("updatedAt")
        var updatedAt: String?,
        @SerializedName("userType")
        var userType: String?,
        @SerializedName("__v")
        var v: Int?,
        @SerializedName("vehicleBrand")
        var vehicleBrand: String?,
        @SerializedName("vehicleDocuments")
        var vehicleDocuments: List<Any?>?,
        @SerializedName("vehicleLicence")
        var vehicleLicence: String?,
        @SerializedName("vehicleModel")
        var vehicleModel: String?,
        @SerializedName("vehicleNumber")
        var vehicleNumber: String?,
        @SerializedName("vehicleType")
        var vehicleType: String?
    ) {
        data class Location(
            @SerializedName("coordinates")
            var coordinates: List<Double?>?,
            @SerializedName("type")
            var type: String?
        )
    }
}