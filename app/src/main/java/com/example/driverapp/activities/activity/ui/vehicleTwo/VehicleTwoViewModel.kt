package com.example.driverapp.activities.activity.ui.vehicleTwo

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.GetDriverDetailsDataModel
import com.example.driverapp.activities.activity.models.response.updateVehicleDtailsDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Field

class VehicleTwoViewModel : BaseViewModel() {

    var getdriverData = MutableLiveData<GetDriverDetailsDataModel>()
    var updatedriverData = MutableLiveData<updateVehicleDtailsDataModel>()


    fun getDriverDetailsApi(context: Context,token: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getDriverDetails(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    getdriverData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                   ErrorUtil.handlerGeneralError(context, it)
                })
    }

    fun updateVehicleDtailsApi(context: Context,token: String,vehicleType:String,vehicleBrand:String,vehicleModel:String,vehicleNumber:String,manufacturingDate:String,vehicleLicence:String,vehicleDocuments:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.updateVehicleDtails(token,vehicleType,vehicleBrand,vehicleModel,vehicleNumber,manufacturingDate,vehicleLicence,vehicleDocuments)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Utils().dismissDialog(progressDialog)
                updatedriverData.value = it
            }, {
                Utils().dismissDialog(progressDialog)
                ErrorUtil.handlerGeneralError(context, it)
            })
    }
}