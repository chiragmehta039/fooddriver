package com.example.driverapp.activities.activity.ui.adapter

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.models.response.OrderDetailResponse
import com.example.driverapp.activities.activity.models.response.OrderListResponse
import com.example.driverapp.activities.activity.ui.orderDetail.OrderDetailActivity
import kotlinx.android.synthetic.main.layout_navigatecostumer.view.*
import kotlinx.android.synthetic.main.layout_todays.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class NavigateAdapter(var context: Context, var data: java.util.ArrayList<OrderDetailResponse.Data.OrderData.OrderDataX>,var mode:String): RecyclerView.Adapter<NavigateAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_navigatecostumer, parent, false))

    }
    override fun getItemCount(): Int {
        return data.size
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.tv_pro_name.setText(data.get(position).productData.productName)
        holder.itemView.tv_price.setText("AED"+" "+(data.get(position).price*data.get(position).quantity).toString())
        holder.itemView.tv_pro_name.setText(data.get(position).productData.productName)
        holder.itemView.tv_descc.setText(data.get(position).productData.description)

        holder.itemView.tv_desccs.setText(mode)
        if(data!!.get(position)!!.productData.productImage.isEmpty())
        {
            Glide.with(context).load(R.drawable.coming_soon).into(holder.itemView.img_pro)

        }
        else
        {
            Glide.with(context).load(data!!.get(position)!!.productData.productImage).into(holder.itemView.img_pro)

        }







    }


    inner class  MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {



    }



}