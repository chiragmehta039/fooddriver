package com.example.driverapp.activities.activity.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.driverapp.activities.activity.utils.Constants.Companion.PERSISTABLE_PREFRENCE_NAME
import com.example.driverapp.activities.activity.utils.Constants.Companion.PREFRENCE_NAME

class SharedPreferenceUtil
private constructor(val context: Context) {
    val TAG = SharedPreferenceUtil::class.java.simpleName


    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFRENCE_NAME, Context.MODE_PRIVATE)
    private val persistableSharedPreferences: SharedPreferences =
        context.getSharedPreferences(PERSISTABLE_PREFRENCE_NAME, Context.MODE_PRIVATE)

    private val editor: SharedPreferences.Editor = sharedPreferences.edit()
    private val persistableEditor: SharedPreferences.Editor = persistableSharedPreferences.edit()

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var instance: SharedPreferenceUtil? = null

        fun getInstance(ctx: Context): SharedPreferenceUtil {
            if (instance == null) {
                instance = SharedPreferenceUtil(ctx)
            }
            return instance!!
        }
    }

    var isFirstTime: Boolean?
        get() = sharedPreferences["isFirstTime", true]!!
        set(value) = sharedPreferences.set("isFirstTime", value)

    var isLogin: Boolean?
        get() = sharedPreferences["isLogin", false]!!
        set(value) = sharedPreferences.set("isLogin", value)

    var isSound: Boolean?
        get() = sharedPreferences["isSound", true]!!
        set(value) = sharedPreferences.set("isSound", value)

    var isInboxAdd: String?
        get() = sharedPreferences["isInboxAdd", ""]!!
        set(value) = sharedPreferences.set("isInboxAdd", value)

    var isInboxAddRecyle: String?
        get() = sharedPreferences["isInboxAddRecyle", ""]!!
        set(value) = sharedPreferences.set("isInboxAddRecyle", value)

    var selectedLanguage: String
        get() = sharedPreferences["selectedLanguage", ""]!!
        set(value) = sharedPreferences.set("selectedLanguage", value)

    var isEnabled: Boolean?
        get() = sharedPreferences["isEnabled", false]!!
        set(value) = sharedPreferences.set("isEnabled", value)

    var jwtToken: String?
        get() = sharedPreferences["jwtToken", ""]!!
        set(value) = sharedPreferences.set("jwtToken", value)
    var device_id: String?
        get() = sharedPreferences["device_id", ""]!!
        set(value) = sharedPreferences.set("device_id", value)
    var device_token: String?
        get() = sharedPreferences["device_token", ""]!!
        set(value) = sharedPreferences.set("device_token", value)
    var isEmailVerified: Int?
        get() = sharedPreferences["isEmailVerified", 0]!!
        set(value) = sharedPreferences.set("isEmailVerified", value)

    var userId: Int
        get() = sharedPreferences["userId", 0]!!
        set(value) = sharedPreferences.set("userId", value)

    var jordanTaxiUserFlag: Int
        get() = sharedPreferences["jordanTaxiUserFlag", 0]!!
        set(value) = sharedPreferences.set("jordanTaxiUserFlag", value)

    var profileImage: String
        get() = sharedPreferences["profileImage", "null"]!!
        set(value) = sharedPreferences.set("profileImage", value)

    var badgeCount: Int
        get() = sharedPreferences["badgeCount", 0]!!
        set(value) = sharedPreferences.set("badgeCount", value)



    var driving_lisence_back: String
        get() = sharedPreferences["driving_lisence_back", "null"]!!
        set(value) = sharedPreferences.set("driving_lisence_back", value)
    var driving_lisence_front: String
        get() = sharedPreferences["driving_lisence_front", "null"]!!
        set(value) = sharedPreferences.set("driving_lisence_front", value)
    var national_id_back: String
        get() = sharedPreferences["national_id_back", "null"]!!
        set(value) = sharedPreferences.set("national_id_back", value)
    var national_id_front: String
        get() = sharedPreferences["national_id_front", "null"]!!
        set(value) = sharedPreferences.set("national_id_front", value)

    var refer_code: String
        get() = sharedPreferences["refer_code", "null"]!!
        set(value) = sharedPreferences.set("refer_code", value)



    var isEnable: Int
        get() = sharedPreferences["isEnable", 1]!!
        set(value) = sharedPreferences.set("isEnable", value)

    var loginId: Int
        get() = sharedPreferences["loginId", 0]!!
        set(value) = sharedPreferences.set("loginId", value)


    var name: String
        get() = sharedPreferences["name", "null"]!!
        set(value) = sharedPreferences.set("name", value)

    var email: String
        get() = sharedPreferences["email", "null"]!!
        set(value) = sharedPreferences.set("email", value)


    var firstName: String
        get() = sharedPreferences["firstName", "null"]!!
        set(value) = sharedPreferences.set("firstName", value)


    var LastName: String
        get() = sharedPreferences["LastName", "null"]!!
        set(value) = sharedPreferences.set("LastName", value)


    var language: String
        get() = sharedPreferences["language", "1"]!!
        set(value) = sharedPreferences.set("language", value)



    var userMobile: String
        get() = sharedPreferences["userMobile", "null"]!!
        set(value) = sharedPreferences.set("userMobile", value)
    var image: String
        get() = sharedPreferences["image", "null"]!!
        set(value) = sharedPreferences.set("image", value)
    var userCountryCode: String
        get() = sharedPreferences["userCountryCode", "null"]!!
        set(value) = sharedPreferences.set("userCountryCode", value)

    var userEmail: String
        get() = sharedPreferences["userEmail", "null"]!!
        set(value) = sharedPreferences.set("userEmail", value)

    var userFirstName: String
        get() = sharedPreferences["userFirstName", "null"]!!
        set(value) = sharedPreferences.set("userFirstName", value)

    var userLastName: String
        get() = sharedPreferences["userLastName", "null"]!!
        set(value) = sharedPreferences.set("userLastName", value)
    var front_image: String
        get() = sharedPreferences["front_image", "null"]!!
        set(value) = sharedPreferences.set("front_image", value)
    var back_image: String
        get() = sharedPreferences["back_image", "null"]!!
        set(value) = sharedPreferences.set("back_image", value)

    /**
     * puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key]
     */
    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit({ it.putString(key, value) })
            is Int -> edit({ it.putInt(key, value) })
            is Boolean -> edit({ it.putBoolean(key, value) })
            is Float -> edit({ it.putFloat(key, value) })
            is Long -> edit({ it.putLong(key, value) })
            else -> Log.e(TAG, "Setting shared pref failed for key: $key and value: $value ")
        }
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    /**
     * finds value on given key.
     * [T] is the type of value
     * @param defaultValue optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
     */
    inline operator fun <reified T : Any> SharedPreferences.get(
        key: String,
        defaultValue: T? = null
    ): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as? String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    fun deletePreferences() {
        editor.clear()
        editor.apply()
    }

    fun deletePreference() {
        persistableEditor.clear()
        persistableEditor.apply()
    }
}