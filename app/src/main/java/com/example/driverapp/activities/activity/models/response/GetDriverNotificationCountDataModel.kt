package com.example.driverapp.activities.activity.models.response


import com.google.gson.annotations.SerializedName

data class GetDriverNotificationCountDataModel(
    @SerializedName("data")
    var `data`: String?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: Int?
)