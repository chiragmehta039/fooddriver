package com.example.driverapp.activities.activity.utils

import android.R
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.*
import android.content.pm.ResolveInfo
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Parcelable
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.utils.Constants.Companion.EMAIL_PATTERN
import com.example.driverapp.activities.activity.utils.Constants.Companion.PASSWORD_PATTERN
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_vehicle_details.*

import java.io.File
import java.io.FileInputStream
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


val String.isValidEmail: Boolean
    get() = if (this.length < 3 || this.length > 265)
        false
    else {
        this.matches(EMAIL_PATTERN.toRegex())
    }

val String.isNumber: Boolean
    get() =
        try {
            this.toDouble()
            true
        } catch (e: Exception) {
            false
        }

val String.isValidMobile: Boolean
    get() = if (this.length >= 5 && this.length <= 15)
        true
    else {
        false
    }

val String.isValidMobileWithCountryCode: Boolean
    get() = if (this.length >= 7 && this.length <= 16)
        true
    else {
        false
    }


val String.isValidPassword: Boolean
    get() = if (this.length < 8) {
        false
    } else {
        val matcher: Matcher
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(this)
        matcher.matches()
    }

fun EditText.getString(): String {
    return this.text.toString().trim()
}

var toast: Toast? = null
fun AppCompatActivity.showToast(message: String) {
    if (toast != null) toast!!.cancel()
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun AppCompatActivity.getBitmap(image: String): Bitmap {
    val `is`: FileInputStream = openFileInput(image)
    val bmp = BitmapFactory.decodeStream(`is`)
    `is`.close()
    return bmp
}


fun AppCompatActivity.enablewithStatusBarColor() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.statusBarColor = resources.getColor(R.color.white)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }
}


fun isDigits(number: String): Boolean {
    if (!TextUtils.isEmpty(number)) {
        return TextUtils.isDigitsOnly(number.replace("+", ""));
    } else {
        return false;
    }
}

var gson: Gson? = null
fun getGsonInstance(): Gson {

    if (gson == null)
        gson = Gson()
    return gson!!
}


//fun hideKeyboard(context: Context?) {
//    if (context is Activity) {
//        val focusedView = context.currentFocus
//        val inputMethodManager =
//            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputMethodManager.hideSoftInputFromWindow(
//            focusedView?.windowToken,
//            InputMethodManager.HIDE_NOT_ALWAYS
//        )
//    }
//}

fun getOutputFormat(responseDate: String?): String? {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
//            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
//            LocalDate date = LocalDate.parse(responseDate, inputFormatter);
//            String formattedDate = outputFormatter.format(date);
//            return formattedDate;
//        }else {
    val inputFormate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
    val outputFormate = SimpleDateFormat("MMMM,dd-yyyy @ HH:mm", Locale.ENGLISH)
    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return outputFormate.format(date)
    //}
}


fun getDateOutputFormat(responseDate: String?): String? {

    val inputFormate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss a")
    inputFormate.timeZone=TimeZone.getTimeZone("GMT+04:00")
    val outputFormate = SimpleDateFormat("MMMM,dd-yyyy @ HH:mm a", Locale.ENGLISH)
    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return outputFormate.format(date)
}

fun parseDateToddMMyyyy(time: String?): String? {
    //2021-01-19T11:50:30.099Z
    val inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    val outputPattern = "MMMM,dd-yyyy @ h:mma"
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")


    val outputFormat = SimpleDateFormat(outputPattern)
    var str: String? = null
    try {
        str = outputFormat.format(simpleDateFormat.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return str
}



fun dateMissedToddMMyyyy(time: String?): String? {
    //2021-01-19T11:50:30.099Z
    val inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    val outputPattern = "dd-MM-yy @ h:mma"
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")


    val outputFormat = SimpleDateFormat(outputPattern)
    var str: String? = null
    try {
        str = outputFormat.format(simpleDateFormat.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return str
}
fun getDeliverOutputFormat(responseDate: String?): String? {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
//            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
//            LocalDate date = LocalDate.parse(responseDate, inputFormatter);
//            String formattedDate = outputFormatter.format(date);
//            return formattedDate;
//        }else {
    val inputFormate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
    val outputFormate = SimpleDateFormat("MMMM,dd-yyyy @ h:mm", Locale.ENGLISH)
    inputFormate.timeZone=TimeZone.getTimeZone("UTC")
    outputFormate.timeZone=TimeZone.getTimeZone("UTC")

    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return outputFormate.format(date)
    //}
}




fun getDateFormat(responseDate: String?): String? {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
//            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
//            LocalDate date = LocalDate.parse(responseDate, inputFormatter);
//            String formattedDate = outputFormatter.format(date);
//            return formattedDate;
//        }else {
    val inputFormate = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val outputFormate = SimpleDateFormat("MMMM,dd-yyyy", Locale.ENGLISH)
    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return outputFormate.format(date)
    //}
}




fun getTimeStamp(responseDate: String?): Long? {

    val inputFormate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
    inputFormate.timeZone=TimeZone.getTimeZone("UTC")
    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
        return date.time
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null

}



fun getDeliverDateOutputFormat(responseDate: String?): String? {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
//            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
//            LocalDate date = LocalDate.parse(responseDate, inputFormatter);
//            String formattedDate = outputFormatter.format(date);
//            return formattedDate;
//        }else {
    val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    val outputFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
    val inputDateStr = "2013-06-24"
    var date: Date = inputFormat.parse(inputDateStr)
    /*   val outputDateStr: String = outputFormat.format(date)
       val inputFormate = SimpleDateFormat("yyyy-MM-dd ", Locale.ENGLISH)
       val outputFormate = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
       var date: Date? = null*/
    try {
        date = inputFormat.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return outputFormat.format(date)
    //}
}


fun View.setDebounceClickListener(listener: View.OnClickListener, waitMillis: Long = 1000) {
    var lastClickTime = 0L
    setOnClickListener { view ->
        if (System.currentTimeMillis() > lastClickTime + waitMillis) {
            listener.onClick(view)
            lastClickTime = System.currentTimeMillis()
        }
    }
}

fun ImageView.setImageFromDevice(fileUri: String?) {
    fileUri?.let {
        val bmp = BitmapFactory.decodeFile(fileUri);
        setImageBitmap(bmp)
    }
}

fun TextView.setImageFromName(fileUri: String?) {
    fileUri?.let {
        val bmp = BitmapFactory.decodeFile(fileUri);
        setImageFromName(fileUri)
    }
}

fun getPickIntent(context: Context, cameraOutputUri: Uri?, type: String): Intent? {
    val intents = ArrayList<Intent>()
    if (true) {

            when (type) {
                "Image" -> {

                    intents.add( Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
                }
                "Video" -> {
                    intents.add( Intent()
                    )
                }
                "Audio" -> {
                    intents.add( Intent(
                        Intent.ACTION_GET_CONTENT,
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    ))
                }
                else -> {
                    return null
                }


            }

    }

    if (true) setCameraIntents(context, intents, cameraOutputUri,type)


    if (intents.isEmpty()) return null
    val result = Intent.createChooser(intents.removeAt(0), null)
    if (!intents.isEmpty()) result.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(arrayOf<Parcelable>()))
    return result
}

fun setCameraIntents(context: Context, cameraIntents: ArrayList<Intent>, output: Uri?,type: String) {
    var captureIntent:Intent?=null
    when (type){
        "Image" -> {
            captureIntent=  Intent(MediaStore.ACTION_IMAGE_CAPTURE)
           // captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, output)
        }
        "Video" -> {
            captureIntent=Intent(MediaStore.ACTION_VIDEO_CAPTURE)
       captureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 20);
        }
        "Audio" -> {captureIntent=Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION) }
        else -> {
             null
        }

    }

    val packageManager = context.packageManager
    val listCam: List<ResolveInfo> = packageManager.queryIntentActivities(captureIntent!!, 0)
    for (res in listCam) {
        val packageName: String = res.activityInfo.packageName
        val intent = Intent(captureIntent)
        intent.component = ComponentName(res.activityInfo.packageName, res.activityInfo.name)
        intent.setPackage(packageName)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output)
        intent.putExtra("uri", output)
        cameraIntents.add(intent)
    }
}
/*fun setStatusBarGradiant(activity: Activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val window: Window = activity.window
        val background = activity.resources.getDrawable(R.drawable.toolbar_gradient)
        window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        window.setStatusBarColor(activity.resources.getColor(R.color.transparent))
        window.setNavigationBarColor(activity.resources.getColor(R.color.transparent))
        window.setBackgroundDrawable(background)
    }



}*/

/*fun AppCompatActivity.showMenu(array: ArrayList<String>, view: View, listener: CommonListener){
    val popupMenu = ListPopupWindow(this)
    popupMenu.setAdapter(
        ArrayAdapter(
            this,
            R.layout.spinner_dropdown,
            array
        )
    )
    popupMenu.anchorView = view
    popupMenu.verticalOffset = -20
    //popupMenu.setBackgroundDrawable(getDrawable(R.drawable.popup_solid_curve))

    popupMenu.isModal = true
    popupMenu.setOnDismissListener {

    }

    popupMenu.setOnItemClickListener { adapterView, view, i, l ->
        listener.onSelect(array[i], i)
        popupMenu.dismiss()
    }

    popupMenu.show()
}*/


fun getPath(uri: Uri, context: Context): String? {
    val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
    if (isKitKat) {
        // MediaStore (and general)
        return getForApi19(uri, context)
    } else if ("content".equals(uri.getScheme(), ignoreCase = true)) {

        // Return the remote address
        return if (isGooglePhotosUri(uri)) uri.getLastPathSegment() else getDataColumn(
            uri,
            null,
            null,
            context
        )
    } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
        return uri.getPath()
    }
    return null
}

@TargetApi(19)
private fun getForApi19(uri: Uri, context: Context): String? {
    //Log.e(tag, "+++ API 19 URI :: $uri")
    if (DocumentsContract.isDocumentUri(context, uri)) {
        // Log.e(tag, "+++ Document URI")
        // ExternalStorageProvider
        if (isExternalStorageDocument(uri)) {
            // Log.e(tag, "+++ External Document URI")
            val docId: String = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).toTypedArray()
            val type = split[0]
            if ("primary".equals(type, ignoreCase = true)) {
                // Log.e(tag, "+++ Primary External Document URI")
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            }

            // TODO handle non-primary volumes
        } else if (isDownloadsDocument(uri)) {
            //  Log.e(tag, "+++ Downloads External Document URI")
            val id: String = DocumentsContract.getDocumentId(uri)
            val contentUri: Uri = ContentUris.withAppendedId(
                Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
            )
            return getDataColumn(contentUri, null, null, context)
        } else if (isMediaDocument(uri)) {
            //  Log.e(tag, "+++ Media Document URI")
            val docId: String = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).toTypedArray()
            val type = split[0]
            var contentUri: Uri? = null
            if ("image" == type) {
                // Log.e(tag, "+++ Image Media Document URI")
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            } else if ("video" == type) {
                // Log.e(tag, "+++ Video Media Document URI")
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            } else if ("audio" == type) {
                // Log.e(tag, "+++ Audio Media Document URI")
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            }
            val selection = "_id=?"
            val selectionArgs = arrayOf(
                split[1]
            )
            return getDataColumn(contentUri, selection, selectionArgs, context)
        }
    } else if ("content".equals(uri.getScheme(), ignoreCase = true)) {
        //  Log.e(tag, "+++ No DOCUMENT URI :: CONTENT ")

        // Return the remote address
        return if (isGooglePhotosUri(uri)) uri.getLastPathSegment() else getDataColumn(
            uri,
            null,
            null,
            context
        )
    } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
        //  Log.e(tag, "+++ No DOCUMENT URI :: FILE ")
        return uri.getPath()
    }
    return null
}
fun containsIgnoreCase(str: String?, searchStr: String?): Boolean {
    if (str == null || searchStr == null) return false

    val length = searchStr.length
    if (length == 0)
        return true

    for (i in str.length - length downTo 0) {
        if (str.regionMatches(i, searchStr, 0, length, ignoreCase = true))
            return true
    }
    return false
}

/**
 * Get the value of the data column for this Uri. This is useful for
 * MediaStore Uris, and other file-based ContentProviders.
 *
 * @param uri The Uri to query.
 * @param selection (Optional) Filter used in the query.
 * @param selectionArgs (Optional) Selection arguments used in the query.
 * @return The value of the _data column, which is typically a file path.
 */
fun getDataColumn(
    uri: Uri?, selection: String?,
    selectionArgs: Array<String>?,
    context: Context
): String? {
    var cursor: Cursor? = null
    val column = "_data"
    val projection = arrayOf(
        column
    )
    try {
        cursor = uri?.let {
            context.getContentResolver().query(
                it, projection, selection, selectionArgs,
                null
            )
        }
        if (cursor != null && cursor.moveToFirst()) {
            val index: Int = cursor.getColumnIndexOrThrow(column)
            return cursor.getString(index)
        }
    } finally {
        if (cursor != null) cursor.close()
    }
    return null
}


/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is ExternalStorageProvider.
 */
fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.getAuthority()
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is DownloadsProvider.
 */
fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.getAuthority()
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is MediaProvider.
 */
fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.getAuthority()
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is Google Photos.
 */
fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.getAuthority()
}

fun loadBitmapFromView(v: View, v2: View): Bitmap? {
    val maxWidth = 0
    val maxHeight = 0
    // v.measure(View.MeasureSpec.makeMeasureSpec(maxWidth, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(maxHeight, View.MeasureSpec.AT_MOST))
    v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
    val b = Bitmap.createBitmap(
        v.measuredWidth,
        v.measuredHeight,
        Bitmap.Config.ARGB_8888
    )
    val c = Canvas(b)
    //v.layout(v.left, v.top, v.right, v.bottom)
    v.draw(c)
    return b
}


fun getRealPathFromURI(contentURI: String?, activity: Activity): String? {
    val contentUri = Uri.parse(contentURI)
    val cursor: Cursor = activity.getContentResolver().query(contentUri, null, null, null, null)!!
    return if (cursor == null) {
        contentUri.path
    } else {
        cursor.moveToFirst()
        val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        if (index > 0) {
            cursor.getString(index)
        } else getWorkingDirectory(activity).toString() + "/" + cursor.getString(0)
    }
}

fun getWorkingDirectory(activity: Activity): File? {
    val directory =
        File(Environment.getExternalStorageDirectory(), activity.applicationContext.packageName)
    if (!directory.exists()) {
        directory.mkdir()
    }
    return directory
}

/*fun dialogCalendar(){
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)



    val dpd = DatePickerDialog(, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        // Display Selected date in textbox
        dateTv.setText("" + dayOfMonth + "/ " + monthOfYear + "/ " + year)
        // dateTv.setText("" + dayOfMonth + " " + MONTHS[monthOfYear] + ", " + year)

    }, year, month, day)

    dpd.show()
}*/




fun backAlert(activity: Activity) {

    val builder = AlertDialog.Builder(activity)

    with(builder)
    {

        setMessage(
            "Are you sure? \n" +
                    "You may lose all your saved changes.\n"
        )
        setPositiveButton("Yes", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                // prefs.isLogin = false
                //    com.e.quizlok.utils.ProgressDialogUtils.getInstance().showProgress(context,true)
                // viewModel.logOut(Authorization = com.e.quizlok.utils.SharedPreferenceUtil.getInstance(context).jwtToken!!)
                activity.onBackPressed()
            }

        })


        setNegativeButton("No", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {

            }
        })

        show()
    }


}












