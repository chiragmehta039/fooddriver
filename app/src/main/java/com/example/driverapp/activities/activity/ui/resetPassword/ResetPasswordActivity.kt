package com.example.driverapp.activities.activity.ui.resetPassword

import Utils
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.forgotPassword.ForgotPasswordViewModel
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.utils.Constants
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.activity_reset_password.backIv
import kotlinx.android.synthetic.main.activity_reset_password.nextTv
import kotlinx.android.synthetic.main.activity_reset_password.passwordd
import kotlinx.android.synthetic.main.activity_reset_password.titleTv
import kotlinx.android.synthetic.main.activity_set_password.*

class ResetPasswordActivity : BaseActivity(), View.OnClickListener{
    lateinit var forgotPasswordViewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        init()
        initControl()
    }


    override fun init() {
        forgotPasswordViewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)

        forgotPasswordViewModel.forgotcheckData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                showToast("Password reset successfully!")
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        nextTv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv -> {
                onBackPressed()
            }
            R.id.nextTv -> {
                if (confirmPasswordd.text.toString().trim().isEmpty() || passwordd.text.toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please fill all details!", 1, false)
                } else if (passwordd.text.toString().length < 6)
                    Utils().getMessageDialog(this, "Password must contain minimum 6 characters and maximum 16 characters", 1, false)
                else if (passwordd.text.toString().length > 16)
                    Utils().getMessageDialog(this, "Password must contain minimum 6 characters and maximum 16 characters", 1, false)
                else if (passwordd.text.toString().trim() != confirmPasswordd.text.toString().trim()) {
                    Utils().getMessageDialog(this, "password and Confirm password must be same!", 1, false)
                } else {
                        forgotPasswordViewModel.driverForgotPasswordApi(this, intent.getStringExtra("driverid")!!, passwordd.text.toString().trim())
                }
            }
        }
    }

}