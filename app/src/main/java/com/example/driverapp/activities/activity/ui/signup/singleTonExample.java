package com.example.driverapp.activities.activity.ui.signup;

import java.security.Identity;

public class singleTonExample {
   String name;
   String email;
   String countrycodepicker;
   String phone;
   String datetv1;
   private static final singleTonExample ourInstance = new singleTonExample();
   public static singleTonExample getInstance() {
      return ourInstance;
   }
   private singleTonExample() { }
   public void setName(String name) {
      this.name = name;
   }
   public String getName() {
      return name;
   }

   public void setEmail(String email) {
      this.email = email;
   }
   public String getEmail() {
      return email;
   }

   public void setCountrycodepicker(String countrycodepicker) {
      this.countrycodepicker = countrycodepicker;
   }
   public String getCountrycodepicker() {
      return countrycodepicker;
   }

   public void setPhone(String phone) {
      this.phone = phone;
   }
   public String getPhone() {
      return phone;
   }


   public void setDatetv1(String datetv1) {
      this.datetv1 = datetv1;
   }
   public String getDatetv1() {
      return datetv1;
   }
}