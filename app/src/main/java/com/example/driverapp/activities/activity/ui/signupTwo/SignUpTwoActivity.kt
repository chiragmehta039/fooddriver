package com.example.driverapp.activities.activity.ui.signupTwo

import Utils
import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.otp.OtpActivity
import com.example.driverapp.activities.activity.ui.signup.SignupActivity
import com.example.driverapp.activities.activity.ui.signup.SignupViewModel
import com.example.driverapp.activities.activity.ui.signup.singleTonExample
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.FilePath
import com.example.driverapp.activities.activity.utils.GetPath
import com.example.driverapp.activities.activity.utils.S3Bucket
import com.rento.imagepicker.FilePickUtils
import kotlinx.android.synthetic.main.activity_sign_up_two.*
import kotlinx.android.synthetic.main.activity_sign_up_two.backIv
import kotlinx.android.synthetic.main.activity_sign_up_two.chooseFile
import kotlinx.android.synthetic.main.activity_sign_up_two.nextTv
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_sign_up_two.*
import kotlinx.android.synthetic.main.activity_sign_up_two.*
import java.io.File
import java.util.*

class SignUpTwoActivity : BaseActivity() , View.OnClickListener{
    lateinit var signupViewModel: SignupViewModel
    private val PICK_PDF_REQUEST = 1
    private val STORAGE_PERMISSION_CODE = 123
    private var filePath: String? = null
    private var path: String? = null


    private val onFileChoose = FilePickUtils.OnFileChoose { fileUri, requestCode ->
        imageFilePath = fileUri
    }
    var filePickUtils = FilePickUtils(this, onFileChoose)
    var lifeCycleCallBackManager = filePickUtils.callBackManager
    companion object {
        var tv: TextView? = null
    }
    private var imageFilePath: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_two)
        init()
        initControl()


        getSharedPreferences(Constants.DM,MODE_PRIVATE).getString(Constants.DM, "")


        chooseFile.setOnClickListener(View.OnClickListener {
            requestStoragePermission()

        })
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onBackPressed() {
        getSharedPreferences(Constants.DLICENCE, MODE_PRIVATE).edit().putString(Constants.DLICENCE, drivinglicence.text.toString()).apply()
        getSharedPreferences(Constants.DN, MODE_PRIVATE).edit().putString(Constants.DN,nationalidentity.text.toString()).apply()
        getSharedPreferences(Constants.DM, MODE_PRIVATE).edit().putString(Constants.DM, nationalidentity1.text.toString()).apply()
        val singletonexample: singleTonExample =
            singleTonExample.getInstance()
        startActivity(
                Intent(this, SignupActivity::class.java)

                        .putExtra("name", singletonexample.name)
                        .putExtra("email", singletonexample.email)
                        .putExtra("phone", singletonexample.phone)
                        .putExtra("datetv1", singletonexample.datetv1)
                        .putExtra("switch", intent.getStringExtra("1"))
                        .putExtra("countrycodepicker",singletonexample.countrycodepicker)


        )

    }


    override fun init() {
        signupViewModel = ViewModelProvider(this).get(SignupViewModel::class.java)

        signupViewModel.signUpDataCheck.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                val singletonexample: singleTonExample =
                    singleTonExample.getInstance()
                val pathList = ArrayList<String>()
                pathList.add(File(filePath).absolutePath)
                S3Bucket(this, pathList, true) { uploadedUrl ->
                startActivity(Intent(this, OtpActivity::class.java)

                    .putExtra("name", singletonexample.name)
                    .putExtra("email", singletonexample.email)
                    .putExtra("phone", singletonexample.phone)
                    .putExtra("datetv1", singletonexample.datetv1)
                    .putExtra("switch", intent.getStringExtra("1"))
                    .putExtra("countrycodepicker",singletonexample.countrycodepicker)
                        .putExtra("drivinglisence", drivinglicence.text.toString().trim())
                        .putExtra("nationalidentity", nationalidentity.text.toString().trim())
                        .putExtra("passport", nationalidentity1.text.toString().trim())

                        .putExtra("policeverification", if (radioButton.isChecked) "false" else "true")
                        .putExtra("choosefile", TextUtils.join(",", uploadedUrl!!)))
            }
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        calendars.setOnClickListener(this)
        nextTv.setOnClickListener(this)
        consDocuments1.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv->{
                onBackPressed()
            }
            R.id.calendars->
            {
                dialogCalendar()
            }
            R.id.nextTv-> {
                if (drivinglicence.getText().toString().trim().isEmpty()) {
                    Utils().getMessageDialog(
                        this,
                        "Please enter the driver license number!",
                        0,
                        false
                    )
                } else if (nationalidentity.getText().toString().isEmpty()) {
                    Utils().getMessageDialog(
                        this,
                        "Please enter  emirate id no.!",
                        0,
                        false
                    )
                }
                else if (nationalidentity1.getText().toString().isEmpty()) {
                    Utils().getMessageDialog(
                            this,
                            "Please enter passport no.!",
                            0,
                            false
                    )
                }




                else if (filePath==null) {
                    Utils().getMessageDialog(this, "Please select the document!", 0, false)
                } else {
                    val singletonexample: singleTonExample =
                        singleTonExample.getInstance()
                    signupViewModel.checkDriverMobileAndEmailApi(this,
                        singletonexample.phone,
                        singletonexample.email
                    )
                }
            }

            R.id.consDocuments1->{
                requestStoragePermission()
            }
        }
    }


    fun dialogCalendar(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
            manudate.setText("" + dayOfMonth + "/ " + monthOfYear + "/ " + year)
            // dateTv.setText("" + dayOfMonth + " " + MONTHS[monthOfYear] + ", " + year)

        }, year, month, day)

        dpd.show()
    }



    private fun requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            showFileChooser()
        }
        else  {
            ActivityCompat.requestPermissions(
                this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    STORAGE_PERMISSION_CODE
            )
        }
    }


    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("*/*");
        startActivityForResult(intent, PICK_PDF_REQUEST)
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            filePath = GetPath.getPath(this, Uri.parse(data.getDataString()))
            Log.e("Uri",""+data.data);
            chooseFile.setText(File(filePath).name)
        }else {
            if (lifeCycleCallBackManager != null) {
                lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}