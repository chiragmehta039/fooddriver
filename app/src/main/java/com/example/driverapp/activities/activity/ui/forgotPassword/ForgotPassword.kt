package com.example.driverapp.activities.activity.ui.forgotPassword

import Utils
import android.content.Intent
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.models.response.CheckDriverMobileAndEmailDataModel
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.otp.OtpActivity
import com.example.driverapp.activities.activity.ui.signup.SignupViewModel
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.backIv
import kotlinx.android.synthetic.main.activity_forgot_password.nextTv
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.countryCodePicker
import kotlinx.android.synthetic.main.activity_forgot_password.tiePhoneNo
import kotlinx.android.synthetic.main.activity_forgot_password.tilPhoneNo
import kotlinx.android.synthetic.main.activity_signup.*
import me.ibrahimsn.lib.PhoneNumberKit

class ForgotPassword: BaseActivity(),View.OnClickListener{
    lateinit var forgotPasswordViewModel: ForgotPasswordViewModel

    var keyDel=0
    var phoneNumber : PhoneNumberFormattingTextWatcher?=null
    var watcher: TextWatcher ?=null
    var isListnerAdded :Boolean=false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        val phoneNumberKit = PhoneNumberKit(this)
        phoneNumberKit.attachToInput(tilPhoneNo, countryCodePicker.selectedCountryCodeAsInt)
        phoneNumberKit.setupCountryPicker(this as AppCompatActivity, searchEnabled = true)
        init()
        initControl()

    }
    fun mobileNo(mobileNo: TextInputEditText, countryCode: String) : String? {
        return mobileNo.text?.replace("[^\\d]".toRegex(), "")?.replace(countryCode.replace("+",""),"")
    }
    override fun init() {
        forgotPasswordViewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        phoneNumber= PhoneNumberFormattingTextWatcher()

        forgotPasswordViewModel.forgotData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                startActivity(Intent(this, OtpActivity::class.java).putExtra("forgot_key", true)
                        .putExtra("countrycodepicker",countryCodePicker.selectedCountryCodeWithPlus)
                        .putExtra("phone",mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?: "")
                        .putExtra("driverid",it.data!!.userId))
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun onResume() {
        super.onResume()


    }



    fun mobileNo(mobileNo: CharSequence) : String? {
        return mobileNo.toString().replace("[^\\d]".toRegex(), "")
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        nextTv.setOnClickListener(this)

//        watcher = object : TextWatcher {
//            override fun afterTextChanged(s: Editable) {
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                if(s.length>0){
//                    try{
//                        mobileNo(s)?:"".toLong()
//                        phoneforgot.removeTextChangedListener(watcher)
//                        if(!isListnerAdded) {
//                            isListnerAdded = true
//                            phoneforgot.addTextChangedListener(phoneNumber)
//                        }
//
//                    }catch (e:java.lang.Exception){
//                    }
//
//                }
//
//            }
//        }
//        phoneforgot.addTextChangedListener(watcher)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv -> {
                onBackPressed()
            }

            R.id.nextTv -> {

                 if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.isEmpty()!!){
                    Utils().getMessageDialog(this, "Please enter the mobile number!", 0, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0<7){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0>15){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }

                else {
            forgotPasswordViewModel.checkDriverMobilForForgotPasswordApi(this,countryCodePicker.selectedCountryCodeWithPlus,mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "")
                }
            }
        }
    }
}