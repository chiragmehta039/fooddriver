package com.example.driverapp.activities.activity.utils

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import com.example.driverapp.R

class ProgressDialogUtils {

    public var mDialog: Dialog? = null
    private var dialog: ProgressDialog ?=null
    private var c : Context? =null
    companion object {
        var progressDialog: ProgressDialogUtils? = null
        fun getInstance(): ProgressDialogUtils {
            if (progressDialog == null) {
                progressDialog = ProgressDialogUtils()
            }
            return progressDialog as ProgressDialogUtils
        }
    }

    fun showProgress(context: Context,  cancelable: Boolean) {
//        try {
//            if (mDialog == null) {
        if(mDialog==null||!mDialog!!.isShowing){
            if (mDialog == null||context != c) {
                c=context
                mDialog = Dialog(context)
                mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
               // mDialog?.setContentView(R.layout.custom_progress_dialog)
                mDialog?.setCancelable(cancelable)
                mDialog?.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                mDialog?.setCanceledOnTouchOutside(cancelable)
            }

            mDialog?.show()
        }


//            } else {
//                mDialog?.setCancelable(cancelable)
//                mDialog!!.show()
//            }
//        } catch (e: Exception){
//            Log.e("MyExceptions", e.message)
//        }
    }

    fun hideProgress() {
        if (mDialog != null&& mDialog!!.isShowing) {
            mDialog?.dismiss()
            mDialog = null
        }
    }

    fun dispose() {
        if (progressDialog != null) {
            progressDialog == null
        }
    }
}