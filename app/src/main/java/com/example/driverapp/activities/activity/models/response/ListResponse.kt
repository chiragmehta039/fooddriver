package com.example.driverapp.activities.activity.models.response

data class ListResponse(
    val `data`: List<Data>,
    val message: String,
    val status: Int
)
{
    data class Data(
            val _id: String,
            val addNote: String,
            val address: String,
            val addressType: String,
            val areaId: String,
            val brandData: BrandData,
            val brandId: String,
            val buildingAndApart: String,
            val createdAt: String,
            val currency: String,
            val deliveryCharge: Int,
            val deliveryDate: String,
            val discountAmount: Int,
            val driverId: String,
            val excepetdDeliveryTime: Int,
            val landmark: String,
            val latitude: String,
            val longitude: String,
            val noCutlery: Boolean,
            val offerAmount: Int,
            val orderData: List<OrderData>,
            val orderNumber: String,
            val paymentAmount: Int,
            val paymentMode: String,
            val promocode: String,
            val restaurantData: RestaurantData,
            val restaurantId: String,
            val serviceType: String,
            val status: String,
            val subTotal: Int,
            val total: Int,
            val userData: UserData,
            val userId: String
    )
    data class OrderData(
            val _id: String,
            val actualAmount: Int,
            val amountWithQuantuty: Int,
            val choiceAmount: Int,
            val extra: List<com.example.driverapp.activities.activity.models.response.Extra>,
            val price: Int,
            val productData: com.example.driverapp.activities.activity.models.response.ProductData,
            val productId: String,
            val quantity: Int
    )

    data class RestaurantData(
            val address: String,
            val branchNameEn: String,
            val countryCode: String,
            val image: String,
            val latitude: String,
            val longitude: String,
            val mobileNumber: String
    )

    data class UserData(
            val countryCode: String,
            val firstName: String,
            val lastName: String,
            val mobileNumber: String,
            val profilePic: String
    )

    data class CuisineArrayEn(
            val item_id: Int,
            val item_text: String
    )

    data class Extra(
            val categoryId: String,
            val choiceId: String,
            val name: String,
            val price: Int,
            val quantity: Int
    )

    data class ProductData(
            val __v: Int,
            val _id: String,
            val adminVerifyStatus: String,
            val availabilityStatus: Boolean,
            val avgRating: Int,
            val branchId: String,
            val brandId: String,
            val category: List<com.example.driverapp.activities.activity.models.response.Category>,
            val changeRequest: Boolean,
            val changeRequestApporve: String,
            val changeRequestId: String,
            val changeRequestMsg: String,
            val createdAt: String,
            val deleteStatus: Boolean,
            val description: String,
            val descriptionAr: String,
            val dietary: List<com.example.driverapp.activities.activity.models.response.Dietary>,
            val index: Int,
            val menuAssignStatus: Boolean,
            val menuCategoryName: String,
            val menuId: String,
            val price: Int,
            val priceSelection: Boolean,
            val productImage: String,
            val productName: String,
            val productNameAr: String,
            val ratingByUsers: Int,
            val showDietary: List<String>,
            val status: String,
            val statusEnable: Boolean,
            val totalOrder: Int,
            val totalRating: Int,
            val type: String,
            val updatedAt: String
    )

    data class Category(
            val _id: String,
            val category: List<CategoryX>,
            val categoryNameAr: String,
            val categoryNameEn: String,
            val choice: List<ChoiceX>,
            val max: Int,
            val min: Int
    )

    data class Dietary(
            val _id: String,
            val name: String
    )

    data class CategoryX(
            val categoryNameAr: String,
            val categoryNameEn: String,
            val choice: List<com.example.driverapp.activities.activity.models.response.Choice>
    )

    data class ChoiceX(
            val _id: String,
            val name: String,
            val price: Int,
            val status: Boolean
    )

    data class Choice(
            val name: String,
            val price: Int
    )
    data class BrandData(
            val _id: String,
            val cuisineArrayEn: List<CuisineArrayEn>,
            val latitude: String,
            val logo: String,
            val longitude: String,
            val storeName: String
    )

}


