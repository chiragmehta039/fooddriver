package com.example.driverapp.activities.activity.ui.profile

import Utils
import Utils.Companion.checkMailAddress
import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.HomeMapsActivity
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.otp.OtpActivity
import com.example.driverapp.activities.activity.ui.vehicleTwo.VehicleTwoViewModel
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.IMAGE
import com.example.driverapp.activities.activity.utils.S3Bucket
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.icontractor.location.LocationClass
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.editTv
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile_two.*
import kotlinx.android.synthetic.main.activity_profile_two.backIv
import kotlinx.android.synthetic.main.activity_profile_two.countryCodePicker
import kotlinx.android.synthetic.main.activity_profile_two.manudate
import kotlinx.android.synthetic.main.activity_profile_two.nationalidentity
import kotlinx.android.synthetic.main.activity_profile_two.nationalidentity1
import kotlinx.android.synthetic.main.activity_profile_two.nextTv
import kotlinx.android.synthetic.main.activity_profile_two.*
import kotlinx.android.synthetic.main.activity_profile_two.name
import kotlinx.android.synthetic.main.activity_profile_two.tiePhoneNo
import kotlinx.android.synthetic.main.activity_profile_two.tilPhoneNo
import kotlinx.android.synthetic.main.activity_signup.*
import me.ibrahimsn.lib.PhoneNumberKit
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class ProfileTwoActivity : BaseActivity(), View.OnClickListener,HomeActivity.UploadedListener,DatePickerDialog.OnDateSetListener {
    private lateinit var genderList: ArrayList<String>
    lateinit var vehicleTwoViewModel: VehicleTwoViewModel
    lateinit var proflieViewModel: ProflieViewModel
    var phoneNumber : PhoneNumberFormattingTextWatcher?=null
    var watcher: TextWatcher ?=null
    var isListnerAdded :Boolean=false
    private var mobile = ""
    private var locationClass: LocationClass? = null
    private var file: File? = null
    private var captureMediaFile: File? = null
    private var img:String? = null
    private val PERMISSION_CODE = 12
    private val CAMERA_REQUEST = 100
    private val GALLERY_PICTURE = 101
    private val ADDRESS_PICK = 112
    private val OTP_CHECK = 118
    private var lat =""
    private var log =""
    private var cityName =""
    var keyDel=0
    private val date: Date = Calendar.getInstance().time


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_two)
        init()
        initControl()
        val phoneNumberKit = PhoneNumberKit(this)
        phoneNumberKit.attachToInput(tilPhoneNo, countryCodePicker.selectedCountryCodeAsInt)
        phoneNumberKit.setupCountryPicker(this as AppCompatActivity, searchEnabled = true)
       // tiePhoneNo.addTextChangedListener { tilPhoneNo.isErrorEnabled=false }

        consManufacturing.setOnClickListener(View.OnClickListener {
            dialogCalendar()

        })

    }


    override fun init() {
        //        detectLocationTextView=findViewById(R.id.detectLocationTextView);
        locationClass = LocationClass(this)
        phoneNumber=PhoneNumberFormattingTextWatcher()

        myprofile()
        vehicleTwoViewModel = ViewModelProvider(this).get(VehicleTwoViewModel::class.java)
        proflieViewModel = ViewModelProvider(this).get(ProflieViewModel::class.java)

        vehicleTwoViewModel.getDriverDetailsApi(this, getSharedPreferences(Constants.BITEME,MODE_PRIVATE).getString(Constants.TOKEN, "")!!)

        vehicleTwoViewModel.getdriverData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(IMAGE, it.data!!.profilePic).apply()
                mobile = it.data!!.mobileNumber!!
                manudate.setText(it.data!!.dob)
                fullname.setText(it.data!!.firstName)
                name.setText("@"+it.data!!.username)
                profileemail.setText(it.data!!.email)
                tiePhoneNo.setText(it.data!!.countryCode+it.data!!.mobileNumber)
                nationalidentity.setText(it.data!!.emirateNumber)
                nationalidentity1.setText(it.data!!.passportNumber)

                countryCodePicker.setCountryForPhoneCode(Integer.parseInt(it.data!!.countryCode))
                Glide.with(this).load(it.data!!.profilePic).placeholder(R.drawable.profilebig).into(imageView23)
                img = it.data!!.profilePic
                if(it.data!!.address == null) {
                    val geocoder: Geocoder
                    val addresses: List<Address>
                    geocoder = Geocoder(this, Locale.getDefault())
                    addresses = geocoder.getFromLocation(locationClass!!.latitude, locationClass!!.longitude, 1)
                    val address: String = addresses[0].getAddressLine(0)
                    profileaddress.setText(address)
                } else {
                    profileaddress.setText(it.data!!.address)
                }
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

        proflieViewModel.updateprofileData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(IMAGE, it.data!!.profilePic).apply()
                showToast(it.message!!)
//                Utils().getMessageDialog(this, it.message!!, 0, false)
                myprofile()
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

        proflieViewModel.checkmobileData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                val intent = Intent(this, OtpActivity::class.java)
                intent.putExtra("profile", true)
                        .putExtra("countrycodepicker", countryCodePicker.selectedCountryCodeWithPlus)
                        .putExtra("phone", mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "")
                startActivityForResult(intent, OTP_CHECK)
//                startActivity(
//                         Intent(this, OtpActivity::class.java)
//                        .putExtra("profile", true)
//                        .putExtra("name", name.text.toString().trim())
//                        .putExtra("fullname", fullname.text.toString().trim())
//                        .putExtra("countrycodepicker",countryCodePicker.selectedCountryCodeWithPlus)
//                        .putExtra("phone",profilephone.text.toString().trim())
//                        .putExtra("email",profileemail.text.toString().trim())
//                        .putExtra("lat",lat)
//                        .putExtra("log",log)
//                        .putExtra("address",profileaddress.text.toString().trim())
//                        .putExtra("img",img)
//                        .putExtra("file",file))
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        editTv.setOnClickListener(this)
        nextTv.setOnClickListener(this)
        backIv.setOnClickListener(this)
        calendars.setOnClickListener(this)
        imageView25.setOnClickListener(this)
        editTv2.setOnClickListener(this)
        profileaddress.setOnClickListener(this)

        reationSpinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                if (i != 0) {
                    editTv2.setText(genderList.get(i))
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

//        watcher = object : TextWatcher {
//            override fun afterTextChanged(s: Editable) {
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                if(s.length>0){
//                    try{
//                        mobileNo(s)?:"".toLong()
//                        profilephone.removeTextChangedListener(watcher)
//                        if(!isListnerAdded) {
//                            isListnerAdded = true
//                            profilephone.addTextChangedListener(phoneNumber)
//                        }
//
//                    }catch (e:java.lang.Exception){
//                    }
//
//                }
//
//            }
//        }
//        profilephone.addTextChangedListener(watcher)
    }


    override fun onResume() {
        super.onResume()
//        profilephone.addTextChangedListener(object : TextWatcher {
//
//
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                profilephone.setOnKeyListener(object : View.OnKeyListener{
//                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
//                        if (keyCode == KeyEvent.KEYCODE_DEL) {
//                            keyDel = 1;
//                        }
//                        return false;
//                    }
//
//
//                })
//                if(profilephone.text.length<2)
//                {
//                    keyDel=0
//                }
//                try{
//
//                    profilephone.text.toString().toLong()
//                    var cou=   countryCodePicker.selectedCountryCodeWithPlus
//                    if(cou.equals("+91"))
//                    {
//                        if(keyDel==0) {
//
//
//                            if (profilephone.text?.length == 3 ) {
//
//
//                                profilephone.setText(profilephone.text.toString() + "-")
//
//                                profilephone.setSelection(profilephone.text.length)
//                                keyDel=1
//
//
//                            }
//
//
//
//
//
//                        }
//                    }
//                    else
//                    {
//                        profilephone.text.toString().toLong()
//                        if(keyDel==0) {
//
//
//                            if (profilephone.text?.length == 3 ) {
//
//
//                                profilephone.setText(profilephone.text.toString() + "-")
//
//                                profilephone.setSelection(profilephone.text.length)
//                                keyDel=1
//
//
//                            }
//
//
//
//                        }
//
//                    }
//
//                } catch (e : Exception){
//
//                }
//
//            }
//
//            override fun afterTextChanged(arg0: Editable) {
//            }
//
//            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
//            }
//        })

    }



    fun dialogCalendar(){

        val dialog = DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, setFormat("yyyy",date), setFormat("MM",date) - 1, setFormat("dd",date))
        dialog.datePicker.maxDate = Date().time
        dialog.show()



    }
    private fun setFormat(key:String, date:Date): Int{
        return SimpleDateFormat(key).format(date).toInt()
    }
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = simpleDateFormat.parse(year.toString() + "-" + (month + 1) + "-" + dayOfMonth)
            val newDate : String= SimpleDateFormat("dd-MM-yyyy").format(date)
            manudate.setText(newDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }    }


    fun mobileNo(mobileNo: String, countryCode: String) : String? {
        return mobileNo.replace("[^\\d]".toRegex(), "").replace(countryCode,"")
    }
    fun mobileNo(mobileNo: TextInputEditText, countryCode: String) : String? {
        return mobileNo.text?.replace("[^\\d]".toRegex(), "")?.replace(countryCode.replace("+",""),"")
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.backIv -> {
                if (nextTv.visibility == View.VISIBLE) {
                    myprofile()
                } else {
                    onBackPressed()
                }
            }
            R.id.calendars->
            {
                dialogCalendar()
            }
            R.id.editTv -> {
                editprofile()
            }

            R.id.profileaddress -> {
                val intent = Intent(this, HomeMapsActivity::class.java)
                startActivityForResult(intent, ADDRESS_PICK)
            }

            R.id.imageView25 -> {
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                                this,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                                == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                                this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                                == PackageManager.PERMISSION_GRANTED)
                ) {
                    profileBottomLayout()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                arrayOf(
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                                ), PERMISSION_CODE
                        )
                    }
                }
            }
            R.id.nextTv -> {


                if(manudate.text.toString().isEmpty())
                {
                    Utils().getMessageDialog(this, "Please select dob!", 0, false)

                }
              else  if (name.getText().toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter the Name!", 0, false)
                } else if (fullname.getText().toString().isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter the Full Name!", 0, false)
                } else if (!profileemail.getText().toString().isEmpty() && !checkMailAddress(profileemail.getText().toString().trim())) {
                    Utils().getMessageDialog(this, "Invalid Email Address!", 0, false)
                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.isEmpty()!!){
                    Utils().getMessageDialog(this, "Please enter the mobile number!", 0, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0<7){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0>15){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }

                else if (!mobile.equals(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "", ignoreCase = true)) {
                    proflieViewModel.checkDriverMobileApi(
                            this,
                            countryCodePicker.selectedCountryCodeWithPlus,
                            mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: ""
                    )
                }
                else if (nationalidentity.text.toString().isEmpty()){
                    Utils().getMessageDialog(this, "Please enter  Emirate Id No.", 1, false)

                }

                else if (nationalidentity1.text.toString().isEmpty()){
                    Utils().getMessageDialog(this, "Please enter passport no.", 1, false)

                }


                else {
                    if (file == null) {
                        proflieViewModel.driverUpdateDetailsApi(this, getSharedPreferences(BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!,
                                fullname.text.toString().trim(), "dummy",
                                img, profileaddress.text.toString().trim(),
                                lat,
                                log, countryCodePicker.selectedCountryCodeWithPlus,
                                mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "", profileemail.text.toString().trim(),
                                nationalidentity.text.toString(),nationalidentity1.text.toString(),manudate.text.toString())
                    } else {
                        val pathList = ArrayList<String>()
                        pathList.add(file!!.absolutePath)
                        S3Bucket(this, pathList, true) { uploadedUrl ->
                            proflieViewModel.driverUpdateDetailsApi(this@ProfileTwoActivity, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!, fullname.text.toString().trim(), "", TextUtils.join(",", uploadedUrl!!), profileaddress.text.toString().trim(), lat, log, countryCodePicker.selectedCountryCodeWithPlus,
                                    mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "", profileemail.text.toString().trim(),nationalidentity.text.toString(),nationalidentity1.text.toString(),manudate.text.toString());
                        }
                    }
                }
            }

            R.id.editTv2 -> {
                GenderSpinner()
            }
        }
    }


    private fun editprofile() {
        name.setSelection(name.getText().toString().trim().length)
        profiletitleTv.setText("Edit Profile")
        nextTv.visibility = View.VISIBLE
        imageView25.visibility = View.VISIBLE
        lockIv.visibility = View.VISIBLE
        editTv2.visibility = View.VISIBLE
        editTv.visibility = View.GONE
//        name.setEnabled(true)
        name.setFocusableInTouchMode(true)
        fullname.setEnabled(true)
        fullname.setFocusableInTouchMode(true)
        profileemail.setEnabled(true)
        manudate.isEnabled=true
        nationalidentity.isEnabled=true
        nationalidentity1.isEnabled=true
        manudate.setFocusableInTouchMode(true)
        nationalidentity.setFocusableInTouchMode(true)
        nationalidentity1.setFocusableInTouchMode(true)



        profileemail.setFocusableInTouchMode(true)


        profileemail.setEnabled(true)
        profileemail.setFocusableInTouchMode(true)
        tilPhoneNo.setEnabled(true)
        tilPhoneNo.setFocusableInTouchMode(true)
        profileaddress.setEnabled(true)
    }


    private fun myprofile() {
        profiletitleTv.setText("My Profile")
        nextTv.visibility = View.GONE
        editTv.visibility = View.VISIBLE
        imageView25.visibility = View.GONE
        lockIv.visibility = View.GONE
        editTv2.visibility = View.GONE
        name.setEnabled(false)
        fullname.setEnabled(false)
        profileemail.setEnabled(false)
        tilPhoneNo.setEnabled(false)
        profileaddress.setEnabled(false)
        nationalidentity.setEnabled(false)
        nationalidentity1.setEnabled(false)
    }


    private fun GenderSpinner() {
        genderList = ArrayList<String>()
        genderList.add("Please select")
        genderList.add("English")
        genderList.add("Arabic")
        val reasonAdapter: ArrayAdapter<*> = object : ArrayAdapter<Any?>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                genderList as List<Any?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val textView = super.getView(position, convertView, parent) as TextView
                textView.setTextColor(Color.TRANSPARENT)
                return textView
            }
        }
        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        reationSpinner2.setAdapter(reasonAdapter)
        reationSpinner2.performClick()
    }

   private fun profileBottomLayout() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val sheetView: View = this.layoutInflater.inflate(R.layout.bottom_dialog, null)
        bottomSheetDialog.setContentView(sheetView)
        bottomSheetDialog.window!!.findViewById<View>(R.id.design_bottom_sheet).setBackgroundResource(
                android.R.color.transparent
        )
        val camIv = sheetView.findViewById<ImageView>(R.id.camIv)
        val gallIv = sheetView.findViewById<ImageView>(R.id.gallIv)
        val canTv = sheetView.findViewById<TextView>(R.id.canTv)
        gallIv.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(intent, GALLERY_PICTURE)
            bottomSheetDialog.dismiss()
        }
        camIv.setOnClickListener {
            captureMediaFile = Utils().getOutputMediaFile(applicationContext)
            if (captureMediaFile != null) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)
                } else {
                    val photoUri = FileProvider.getUriForFile(
                            applicationContext,
                            applicationContext.packageName + ".provider",
                            captureMediaFile!!
                    )
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
                if (intent.resolveActivity(packageManager) != null) {
                    startActivityForResult(intent, CAMERA_REQUEST)
                } else {
                    Toast.makeText(this, "error no camera", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "file saving err", Toast.LENGTH_SHORT).show()
            }
            bottomSheetDialog.dismiss()
        }
        canTv.setOnClickListener { bottomSheetDialog.dismiss() }
        bottomSheetDialog.show()
    }

//    override fun onDateSelect() {
//
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    if (data != null) {
                        val bmp = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                        Glide.with(this).load(bmp).into(imageView23)
                        setImage(bmp)
                    } else {
                        Utils().getMessageDialog(
                                this,
                                "Something Went Wrong.Please Try Again!",
                                0,
                                false
                        )
                    }
                } else {
                    val bmp = BitmapFactory.decodeFile(captureMediaFile!!.getAbsolutePath())
                    Glide.with(this).load(bmp).into(imageView23)
                    setImage(bmp)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            try {
                if (data != null) {
                    val bmp = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                    Glide.with(this).load(bmp).into(imageView23)
                    setImage(bmp)
                } else {
                    Utils().getMessageDialog(
                            this,
                            "Something Went Wrong.Please Try Again!",
                            0,
                            false
                    )
                }
            } catch (e: Exception) {
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        } else if (resultCode == RESULT_OK && requestCode == ADDRESS_PICK) {
            try {
                lat = data!!.getStringExtra("lat")!!
                log = data.getStringExtra("long")!!
                cityName = data.getStringExtra("city")!!
                profileaddress.setText(data.getStringExtra("address"))
            } catch (e: java.lang.Exception) {
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        }
        else if (resultCode == RESULT_OK && requestCode == OTP_CHECK) {
            try {
                if (file == null) {
                    proflieViewModel.driverUpdateDetailsApi(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!,fullname.text.toString().trim(), "", img, profileaddress.text.toString().trim(), lat, log, countryCodePicker.selectedCountryCodeWithPlus, mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "", profileemail.text.toString().trim(),"","","")
                } else {
                    val pathList = ArrayList<String>()
                    pathList.add(file!!.absolutePath)
                    S3Bucket(this, pathList, true
                    ) { uploadedUrl ->
                        proflieViewModel.driverUpdateDetailsApi(this@ProfileTwoActivity, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!,fullname.text.toString().trim(), "", TextUtils.join(",", uploadedUrl!!), profileaddress.text.toString().trim(), lat, log, countryCodePicker.selectedCountryCodeWithPlus, mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "", profileemail.text.toString().trim(),nationalidentity.text.toString(),nationalidentity1.text.toString(),manudate.text.toString())
                    }
                }
            } catch (e: java.lang.Exception) {
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        }
        else {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String?>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                profileBottomLayout()
            } else if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) &&
                    !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                    !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
            ) {
                goToSettings()
            } else {
                Utils().getMessageDialog(this, "This feature require these permissions!", 0, false)
            }
        }
    }

    private fun goToSettings() {
        val myAppSettings = Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:$packageName")
        )
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(myAppSettings)
    }

    private fun setImage(bmp: Bitmap) {
        val thread = Thread {
        file = Utils().saveImageFile(this, bmp) }
        thread.run()
    }

    override fun setimage(image: ImageView) {
        Glide.with(this).load(getSharedPreferences(BITEME, MODE_PRIVATE).getString(Constants.IMAGE, "")).placeholder(R.drawable.profile).into(image)
    }

}