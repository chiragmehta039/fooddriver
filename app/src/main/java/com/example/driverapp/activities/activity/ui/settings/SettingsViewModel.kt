package com.example.driverapp.activities.activity.ui.settings

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.DriverNotificationSettingDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SettingsViewModel : BaseViewModel() {

    var drivernotifyData = MutableLiveData<DriverNotificationSettingDataModel>()

    fun driverNotificationSettingApi(context: Context, token: String, notificationStatus: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverNotificationSetting(token,notificationStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    drivernotifyData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }
}