package com.example.driverapp.activities.activity.models.response


import com.google.gson.annotations.SerializedName

data class CheckDriverMobilForForgotPasswordDataModel(
    @SerializedName("data")
    var `data`: Data?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: Int?
) {
    data class Data(
        @SerializedName("userId")
        var userId: String?
    )
}