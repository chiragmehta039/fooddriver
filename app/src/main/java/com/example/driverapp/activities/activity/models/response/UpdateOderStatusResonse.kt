package com.example.driverapp.activities.activity.models.response

data class UpdateOderStatusResonse(
    val message: String,
    val status: Int
)