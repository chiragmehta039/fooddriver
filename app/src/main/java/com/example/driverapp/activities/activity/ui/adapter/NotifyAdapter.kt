package com.example.driverapp.activities.activity.ui.adapter

import Utils
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.driverapp.R
import com.example.driverapp.activities.activity.models.response.GetDriverNotificationListDataModel
import com.example.driverapp.activities.activity.ui.notification.NotificationActivity1
import com.example.driverapp.activities.activity.ui.notification.NotificationViewModel
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import kotlinx.android.synthetic.main.activity_notification1.*
import kotlinx.android.synthetic.main.layout_notify.view.*

class NotifyAdapter(private val context: Context, private val list: List<GetDriverNotificationListDataModel.Data?>?) : RecyclerView.Adapter<NotifyAdapter.MyViewHolder>() {
    private lateinit var notificationViewModel: NotificationViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyViewHolder = MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_notify, parent, false))


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.textView37.setText(list!!.get(position)!!.notiTitle)
        holder.itemView.textView40.setText(list.get(position)!!.notiMessage)
        holder.itemView.textView38.setText(list.get(position)!!.createdAt)
//        Glide.with(context).load(list!!.get(position)!!.)

        notificationViewModel = ViewModelProvider(context as NotificationActivity1).get(NotificationViewModel::class.java)

        holder.itemView.deleteIv.setOnClickListener{
            notificationViewModel.driverDeleteNotificationApi(context, context.getSharedPreferences(BITEME, AppCompatActivity.MODE_PRIVATE).getString(Constants.TOKEN, "")!!, list.get(position)!!.id!!)
        }

        notificationViewModel.notifydeleteData.observe(context as NotificationActivity1, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Utils().getMessageDialog(context, it.message!!, 0, false)
               notifyDataSetChanged()
            } else {
                Utils().getMessageDialog(context, it.message!!, 0, false)
            }
        })
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    inner class MyViewHolder(view: View?) : RecyclerView.ViewHolder(view!!) {
        init {

        }
    }

}