package com.example.driverapp.activities.activity.ui.otp

import Utils
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.location.SMSReceiver
import com.example.driverapp.activities.activity.ui.resetPassword.ResetPasswordActivity
import com.example.driverapp.activities.activity.ui.setPassword.SetPasswordActivity
import com.example.driverapp.activities.activity.utils.showToast
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import kotlinx.android.synthetic.main.activity_otp.*
import java.util.concurrent.TimeUnit

class OtpActivity : BaseActivity() , View.OnClickListener, OnCompleteListener<AuthResult>,
    OnSuccessListener<Void>,
    OnFailureListener {
    private var resendToken: ForceResendingToken? = null
    private var verificationId: String? = null
    private var mCallbacks: OnVerificationStateChangedCallbacks? = null
    private var mAuth: FirebaseAuth? = null
    private var progressDialog: Dialog ? = null
//    lateinit var proflieViewModel: ProflieViewModel




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        init()
        initControl()
        autoReadOTP()
    }
    private fun autoReadOTP() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener(this)
        task.addOnFailureListener(this)
    }

    override fun init() {

//        proflieViewModel = ViewModelProvider(this).get(ProflieViewModel::class.java)
//        proflieViewModel.updateprofileData.observe(this, androidx.lifecycle.Observer {
//            if (it!!.status == 200) {
//                Utils().getMessageDialog(this, it.message!!, 0, false)
////                myprofile()
//            } else {
//                Utils().getMessageDialog(this, it.message!!, 0, false)
//            }
//        })

        mAuth = FirebaseAuth.getInstance()

//         var mCallbacks:PhoneAuthProvider.OnVerificationStateChangedCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
//
//            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
//            //    hideLoader()
//                if(credential?.smsCode!=null) {
//                    var str = credential?.smsCode!!.toString()
//                    var first:String=str[0].toString()
//                    var second:String=str[1].toString()
//                    var third:String=str[2].toString()
//                    var fourth:String=str[3].toString()
//                    var fives:String=str[4].toString()
//                    var sizxth:String=str[5].toString()
//
//                    one.setText(first)
//                    two.setText(second)
//                    three.setText(third)
//                    four.setText(fourth)
//                    five.setText(fives)
//                    six.setText(sizxth)
//
//
//
////                    binding?.otpView.setOTP("" + phoneAuthCredential?.smsCode!!)
//                    signInWithPhoneAuthCredential(credential)
//                }
//            }
//
//            override fun onVerificationFailed(e: FirebaseException) {
//                showToast(e.localizedMessage)
//
//                if (e is FirebaseTooManyRequestsException) Utils().getMessageDialog(
//                        this@OtpActivity,
//                        "Too many requests. Please try again later!",
//                        0,
//                        false
//                )
//                Utils().dismissDialog(progressDialog)
//                e.printStackTrace()
//                Log.w("testData", "authFailed: " + e.message)
//            }
//
//            override fun onCodeSent(s: String, token: PhoneAuthProvider.ForceResendingToken) {
//                verificationId = s
//                resendToken = token
//                Utils().dismissDialog(progressDialog)
//            }
//        }

        mCallbacks = object :PhoneAuthProvider. OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                if(phoneAuthCredential?.smsCode!=null) {
                    showToast(phoneAuthCredential?.smsCode!!.toString())

                    var str = phoneAuthCredential?.smsCode!!.toString()
                    var first:String=str[0].toString()
                    var second:String=str[1].toString()
                    var third:String=str[2].toString()
                    var fourth:String=str[3].toString()
                    var fives:String=str[4].toString()
                    var sizxth:String=str[5].toString()

                    one.setText(first)
                    two.setText(second)
                    three.setText(third)
                    four.setText(fourth)
                    five.setText(fives)
                    six.setText(sizxth)



//                    binding?.otpView.setOTP("" + phoneAuthCredential?.smsCode!!)
                   signInWithPhoneAuthCredential(phoneAuthCredential)
                }
                Log.w("testData", "authComplete")
            }

            override fun onVerificationFailed(e: FirebaseException) {
                showToast(e.localizedMessage)

                if (e is FirebaseTooManyRequestsException) Utils().getMessageDialog(
                    this@OtpActivity,
                    "Too many requests. Please try again later!",
                    0,
                    false
                )
                Utils().dismissDialog(progressDialog)
                e.printStackTrace()
                Log.w("testData", "authFailed: " + e.message)
            }

            override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
                super.onCodeSent(s, forceResendingToken)
                verificationId = s
                resendToken = forceResendingToken
                Utils().dismissDialog(progressDialog)
            }
        }

        one.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (!charSequence.toString().isEmpty())
                    two.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        two.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (!charSequence.toString().isEmpty())
                    three.requestFocus()
                else one.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        three.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (!charSequence.toString().isEmpty())
                    four.requestFocus()
                else
                    two.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        four.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (!charSequence.toString().isEmpty())
                    five.requestFocus()
                else
                    three.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        five.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (!charSequence.toString().isEmpty())
                    six.requestFocus()
                else four.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        six.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().isEmpty())
                    five.requestFocus()
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        sendCode()
    }

    private fun sendCode() {

        val options = PhoneAuthOptions.newBuilder(mAuth!!).setPhoneNumber( intent.getStringExtra("countrycodepicker") + intent.getStringExtra("phone") ?: "").setTimeout(60L, TimeUnit.SECONDS).setActivity(this).setCallbacks(mCallbacks!!).build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }


    private fun resendVerificationCode(token: ForceResendingToken) {

        val options = PhoneAuthOptions.newBuilder(mAuth!!)
                .setPhoneNumber(intent.getStringExtra("countrycodepicker") + intent.getStringExtra("phone") ?: "") // Phone number to verify
                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                .setActivity(this) // Activity (for callback binding)
                .setCallbacks(mCallbacks!!) // OnVerificationStateChangedCallbacks
                .setForceResendingToken(token!!)
                .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {

        mAuth!!.signInWithCredential(credential).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                if (intent.hasExtra("forgot_key")) {
                    val i = Intent(this, ResetPasswordActivity::class.java)
                    i.putExtra("otp", true)
                    i.putExtras(intent.extras!!)
                    startActivity(i)
                }else if(intent.getBooleanExtra("profile", false)) {
                    setResult(RESULT_OK)
                    finish()
//                    finish()
//                    if (intent.getStringExtra("file") == null) {
//                        proflieViewModel.driverUpdateDetailsApi(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!,
//                            intent.getStringExtra("name")+"",
//                            intent.getStringExtra("fullname")+"",
//                            intent.getStringExtra("img")+"",
//                            intent.getStringExtra("address")+"",
//                            intent.getStringExtra("lat")+"",
//                            intent.getStringExtra("log")+"",
//                            intent.getStringExtra("countrycodepicker")+"",
//                            intent.getStringExtra("phone")+"",
//                            intent.getStringExtra("email")+"")
//                    } else {
//                        S3Bucket(this, intent.getStringArrayListExtra("file"), true, object : S3Bucket.UploadedListener {
//                            override fun finish(uploadedUrl: ArrayList<String?>?) {
//                                proflieViewModel.driverUpdateDetailsApi(this@OtpActivity, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!,
//                                    intent.getStringExtra("name")+"",
//                                    intent.getStringExtra("fullname")+"",
//                                    TextUtils.join(",", uploadedUrl!!),
//                                    intent.getStringExtra("address")+"",
//                                    intent.getStringExtra("lat")+"",
//                                    intent.getStringExtra("log")+"",
//                                    intent.getStringExtra("countrycodepicker")+"",
//                                    intent.getStringExtra("phone")+"",
//                                    intent.getStringExtra("email")+"")
//                           }
//                        })
//                    }
                } else {
                    startActivity(Intent(this, SetPasswordActivity::class.java).putExtras(intent.extras!!))
                }

                Utils().dismissDialog(progressDialog)
            } else {
                Utils().dismissDialog(progressDialog)
                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    Utils().getMessageDialog(this, "Invalid Otp!", 0, false)
                }
            }
        }
    }


    override fun initControl() {
        backIv.setOnClickListener(this)
        nextTv.setOnClickListener(this)
        forgotPass.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv -> {
                onBackPressed()
            }

            R.id.nextTv -> {
                val otp = one.text.toString().trim() + two.text.toString().trim() +
                        three.text.toString().trim() + four.text.toString().trim() +
                        five.text.toString().trim() + six.text.toString().trim()

                if (otp.isEmpty() || otp.length != 6) {
                    Utils().getMessageDialog(this, "Please enter otp first!", 1, false)
                } else {
                    if(progressDialog == null || !progressDialog!!.isShowing())
                        progressDialog = Utils().getProgressDialog(this)
                    if(verificationId!=null) {
                        val credential = PhoneAuthProvider.getCredential(verificationId!!, otp)
                        signInWithPhoneAuthCredential(credential)
                    }else{
                        Utils().getMessageDialog(this, "Invalid Otp!", 0, false)
                    }
                }
            }

            R.id.forgotPass -> {
                progressDialog = Utils().getProgressDialog(this)
                resendVerificationCode(resendToken!!)
            }
        }
    }

    override fun onComplete(task: Task<AuthResult>) {
        if (task.isSuccessful) {
            val i = Intent(this, ResetPasswordActivity::class.java)
            i.putExtra("otp", true)
            i.putExtras(intent.extras!!)
            startActivity(i)
        } else {
            if (task.exception is FirebaseAuthInvalidCredentialsException) {

            }
        }

        }


    override fun onSuccess(p0: Void?) {
        val filter = IntentFilter()
        filter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        this.registerReceiver(SMSReceiver(), filter)    }

    override fun onFailure(p0: Exception) {
    }
}