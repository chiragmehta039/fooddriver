package com.example.driverapp.activities.activity.ui.termsCondition

import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import kotlinx.android.synthetic.main.activity_terms_and_conditions.*

class TermsAndConditionsActivity  : BaseActivity(), View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_conditions)
        init()
        initControl()

    }


    override fun init() {
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        /*   forgotPass.setOnClickListener(this)*/

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv->{
                onBackPressed()
            }
            /*   R.id.forgotPass->{
                   startActivity(Intent(this, ForgotPassword::class.java))
               }*/


        }
    }
}