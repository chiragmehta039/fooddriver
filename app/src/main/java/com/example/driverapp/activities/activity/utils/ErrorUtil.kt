package com.example.driverapp.activities.activity.utils

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.driverapp.R
import com.example.driverapp.activities.activity.models.base.ErrorBean
import com.example.driverapp.activities.activity.ui.login.LoginActivity


import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ErrorUtil {

    fun handlerGeneralError(context: Context?, throwable: Throwable) {
        ProgressDialogUtils.getInstance().hideProgress()
        throwable.printStackTrace()

        if (context == null) return

        when (throwable) {
            //For Display Toast

            is ConnectException -> Toast.makeText(context, context.getString(R.string.netwok_error), Toast.LENGTH_SHORT).show()
            is SocketTimeoutException -> Toast.makeText(context, context.getString(R.string.connection_lost), Toast.LENGTH_SHORT).show()
            is UnknownHostException -> Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            is InternalError -> Toast.makeText(context,  context.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

            is HttpException -> {
                try {
                    when (throwable.code()) {
                        401 -> {
                            //Logout
                            forceLogout(context)
                        }
                        403 -> {

                            displayError(context, throwable)
                        }
                        else -> {

                            displayError(context, throwable)
                        }
                    }
                } catch (exception: Exception) {

                }
            } else -> {

        }
            //For Display SnackBar
            /*is HttpException -> {
                try {
                    when (throwable.code()) {
                        401 -> {
                            SnackbarUtils.displayError(view, throwable)
                            //logout(context)


                        }
                        else -> {
                            SnackbarUtils.displayError(view, throwable)
                        }
                    }
                } catch (exception: Exception) {
                    SnackbarUtils.somethingWentWrong(view)
                    exception.printStackTrace()
                }
            }
            is ConnectException -> SnackbarUtils.displayError(view, throwable)
            is SocketTimeoutException -> SnackbarUtils.displayError(view, throwable)
            else -> SnackbarUtils.somethingWentWrong(view)
       */

        }
    }




//    ** Perform logout for both the success and error case (force logout)

    fun forceLogout(context: Context) {



        var sharedPreferenceUtil= SharedPreferenceUtil.getInstance(context)
        sharedPreferenceUtil.isLogin=false
        sharedPreferenceUtil.userFirstName=""
        sharedPreferenceUtil.userEmail=""
        sharedPreferenceUtil.userMobile=""
        sharedPreferenceUtil.jwtToken=""


    }

    fun displayError(context: Context, exception: HttpException) {
        try {
            val errorBody = getGsonInstance().
            fromJson(exception.response()?.errorBody()?.charStream(),
                ErrorBean::class.java)
            //  SnackbarUtils.displaySnackbar(view, errorBody.message)
            Log.e("ErrorMessage", errorBody.message)
            Toast.makeText(context, errorBody.message, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Log.e("MyExceptions", e.message!!)
            Toast.makeText(context, context.getString(R.string.error_exception),
                Toast.LENGTH_SHORT).show()
        }
    }
}


