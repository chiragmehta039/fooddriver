package com.example.driverapp.activities.activity.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.map.MapActivity
import com.example.driverapp.activities.activity.ui.orderDetail.OrderDetailActivity
import kotlinx.android.synthetic.main.activity_vehicle_details.*
import kotlinx.android.synthetic.main.layout_home.view.*
import kotlinx.android.synthetic.main.layout_todays.view.*

class RejectedAdapter(var context: Context): RecyclerView.Adapter<RejectedAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.layout_home, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.homeConsLay.setOnClickListener {
            context.startActivity(Intent(context, MapActivity::class.java))
        }







    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



    }



}


