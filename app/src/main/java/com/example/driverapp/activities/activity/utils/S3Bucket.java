package com.example.driverapp.activities.activity.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.example.driverapp.R;

import java.io.File;
import java.util.ArrayList;

/*MonaSharma*/

public class S3Bucket
{

    private Context context;
    private final String POOL_ID = "ap-south-1:a5550f08-5205-4110-b5f1-65cc4033ce61";
    private final String BUCKET_NAME="youpick";
    private AmazonS3Client s3Client;
    private boolean showProgress;
    private ArrayList<String> pathList, data;
    private Handler uploadHandler;
    private int index = 0;
    private ArrayList<TransferObserver> transferObserver;
    private UploadedListener uploadedListener;

    public interface UploadedListener{
        void finish(ArrayList<String> uploadedUrl);
    }

    public S3Bucket(Context context, ArrayList<String> pathList, boolean showProgress, UploadedListener uploadedListener)
    {
        this.context = context;
        this.showProgress = (showProgress && (pathList.size() == 1));
        this.pathList = pathList;
        transferObserver = new ArrayList<>();
        this.uploadedListener=uploadedListener;

        s3ConfBucket();
        uploadFileToS3();
    }

    private void s3ConfBucket() {
//        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider = new CognitoCachingCredentialsProvider(
//                context,
//                POOL_ID,
//                Regions.AP_SOUTH_1);

         s3Client = new AmazonS3Client(new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return "AKIAV65DEK7Y7MW6N3R6";
            }

            @Override
            public String getAWSSecretKey() {
                return "gP3EYOChqqdnTrs9N/2w1M68sjcySF9C2flWXRVc";
            }
        });


//        s3Client = new AmazonS3Client(new BasicAWSCredentials("AKIAV65DEK7Y7MW6N3R6", "gP3EYOChqqdnTrs9N/2w1M68sjcySF9C2flWXRVc"));
//        s3Client = new AmazonS3Client(cognitoCachingCredentialsProvider);

        s3Client.setRegion(Region.getRegion(Regions.US_EAST_2));
    }


    public void uploadFileToS3() {
        Dialog progressDialog;
        progressDialog = getProgressDialog();

        ArrayList<TransferObserver> transferObserverList = new ArrayList<>();


        for (int i = 0; i < pathList.size(); i++) {
            File file = new File(pathList.get(i));

            TransferObserver transferObserver = new TransferUtility(s3Client, context).upload(
                    BUCKET_NAME,
                    file.getName(),
                    file
            );

            transferObserverList.add(transferObserver);

        }


        index = 0;
        data = null;


        uploadHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                if (msg.what == 1) {
                    if ((index + 1) < transferObserver.size()) {
                        index += 1;
                        Log.w("testData", "index update: " + index);
                    } else {
                        progressDialog.dismiss();
                        if(uploadedListener!=null)
                        {
                            uploadedListener.finish(data);
                        }

                    }
                }

            }
        };

        this.transferObserver = transferObserverList;
        for (int i = 0; i < this.transferObserver.size(); i++)
            transferObserverListener(transferObserver.get(i), progressDialog);

    }


    public void transferObserverListener(TransferObserver transferObserver, Dialog progressDialog) {

        if (data == null)
            data = new ArrayList<>();

        Log.w("testData", "inside download method");
        ProgressBar progressBar;
        if(pathList.size()==1 && showProgress)
        {
            progressBar = progressDialog.findViewById(R.id.progressBarWith);
        }else
        {
            progressBar = progressDialog.findViewById(R.id.progressBarWithout);
        }

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {

                Log.w("testData", state.name() + "  index: " + index);

                if (state == TransferState.COMPLETED) {
                    String url = "https://" + BUCKET_NAME + ".s3.us-east-2.amazonaws.com/" + transferObserver.getKey();
                    Log.w("testData :,", url);

                    data.add(url);

                    uploadHandler.sendEmptyMessage(1);

                } else if (state == TransferState.FAILED) {
                    uploadHandler.sendEmptyMessage(1);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                // int percentage = (int) (bytesCurrent/bytesTotal * 100);
                float percentage = ((float) bytesCurrent / (float) bytesTotal * 100);
                ;
                Log.w("testData", "Uploaded: " + percentage);
                if (showProgress)
                    progressBar.setProgress(Math.round(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {
                // uploadHandler.sendEmptyMessage(1);
                ex.printStackTrace();
                Log.w("testData", "error");
            }
        });
    }


    private Dialog getProgressDialog() {
        Dialog pd = new Dialog(context, android.R.style.Theme_Black);
        View view = LayoutInflater.from(context).inflate(R.layout.progress_layout_s3, null);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.getWindow().setBackgroundDrawableResource(R.color.transparent);
        pd.setContentView(view);
        pd.setCancelable(false);

        if(pathList.size()==1 && showProgress)
        {
            view.findViewById(R.id.progressBarWith).setVisibility(View.VISIBLE);
        }else
        {
            view.findViewById(R.id.progressBarWithout).setVisibility(View.VISIBLE);
        }

        pd.show();

        return pd;

    }
}
