package com.example.driverapp.activities.activity.ui.profile

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.CheckDriverMobileDataModel
import com.example.driverapp.activities.activity.models.response.DriverChangeEmailDataModel
import com.example.driverapp.activities.activity.models.response.DriverChangeMobileNumberDataModel
import com.example.driverapp.activities.activity.models.response.DriverUpdateDetailsDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProflieViewModel : BaseViewModel() {

    var updateprofileData = MutableLiveData<DriverUpdateDetailsDataModel>()
    var checkmobileData = MutableLiveData<CheckDriverMobileDataModel>()
//    var changeEmailData = MutableLiveData<DriverChangeEmailDataModel>()
//    var changeMobileData = MutableLiveData<DriverChangeMobileNumberDataModel>()


    fun driverUpdateDetailsApi(context: Context, token: String, firstName: String, lastName: String, profilePic: String?, address: String, latitude: String, longitude: String,countryCode: String, mobileNumber: String, email: String,emNum:String,pNumber:String,dob:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverUpdateDetails(token,firstName,"", profilePic!!, address , latitude, longitude,countryCode,mobileNumber,emNum,pNumber,dob,email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    updateprofileData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }

    fun checkDriverMobileApi(context:Context, countryCode: String, mobileNumber: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.checkDriverMobile(countryCode, mobileNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Utils().dismissDialog(progressDialog)
                checkmobileData.value = it
            }, {
                Utils().dismissDialog(progressDialog)
                ErrorUtil.handlerGeneralError(context, it)
            })
    }

//    fun driverChangeEmailApi(context:Context, countryCode: String, mobileNumber: String) {
//        val progressDialog = Utils().getProgressDialog(context)
//        apiInterface.driverChangeEmail(countryCode, mobileNumber)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//                Utils().dismissDialog(progressDialog)
//                changeEmailData.value = it
//            }, {
//                Utils().dismissDialog(progressDialog)
//                ErrorUtil.handlerGeneralError(context, it)
//            })
//    }
//
//    fun driverChangeMobileNumberApi(context:Context, countryCode: String, mobileNumber: String) {
//        val progressDialog = Utils().getProgressDialog(context)
//        apiInterface.driverChangeMobileNumber(countryCode, mobileNumber)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//                Utils().dismissDialog(progressDialog)
//                changeMobileData.value = it
//            }, {
//                Utils().dismissDialog(progressDialog)
//                ErrorUtil.handlerGeneralError(context, it)
//            })
//    }
}