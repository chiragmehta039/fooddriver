package com.example.driverapp.activities.activity.ui.firebase

import android.R.id.message
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.dialog_burgerking.*


class BReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        val i = Intent(context, HomeActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("orderId",intent.getStringExtra("orderId"))
                .putExtra("distance",intent.getStringExtra("distance"))
                .putExtra("restaurantName",intent.getStringExtra("restaurantName")




                )
        context.startActivity(i)
      //  dialogBurgerking(context)
    }


    fun dialogBurgerking(context: Context) {
      var    dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.dialog_burgerking)
        dialog!!.window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()

        dialog!!.acceptTv.setOnClickListener {

            dialog!!.dismiss()
        }
        dialog!!.rejectTv.setOnClickListener {
            dialog!!.dismiss()
        }
    }

}