package com.example.driverapp.activities.activity.ui.orderDetail

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.map.MapActivity
import kotlinx.android.synthetic.main.activity_order_detail.*

class OrderDetailActivity : BaseActivity(), View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        init()
        initControl()

    }



    override fun init() {

    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        navigation.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.backIv->{
                onBackPressed()
            }
            R.id.navigation->{
                startActivity(Intent(this, MapActivity::class.java))
            }



        }
    }
}