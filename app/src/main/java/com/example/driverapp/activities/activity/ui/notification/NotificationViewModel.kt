package com.example.driverapp.activities.activity.ui.notification

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverClearNotificationDataModel
import com.example.driverapp.activities.activity.models.response.DriverDeleteNotificationDataModel
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.GetDriverNotificationListDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NotificationViewModel : BaseViewModel() {

    var notifyListData = MutableLiveData<GetDriverNotificationListDataModel>()
    var notifydeleteData = MutableLiveData<DriverDeleteNotificationDataModel>()
    var notifyclearData = MutableLiveData<DriverClearNotificationDataModel>()

    fun getDriverNotificationListApi(context: Context,token:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getDriverNotificationList(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    notifyListData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun driverDeleteNotificationApi(context: Context,token:String,notificationId:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverDeleteNotification(token,notificationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    notifydeleteData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun driverClearNotificationApi(context: Context,token:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverClearNotification(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    notifyclearData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
}