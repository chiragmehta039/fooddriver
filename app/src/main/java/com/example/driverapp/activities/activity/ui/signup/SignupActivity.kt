package com.example.driverapp.activities.activity.ui.signup

import Utils
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.ui.signupTwo.SignUpTwoActivity
import com.google.android.material.textfield.TextInputEditText
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import kotlinx.android.synthetic.main.activity_signup.*
import me.ibrahimsn.lib.PhoneNumberKit
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SignupActivity: BaseActivity() ,View.OnClickListener,DatePickerDialog.OnDateSetListener{
    var phoneNumber : PhoneNumberFormattingTextWatcher?=null
    var watcher: TextWatcher ?=null
    var isListnerAdded :Boolean=false
    var value="0"
    private val date: Date = Calendar.getInstance().time


    var values="0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        init()
        initControl()

        consdateofbirth.setOnClickListener(View.OnClickListener {
            dialogCalendar()

        })


    }



    override fun init() {
        spannableText()
    }



    override fun initControl() {
        signupTv.setOnClickListener(this)
        nextTv.setOnClickListener(this)
        consdateofbirth.setOnClickListener(this)
        phoneNumber=PhoneNumberFormattingTextWatcher()



//        watcher = object : TextWatcher {
//            override fun afterTextChanged(s: Editable) {
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                if(s.length>0){
//                    try{
//                        mobileNo(s)?:"".toLong()
//                        phone1.removeTextChangedListener(watcher)
//                        if(!isListnerAdded) {
//                            isListnerAdded = true
//                            phone1.addTextChangedListener(phoneNumber)
//                        }
//
//                    }catch (e:java.lang.Exception){
//                    }
//
//                }
//
//            }
//        }
//        phone1.addTextChangedListener(watcher)
    }
    private fun getCountryIsoCode(number: String, context: Context): String? {
        val phoneNumberUtil = PhoneNumberUtil.createInstance(context)
        val validatedNumber = if (number.startsWith("+")) number else "+$number"
        val phoneNumber = try {
            phoneNumberUtil.parse(validatedNumber, null)
        } catch (e: NumberFormatException) {
            Log.e("TAG", "error during parsing a number")
            null
        }
        if(phoneNumber == null) return ""
        return "+"+phoneNumber.countryCode
    }
    override fun onResume() {
        super.onResume()
        if(intent.hasExtra("name"))
        {
            value="1"
            name.setText(intent.getStringExtra("name"));
            email.setText(intent.getStringExtra("email"))
            var number=intent.getStringExtra("countrycodepicker")?.replace("+","")




            //    showToast(intent.getStringExtra("phone"))
            dateTv1.setText(intent.getStringExtra("datetv1"))
            val phoneNumberKit = PhoneNumberKit(this)
            phoneNumberKit.attachToInput(tilPhoneNo, number?.toInt()!!)
            phoneNumberKit.setupCountryPicker(this as AppCompatActivity, searchEnabled = true)

            tiePhoneNo.setText(intent.getStringExtra("countrycodepicker")+intent.getStringExtra("phone"))
            tilPhoneNo.getEditText()?.setText(intent.getStringExtra("countrycodepicker")+intent.getStringExtra("phone"));
            //   value= intent.getStringExtra("switch").toString()


        }
        else
        {
            val phoneNumberKit = PhoneNumberKit(this)
            phoneNumberKit.attachToInput(tilPhoneNo, countryCodePicker.selectedCountryCodeAsInt)
            phoneNumberKit.setupCountryPicker(this as AppCompatActivity, searchEnabled = true)
        }


//        phone1.addTextChangedListener(object : TextWatcher {
//
//
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                phone1.setOnKeyListener(object : View.OnKeyListener{
//                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
//                        if (keyCode == KeyEvent.KEYCODE_DEL) {
//                            keyDel = 1;
//                        }
//                        return false;
//                    }
//
//
//                })
//                if(phone1.text.length<2)
//                {
//                    keyDel=0
//                }
//                try{
//
//                    phone1.text.toString().toLong()
//                    var cou=   countryCodePickerrr.selectedCountryCodeWithPlus
//                    if(cou.equals("+91"))
//                    {
//                        if(keyDel==0) {
//
//
//                            if (phone1.text?.length == 3 ) {
//
//
//                                phone1.setText(phone1.text.toString() + "-")
//
//                                phone1.setSelection(phone1.text.length)
//                                keyDel=1
//
//
//                            }
//
//
//
//
//
//                        }
//                    }
//                    else
//                    {
//                        phone1.text.toString().toLong()
//                        if(keyDel==0) {
//
//
//                            if (phone1.text?.length == 2 ) {
//
//
//                                phone1.setText(phone1.text.toString() + "-")
//
//                                phone1.setSelection(phone1.text.length)
//                                keyDel=1
//
//
//                            }
//
//
//
//                        }
//
//                    }
//
//                } catch (e : Exception){
//
//                }
//
//            }
//
//            override fun afterTextChanged(arg0: Editable) {
//            }
//
//            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
//            }
//        })

    }
    fun mobileNo(mobileNo: CharSequence) : String? {
        return mobileNo.toString().replace("[^\\d]".toRegex(), "")
    }

    fun mobileNo(mobileNo: String, countryCode: String) : String? {
        return mobileNo.replace("[^\\d]".toRegex(), "").replace(countryCode,"")
    }
    fun mobileNo(mobileNo: TextInputEditText, countryCode: String) : String? {
        return mobileNo.text?.replace("[^\\d]".toRegex(), "")?.replace(countryCode.replace("+",""),"")
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.nextTv-> {


                if(name.getText().toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter the Name!", 0, false)
                } else if (email.getText().toString().isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter the Email!", 0, false)
                }

                else if (!email.getText().toString().contains("@")) {
                    Utils().getMessageDialog(this, "Please enter the valid Email!", 0, false)
                }

                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.isEmpty()!!){
                    Utils().getMessageDialog(this, "Please enter the mobile number!", 0, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0<7){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }
                else if(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus)?.length?:0>15){
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)


                }

                else if (dateTv1.getText().toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please select the date of birth!", 0, false)
                } else {

                    if(value.equals("1"))
                    {
                        var number=getCountryIsoCode(tiePhoneNo.text.toString(),this)


                        val singletonexample: singleTonExample =
                            singleTonExample.getInstance()
                        singletonexample.setName(name.text.toString().trim())
                        singletonexample.setCountrycodepicker(number)
                        singletonexample.setEmail( email.text.toString().trim())
                        singletonexample.setPhone(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "")
                        singletonexample.setDatetv1(dateTv1.text.toString().trim())



                        values="1"

                        onBackPressed()
                    }
                    else
                    {
                 //       showToast(countryCodePicker.selectedCountryCodeWithPlus.toString())
                        var number=getCountryIsoCode(tiePhoneNo.text.toString(),this)


                        val singletonexample: singleTonExample =
                            singleTonExample.getInstance()
                        singletonexample.setName(name.text.toString().trim())
                        singletonexample.setCountrycodepicker(number)
                        singletonexample.setEmail( email.text.toString().trim())
                        singletonexample.setPhone(mobileNo(tiePhoneNo,countryCodePicker.selectedCountryCodeWithPlus) ?: "")
                        singletonexample.setDatetv1(dateTv1.text.toString().trim())

                        startActivity(
                                Intent(this, SignUpTwoActivity::class.java)
                        )
                    }

                }
            }
            R.id.consdateofbirth->{
                dialogCalendar()
            }

        }
    }

    override fun onBackPressed() {
        if(values.equals("1"))
        {
            super.onBackPressed()

        }
        else
        {
            startActivity(Intent(this,LoginActivity::class.java))
        }
    }

    fun dialogCalendar(){

        val dialog = DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, setFormat("yyyy",date), setFormat("MM",date) - 1, setFormat("dd",date))
        dialog.datePicker.maxDate = Date().time
        dialog.show()



    }
    private fun setFormat(key:String, date:Date): Int{
        return SimpleDateFormat(key).format(date).toInt()
    }
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = simpleDateFormat.parse(year.toString() + "-" + (month + 1) + "-" + dayOfMonth)
            val newDate : String= SimpleDateFormat("dd-MM-yyyy").format(date)
            dateTv1.setText(newDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

    }



    private fun spannableText() {
        val content = SpannableString("Already have an account? Login")
        content.setSpan(object : ClickableSpan() {
            override fun onClick(vi: View) {
                startActivity(Intent(this@SignupActivity,LoginActivity::class.java))}
            override fun updateDrawState(ds: TextPaint) { super.updateDrawState(ds)
                ds.isUnderlineText = false } },
            25, 30, 0)
        content.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,R.color.solidpink)), 25, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        content.setSpan(StyleSpan(Typeface.BOLD), 25, 30, 0)
        signupTv.setText(content)
        signupTv.setMovementMethod(LinkMovementMethod.getInstance())
    }


}