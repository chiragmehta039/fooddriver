package com.example.driverapp.activities.activity.ui.setPassword

import Utils
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.vehicle.VehicleDetailsActivity
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.activity_set_password.*
import kotlinx.android.synthetic.main.activity_set_password.nextTv

class SetPasswordActivity : BaseActivity() , View.OnClickListener{


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_password)
        init()
        initControl()

    }
    override fun init() {
    }

    override fun initControl() {
        nextTv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.nextTv->{
                if (confirmpassword.text.toString().trim().isEmpty() || password.text.toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please fill all details!", 1, false)
                } else if (password.text.toString().length < 6)
                    Utils().getMessageDialog(this, "Password must contain minimum 6 characters and maximum 16 characters", 1, false)
                else if (password.text.toString().length > 16)
                    Utils().getMessageDialog(this, "Password must contain minimum 6 characters and maximum 16 characters", 1, false)
                else if (password.text.toString().trim() != confirmpassword.text.toString().trim()) {
                    Utils().getMessageDialog(this, "password and Confirm password must be same!", 1, false)
                } else {
                    startActivity(Intent(this,VehicleDetailsActivity::class.java).putExtras(intent.extras!!)
                    .putExtra("password",password.text.toString().trim()))
                }
            }
        }
    }

}