package com.example.driverapp.activities.activity.ui.otp

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OtpViewModel : BaseViewModel() {

    var loginData = MutableLiveData<DriverLoginDataModel>()

    fun loginApi(context: Context, password: String, countryCode: String, mobileNumber: String, deviceToken: String,deviceType: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverLogin(password, countryCode, mobileNumber, deviceToken, deviceType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    loginData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }
}