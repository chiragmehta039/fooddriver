package com.example.driverapp.activities.activity.ui.contact
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import kotlinx.android.synthetic.main.activity_contct_us.*
import kotlinx.android.synthetic.main.activity_reset_password.backIv

class ContactUsActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contct_us)
        init()
        initControl()

    }
    override fun init() {
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        cons1.setOnClickListener(this)
        cons.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv->{
                onBackPressed()
            }
            R.id.cons->{
                try {
                    val email = Intent(Intent.ACTION_SENDTO)
                    email.data = Uri.parse("mailto:" + "Support@bite.me.com")
                    email.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                    email.putExtra(Intent.EXTRA_TEXT, "My Email message")
                    startActivity(email)
                } catch (e: ActivityNotFoundException) {
                }
            }
            R.id.cons1->{
               phoneIntent("+931 912456789")
            }
        }
    }

      fun phoneIntent(phone: String) {
        val callIntent = Intent(Intent.ACTION_DIAL)
        callIntent.data = Uri.parse("tel:$phone")
        startActivity(callIntent)
    }
}