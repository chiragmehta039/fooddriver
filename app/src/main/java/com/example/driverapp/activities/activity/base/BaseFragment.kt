package com.example.driverapp.activities.activity.base

import androidx.fragment.app.Fragment
import com.example.driverapp.activities.activity.utils.SharedPreferenceUtil
import com.example.driverapp.activities.activity.webservices.ApiInterface


abstract class BaseFragment: Fragment() {

//    val apiInterface: ApiInterface by lazy {
//        RetrofitUtil.createBaseApiService()
//    }

    val prefs: SharedPreferenceUtil by lazy {
        SharedPreferenceUtil.getInstance(activity!!)
    }
    abstract fun init()
    abstract fun initControl()
}