package com.example.driverapp.activities.activity.ui.firebase;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.example.driverapp.activities.activity.ui.home.HomeActivity;
import com.example.driverapp.activities.activity.ui.vehicle.VehicleDetailsActivity;

public class MyService extends Service {
   String ordeid="";
   public MyService() {
   }

   @Override
   public void onCreate() {

      super.onCreate();


   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId){
      onTaskRemoved(intent);
      if (intent != null && intent.getExtras() != null){
         ordeid = intent.getStringExtra("orderId");
         startActivity(new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("orderId",ordeid)
                 .putExtra("restaurantName",intent.getStringExtra("restaurantName"))
                 .putExtra("distance",intent.getStringExtra("distance"))
         );;

      }



      return START_STICKY;
   }
   @Override
   public IBinder onBind(Intent intent) {
      // TODO: Return the communication channel to the service.
      throw new UnsupportedOperationException("Not yet implemented");
   }

   @Override
   public void onDestroy() {
      super.onDestroy();
   }



   @Override
   public void onTaskRemoved(Intent rootIntent) {

      super.onTaskRemoved(rootIntent);
   }
}