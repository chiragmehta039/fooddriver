package com.example.driverapp.activities.activity.ui.home

import Utils
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.*
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Field

class HomeViewModel : BaseViewModel() {

    var notifyData = MutableLiveData<GetDriverNotificationCountDataModel>()
    var dutystatusData = MutableLiveData<UpdateDutyStatusDataModel>()
    var logoutData = MutableLiveData<DriverLogoutDataModel>()
    var locationData = MutableLiveData<UpdateLocationDataModel>()
    var getdriverData = MutableLiveData<GetDriverDetailsDataModel>()
    var getOrderListData = MutableLiveData<OrderListResponse>()
    var getOrderListsData = MutableLiveData<OrderListResponse>()

    var orderstatusresponse=MutableLiveData<UpdateOderStatusResonse>()
    var orderdetail = MutableLiveData<OrderDetailResponse>()

    var orderACeeptData = MutableLiveData<OrderResponse>()

    var orderRejectData = MutableLiveData<OrderResponse>()


    fun getDriverNotificationCountApi(context: Context,token:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getDriverNotificationCount(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    notifyData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
    fun getOrderStatusAPi(context: Context,driverid:String,orderid:String,staus:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.updateOrderSatus(driverid,orderid,staus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderstatusresponse.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }




    fun getOrderAccept(context: Context,driverId:String,latitude:String,longitude:String,orderId:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.orderAccept(driverId,latitude,longitude,orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderACeeptData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun getOrderDetail(context: Context, driverId: String, orderId: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getOrderDetail(driverId, orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderdetail.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun getOrderList(context: Context,driverId:String,ordertype:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getOrderList(driverId,ordertype)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    getOrderListData.value = it
                }, {


                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
    fun getOrderLists(context: Context,driverId:String,ordertype:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getOrderList(driverId,ordertype)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    getOrderListsData.value = it
                }, {


                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun getOrderReject(context: Context,driverId:String,orderId:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.orderReject(driverId,orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderACeeptData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun getDriverDetailsApi(context: Context,token: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getDriverDetails(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Utils().dismissDialog(progressDialog)
                getdriverData.value = it
            }, {
                Utils().dismissDialog(progressDialog)
                ErrorUtil.handlerGeneralError(context, it)
            })
    }


    fun updateDutyStatusApi(context: Context,token:String,dutyStatus:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.updateDutyStatus(token,dutyStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    dutystatusData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun driverLogoutApi(context: Context,token:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverLogout(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    logoutData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun updateLocationApi(context: Context,token:String,latitude:String,longitude:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.updateLocation(token,latitude,longitude)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Utils().dismissDialog(progressDialog)
                locationData.value = it
            }, {
                Utils().dismissDialog(progressDialog)
                ErrorUtil.handlerGeneralError(context, it)
            })
    }
}