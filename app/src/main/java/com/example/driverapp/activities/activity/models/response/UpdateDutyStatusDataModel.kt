package com.example.driverapp.activities.activity.models.response


import com.google.gson.annotations.SerializedName

data class UpdateDutyStatusDataModel(
    @SerializedName("data")
    var `data`: Boolean?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: Int?)