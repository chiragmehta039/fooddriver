package com.example.driverapp.activities.activity.ui.adapter

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.orderDetail.OrderDetailActivity
import kotlinx.android.synthetic.main.layout_todays.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class TodayOrderAdapter(var context: Context): RecyclerView.Adapter<TodayOrderAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_todays, parent, false))

    }
    override fun getItemCount(): Int {
        return 2
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.burgerking.setOnClickListener {
            context.startActivity(Intent(context,OrderDetailActivity::class.java))
        }

    }


    inner class  MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {



    }



}