package com.example.driverapp.activities.activity.ui.forgotPassword

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.CheckDriverMobilForForgotPasswordDataModel
import com.example.driverapp.activities.activity.models.response.DriverForgotPasswordDataModel
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ForgotPasswordViewModel : BaseViewModel() {

    var forgotData = MutableLiveData<CheckDriverMobilForForgotPasswordDataModel>()
    var forgotcheckData = MutableLiveData<DriverForgotPasswordDataModel>()


    fun checkDriverMobilForForgotPasswordApi(context: Context,countryCode: String, mobileNumber: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.checkDriverMobilForForgotPassword(countryCode, mobileNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    forgotData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun driverForgotPasswordApi(context: Context,driverId: String, password: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverForgotPassword(driverId, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    forgotcheckData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
}