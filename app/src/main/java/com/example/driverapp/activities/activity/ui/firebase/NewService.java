package com.example.driverapp.activities.activity.ui.firebase;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.Nullable;
  
public class NewService extends Service {
  
    // declaring object of MediaPlayer
    private MediaPlayer player;
  
    @Override
  
    // execution of service will start
    // on calling this method
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(),"This is a Service running in Background",
                Toast.LENGTH_SHORT).show();






        // creating a media player which
        // will play the audio of Default
        // ringtone in android device

        // returns the status
        // of the program
        return START_STICKY;
    }
  
    @Override
  
    // execution of the service will
    // stop on calling this method
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(),"This is a Service stopped in Background",
                Toast.LENGTH_SHORT).show();



        // stopping the process
        player.stop();
    }
  
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}