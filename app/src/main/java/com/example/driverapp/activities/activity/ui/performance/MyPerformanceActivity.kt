package com.example.driverapp.activities.activity.ui.performance

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.lifeTime.LifeTimeOrderActivity
import com.example.driverapp.activities.activity.ui.orderMonth.OrderThisMonthActivity
import com.example.driverapp.activities.activity.ui.todayOrder.TodaysActivity
import kotlinx.android.synthetic.main.activity_performance2.*

class MyPerformanceActivity  : BaseActivity(), View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_performance2)
        init()
        initControl()

    }

    override fun init() {

    }

    override fun initControl() {
        todays.setOnClickListener(this)
        orders.setOnClickListener(this)
        lifetime.setOnClickListener(this)
        backIv.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.backIv->{
                onBackPressed()
            }
            R.id.todays->{
                startActivity(Intent(this,TodaysActivity::class.java))
            }
            R.id.orders->{
                startActivity(Intent(this,OrderThisMonthActivity::class.java))

            }
            R.id.lifetime->{
                startActivity(Intent(this,LifeTimeOrderActivity::class.java))

            }



        }
    }
}