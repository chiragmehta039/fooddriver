package com.example.driverapp.activities.activity.ui.notification

import Utils
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.adapter.NotifyAdapter
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.TOKEN
import kotlinx.android.synthetic.main.activity_notification1.*

class NotificationActivity1 : AppCompatActivity(),View.OnClickListener {
    private lateinit var notifyAdapter: NotifyAdapter
    lateinit var notificationViewModel: NotificationViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification1)
        backIv2.setOnClickListener(this)
        clearalltv.setOnClickListener(this)

        notificationViewModel = ViewModelProvider(this).get(NotificationViewModel::class.java)

        notificationViewModel.getDriverNotificationListApi(this,getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN,"")!!)

        notificationViewModel.notifyListData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                if(it.data==null || it.data!!.size==0){
                    textView5.visibility=View.VISIBLE
                    notifyRecVw.visibility = View.GONE
                }else {
                    textView5.visibility=View.GONE
                    clearalltv.visibility=View.VISIBLE
                    notifyRecVw.visibility = View.VISIBLE
                    notifyRecVw.setLayoutManager(LinearLayoutManager(this))
                    notifyAdapter = NotifyAdapter(this,it.data)
                    notifyRecVw.setAdapter(notifyAdapter)
                }
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })


        notificationViewModel.notifyclearData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.backIv2 ->{
                onBackPressed()
            }

            R.id.clearalltv ->{
                notificationViewModel.driverClearNotificationApi(this,getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN,"")!!)
            }
        }
    }
}