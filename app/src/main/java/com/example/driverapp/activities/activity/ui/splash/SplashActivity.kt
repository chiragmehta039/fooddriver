package com.example.driverapp.activities.activity.ui.splash

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.DEVICE_TOKEN
import com.example.driverapp.activities.activity.utils.Constants.Companion.DRIVER
import com.example.driverapp.activities.activity.utils.Constants.Companion.TOKEN
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import java.util.ArrayList


class SplashActivity : BaseActivity() {

    private lateinit var handler: Handler
    private lateinit var runnable: Runnable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        FirebaseApp.initializeApp(this)
        init()
        initControl()
  //      startNextScreen()

        val handler = Handler()

        handler.postDelayed(object : Runnable {
            override fun run() {

                if (getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN, "")!!.isEmpty()) {
                    val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(intent)
                }

                else {
                    val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                    startActivity(intent)
                }        //
//                finish()
//                handler.removeCallbacks(this)
            }
        }, 4000)
    }






    override fun init() {
        getDeviceToken(this)
    }




    override fun initControl() {
    }


    private fun startNextScreen() {
        handler = Handler(Looper.getMainLooper())
        runnable = Runnable {
            if (getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN, "")!!.isEmpty()) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        handler.postDelayed(runnable, 4000)
    }

    override fun onPause() {
        super.onPause()

    }
    fun getDeviceToken(context: Context): String {
        var token:String?=null
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(OnCompleteListener<String?> { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            token = task.result
            getSharedPreferences(DRIVER, MODE_PRIVATE).edit().putString(DEVICE_TOKEN, token).apply()


        })

        return token.toString()
    }






 private fun gethash() {


     FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
         override fun onComplete(task: Task<InstanceIdResult>) {
             if (!task.isSuccessful()) {
                 return
             }
             val auth_token: String = task.result!!.token
             Log.e("token", auth_token)
         }
     })
 }
}