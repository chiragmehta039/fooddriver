package com.example.driverapp.activities.activity.utils

interface Constants {
    companion object{

        //Shared Prefrence constants
        const val PREFRENCE_NAME = "com.e.carty"
        const val PERSISTABLE_PREFRENCE_NAME = "search_manager"
        const val ACCESS_TOKEN = "access_token"
        const val EXTRA_LOGIN = "login"
        const val EXTRA_EMAIL = "email"
        const val EXTRA_MOBILE="mobile"
        const val IMAGE = "image"
        const val EXTRA_USER_ID = "userid"
        const val IS_FORGOT = "isForgot"
        const val EXTRA_COUNTRY="country"
        const val EXTRA_COUNTRY_CODE="country_code"
        const val EXTRA_FROM = "from"
        const val DEVICE_TOKEN = "DeviceToken"
        const val TOKEN = "token"
        const val  DRIVER = "driver"
        const val  BITEME = "biteme"
        const val  DLICENCE = "DLICENCE"
        const val  DN = "DLICENCE"
        const val  DM = "DLICENCE"

        const val NOTIFICATION_STATUS = "notificationstatus"
        const val DUTY_STATUS = "driverstatus"
        const val IDS = "ids"

        const val DELAY_FOR_NEXT_SCREEN: Long = 3000
        const val DELAY_FOR_NEXT_SCREENN: Long = 4000
        const val RC_SIGN_IN = 123
        const val TERMS_AND_CONDITIONS= " http://www.tommyowendesign.com/quizlock/privacy"
        const val REPORT_PROBLEM= " http://www.tommyowendesign.com/quizlock/support"
        const val PRIVACY_POLICY= "privacyPlicy"
        const val ABOUT_US= "aboutUs"
        const val TYPE_USER= "User"
        const val HELP= "http://3.7.8.81/kirana-user-backend/api/help"
        const val NEW_MESSAGE= "NEW_MESSAGE"
        const val PASSWORD_PATTERN = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@\$%^&<>*~:`-]).{8,}\$"
        const val EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

    }
}