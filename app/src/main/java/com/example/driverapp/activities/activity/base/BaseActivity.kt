package com.example.driverapp.activities.activity.base

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.driverapp.activities.activity.utils.SharedPreferenceUtil

abstract class BaseActivity : AppCompatActivity() {



     val prefs: SharedPreferenceUtil by lazy { SharedPreferenceUtil.getInstance(applicationContext) }
    abstract fun init()
    abstract fun initControl()

    protected open fun showToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


}