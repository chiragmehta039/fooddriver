package com.example.driverapp.activities.activity.models.response

data class OrderResponse(
    val message: String,
    val status: Int
)