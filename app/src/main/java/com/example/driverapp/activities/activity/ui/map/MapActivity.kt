package com.example.driverapp.activities.activity.ui.map

import Utils
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.navCustomer.NavigateCostumerActivity
import com.example.driverapp.activities.activity.ui.profile.GPSTracker
import com.example.driverapp.activities.activity.utils.Constants
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_map.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class MapActivity : BaseActivity(), View.OnClickListener  , OnMapReadyCallback

{
     var mMap: GoogleMap? = null


    private var gpsTracker: GPSTracker? = null
    private val lat1 = 0.0

    private var longs1: kotlin.Double = 0.0
    private var Lat = 0.0
    private var Long: kotlin.Double = 0.0

    private var Lats =""
    private var Longs=""
    var orderid: String = ""
    var markerPoints: ArrayList<LatLng>? = null

    lateinit var viewModel: MapViewModel

    val PATTERN_DASH_LENGTH_PX = 20
    val PATTERN_GAP_LENGTH_PX = 15

    val DOT: PatternItem = Dot()
    val DASH: PatternItem = Dash(PATTERN_DASH_LENGTH_PX.toFloat())
    val GAP: PatternItem = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
    val PATTERN_POLYGON_ALPHA: List<PatternItem> = listOf(GAP, DASH)

    private var isApiHit = false
    private val MY_API_KEY = "AIzaSyDtuK3rI4As9AS7zk8cwxEPPgx1nplM0z0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        if(intent.hasExtra("hide"))
        {
            orderid = intent.getStringExtra("orderid").toString()!!
            viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
            viewModel.getOrderDetail(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, orderid)
            nextTv.visibility=View.GONE
            getDetailResponse()
        }

       else if (intent.hasExtra("orderid")) {
            orderid = intent.getStringExtra("orderid").toString()!!
            nextTv.visibility=View.VISIBLE

            viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
            viewModel.getOrderDetail(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, orderid)
            getDetailResponse()

        }

        init()
        initControl()
        markerPoints = ArrayList()
        userDataResponse()


    }


    fun userDataResponse()
    {
        viewModel.orderstatusresponse.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {

//                homeViewModel.getOrderList(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,"Running")


                //       img = it.data!!.profilePic

            } else {

//                homeViewModel.getOrderList(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,"Running")

            }
        })


        viewModel.mError.observe(this, androidx.lifecycle.Observer {


        })

    }

    fun getDetailResponse() {
        viewModel.orderdetail.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                it.data
                titleTv.setText(it.data.restaurant.branchNameEn)
                Lats=it.data.orderData.driverLat
                Longs=it.data.orderData.driverLong


            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

    }

    fun updateMap() {

    }


    override fun init() {
        var mapFragment: SupportMapFragment = supportFragmentManager
                .findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)
        this.gpsTracker = GPSTracker(this)

        Lat = gpsTracker?.getLatitude()!!
        Long = gpsTracker?.getLongitude()!!

    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        nextTv.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.backIv -> {
                onBackPressed()
            }
            R.id.nextTv -> {
                viewModel.getOrderStatusAPi(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,intent.getStringExtra("orderid").toString()!!,"Arrive")

                startActivity(Intent(this, NavigateCostumerActivity::class.java)
                        .putExtra("orderid", intent.getStringExtra("orderid").toString()!!)
                                .putExtra("lat", intent.getStringExtra("lat").toString()!!)
                                        .putExtra("long", intent.getStringExtra("long").toString()!!)
                                                .putExtra("title", titleTv.text.toString())
                )
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {


        mMap = googleMap

        setMarkers(Lat.toString(), Long.toString(), intent.getStringExtra("lat").toString(), intent.getStringExtra("long").toString());



//        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom( LatLng(gpsTracker?.getLatitude()!!,gpsTracker?.getLongitude()!!),16.0f));

    }


    private fun setMarkers(user_lat: String, user_long: String, doctor_lat: String, doctor_long: String) {
        mMap?.clear()
        val current = LatLng(user_lat.toDouble(), user_long.toDouble())
        val desinetion: LatLng = getLatLng(doctor_lat, doctor_long)!!
        if (!isApiHit) isApiHit = true
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 16f))
        markerPoints!!.add(current)
        if (doctor_lat.isEmpty()) {
            val Curernt = MarkerOptions()
            Curernt.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin))
            Curernt.position(current)

            mMap!!.addMarker(Curernt)
        } else {
            if (mMap != null) mMap!!.clear()
            // Creating MarkerOptions
            val Curernt = MarkerOptions()
            val Dest = MarkerOptions()
            markerPoints!!.add(desinetion)

            // Setting the position of the marker
            Curernt.position(current)
            Dest.position(desinetion)

            if (markerPoints!!.size == 2) {
                Dest.icon(BitmapDescriptorFactory.fromResource(R.drawable.userh))
                Curernt.icon(BitmapDescriptorFactory.fromResource(R.drawable.homeh)).anchor(0.5f, 0.5f)
            } else {
                Dest.icon(BitmapDescriptorFactory.fromResource(R.drawable.userh))
                Curernt.icon(BitmapDescriptorFactory.fromResource(R.drawable.homeh)).anchor(0.5f, 0.5f)
            }

            // Add new marker to the Google Map Android API V2
            mMap!!.addMarker(Curernt)
            mMap!!.addMarker(Dest)
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(current))
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 16f))

            // Checks, whether start and end locations are captured
            if (markerPoints!!.size >= 2) {
                //LatLng origin = (LatLng) markerPoints.get(0);
                // LatLng dest = (LatLng) markerPoints.get(1);

                // Getting URL to the Google Directions API
                val url: String = getDirectionsUrl(current, desinetion)!!
                Log.d("url",url)
                val downloadTask = DownloadTask()

                // Start downloading json data from Google Directions API
                downloadTask.execute(url)
            }

        }


    }

   inner class DownloadTask : AsyncTask<String?, Void?, String>() {
        // Downloading data in non-ui thread


        // Executes in UI thread, after the execution of
        // doInBackground()
        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            val parserTask = ParserTask()

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result)
        }

        override fun doInBackground(vararg params: String?): String {
            var data = ""
            try {
                // Fetching the data from web service
                data = downloadUrl(params[0]!!)!!
            } catch (e: java.lang.Exception) {
                Log.d("Background Task", e.toString())
            }
            return data
        }
        @Throws(IOException::class)
        fun downloadUrl(strUrl: String): String? {
            var data = ""
            var iStream: InputStream? = null
            var urlConnection: HttpURLConnection? = null
            try {
                val url = URL(strUrl)

                // Creating an http connection to communicate with url
                urlConnection = url.openConnection() as HttpURLConnection

                // Connecting to url
                urlConnection.connect()

                // Reading data from url
                iStream = urlConnection!!.inputStream
                val br = BufferedReader(InputStreamReader(iStream))
                val sb = StringBuffer()
                var line: String? = ""
                while (br.readLine().also { line = it } != null) {
                    sb.append(line)
                }
                data = sb.toString()
                br.close()
            } catch (e: java.lang.Exception) {
                Log.d(" while downloading url", e.toString())
            } finally {
                iStream!!.close()
                urlConnection!!.disconnect()
            }
            return data
        }
    }


  inner  class ParserTask : AsyncTask<String?, Int?, List<List<HashMap<String, String>>>?>() {

        // Parsing the data in non-ui thread

        // Executes in UI thread, after the parsing process
        fun zoomRoute(googleMap: GoogleMap?, lstLatLngRoute: List<LatLng?>?) {
            if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return
            val boundsBuilder = LatLngBounds.Builder()
            for (latLngPoint in lstLatLngRoute) boundsBuilder.include(latLngPoint)
            val routePadding = 100
            val latLngBounds = boundsBuilder.build()
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngBounds.center, 10f))

        }


        override fun doInBackground(vararg params: String?): List<List<HashMap<String, String>>>? {
            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null
            try {
                jObject = JSONObject(params[0])
                val parser = DirectionsJSONParser()
                // Starts parsing data
                routes = parser.parse(jObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return routes            }

        override fun onPostExecute(result: List<List<HashMap<String, String>>>?) {
            var points: ArrayList<LatLng?>? = null
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()

            // Traversing through all the routes
            for (i in result!!.indices) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                // Fetching i-th route
                val path = result!![i]

                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    //     ETA_TIME = point.get("time");
                    val position = LatLng(lat, lng)
                    points.add(position)
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(ContextCompat.getColor(this@MapActivity,R.color.solidpink))
                lineOptions.pattern(PATTERN_POLYGON_ALPHA)




                // Drawing polyline in the Google Map for the i-th route
                // mMap.addPolyline(lineOptions);
                if (points != null) {

                    mMap!!.addPolyline(lineOptions)
                    zoomRoute(mMap, points)
                }

            }

        }

    }


    // Fetches data from url passed






    fun getLatLng(lat: String, longs: String): LatLng? {
        return try {
            val dlat = lat.toDouble()
            val dlon = longs.toDouble()
            LatLng(dlat, dlon)
        } catch (e: Exception) {
            LatLng(0.0, 0.0)
        }
    }

    fun getDirectionsUrl(origin: LatLng, dest: LatLng): String? {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service
        val url = "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=AIzaSyAHs-QCa_fmG4dPqx6-5lnDPtUM883KtVo"
        Log.d("getDirectionsUrl: ", url)
        return url
    }

    // Fetches data from url passed

    /**
     * A method to download json data from url
     */



}