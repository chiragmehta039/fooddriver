package com.example.driverapp.activities.activity.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.models.response.OrderListResponse
import com.example.driverapp.activities.activity.ui.map.MapActivity
import com.example.driverapp.activities.activity.ui.navCustomer.NavigateCostumerActivity
import com.example.driverapp.activities.activity.ui.navigateClose.NavigateCloseActivity
import kotlinx.android.synthetic.main.layout_home.view.*
import java.text.SimpleDateFormat
import java.util.*

class HomeAdapter(var context: Context, val clickCheck: String, var data: ArrayList<OrderListResponse.Data>, var filterSelection: FilterSelection,var type:Int): RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_home, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.itemView.tv_add.setText(data.get(position).restaurantData.address)
        holder.itemView.tv_res_name.setText(data.get(position).restaurantData.branchNameEn)
        holder.itemView.tv_price.setText("AED" + " " + data.get(position).total.toString())
//        holder.itemView.earnTv.setText("AED"+" "+data.get(position).discountAmount.toString())

        if(data.get(position).status.equals("Order Ready for pickup"))
        {
            type=1
        }
        else if(data.get(position).status.equals("Out for delivery"))
        {
            type=3
        }

        if(data.get(position).paymentMode.toString().equals("Cash on Delivery"))
        {
            holder.itemView.tv_mode.setText("COD Payment")

        }
        else
        {
            holder.itemView.tv_mode.setText(data.get(position).paymentMode.toString())

        }
        try {
            val _24HourTime = data.get(position).deliveryTime
            val _24HourSDF = SimpleDateFormat("HH:mm")
            val _12HourSDF = SimpleDateFormat("hh:mm a")
            val _24HourDt = _24HourSDF.parse(_24HourTime)
            holder.itemView.tv_date.setText(data.get(position).deliveryDate + "   " +_12HourSDF.format(_24HourDt).toString())

        } catch (e: Exception) {
            e.printStackTrace()
        }
        holder.itemView.tv_c_name.setText(data.get(position).userData.firstName +" "+ data.get(position).userData.lastName)
        holder.itemView.tv_m_n.setText(data.get(position).userData.countryCode + " "+data.get(position).userData.mobileNumber)
        holder.itemView.tv_c_add.setText(data.get(position).address)
        holder.itemView.pendingTv.setText(data.get(position).status)

        if(data.get(position).arriveStatus)
        {
            holder.itemView.pendingTv.setText("Arrive")
            type=4

        }
        else
        {
            if(data.get(position).status.equals("Confirmed"))
            {
                holder.itemView.pendingTv.setText("Accepted")

            }

            else
            {
                holder.itemView.pendingTv.setText(data.get(position).status)

            }
        }


        holder.itemView.tv_num.setText("#" + data.get(position).orderNumber)

        Glide.with(context).load(data.get(position).restaurantData.image).into(holder.itemView.img_res)

        holder.itemView.pendingTv.setOnClickListener {

            if(type==0)
            {
                // fun vehicleDetails(){-
                var popup =  PopupMenu(context, holder.itemView.pendingTv)
                //Inflating the Popup using xml file
                popup.getMenu().add("On the Way");



                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {


                    override fun onMenuItemClick(item: MenuItem?): Boolean {
                        holder.itemView.pendingTv.setText(item?.getTitle())
                        filterSelection.onPostionClick(item?.getTitle() as String, data.get(position)._id,data.get(position).latitude,data.get(position).longitude)
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

           else if(type==3)
            {
                // fun vehicleDetails(){-
                var popup =  PopupMenu(context, holder.itemView.pendingTv)
                //Inflating the Popup using xml file
                popup.getMenu().add("Delivered");



                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {


                    override fun onMenuItemClick(item: MenuItem?): Boolean {
                        holder.itemView.pendingTv.setText(item?.getTitle())
                        filterSelection.onPostionClick(item?.getTitle() as String, data.get(position)._id,data.get(position).latitude,data.get(position).longitude)
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
            else if(type==4)
            {
                // fun vehicleDetails(){-
                var popup =  PopupMenu(context, holder.itemView.pendingTv)
                //Inflating the Popup using xml file
                popup.getMenu().add("Out for delivery");
                popup.getMenu().add("Delivered");


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {


                    override fun onMenuItemClick(item: MenuItem?): Boolean {
                        holder.itemView.pendingTv.setText(item?.getTitle())
                        filterSelection.onPostionClick(item?.getTitle() as String, data.get(position)._id,data.get(position).latitude,data.get(position).longitude)
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
            else
            {
                // fun vehicleDetails(){
                var popup =  PopupMenu(context, holder.itemView.pendingTv)
                //Inflating the Popup using xml file
                popup.getMenu().add("On the Way");
                popup.getMenu().add("Out for delivery");
                popup.getMenu().add("Delivered");


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {


                    override fun onMenuItemClick(item: MenuItem?): Boolean {
                        holder.itemView.pendingTv.setText(item?.getTitle())
                        filterSelection.onPostionClick(item?.getTitle() as String, data.get(position)._id,data.get(position).latitude,data.get(position).longitude)
                        return true;
                    }
                });

                popup.show();//showing p
            }






            // }
        }





        when(clickCheck){
            "active" -> {
                holder.itemView.deliveredTv.visibility = View.GONE
                holder.itemView.earnTv.visibility = View.GONE
                holder.itemView.pendingTv.visibility = View.VISIBLE
                holder.itemView.deliveryStatus.visibility = View.VISIBLE
            }
            "past" -> {
                holder.itemView.deliveredTv.visibility = View.VISIBLE
                holder.itemView.earnTv.visibility = View.VISIBLE
                holder.itemView.pendingTv.visibility = View.GONE
                holder.itemView.navigation.visibility = View.GONE
                holder.itemView.nav_cust.visibility=View.GONE
                holder.itemView.deliveryStatus.visibility = View.GONE

            }
            "rejected" -> {
                holder.itemView.deliveredTv.text = "Rejected"
                holder.itemView.deliveredTv.setTextColor(ContextCompat.getColor(context, R.color.colorred))
                holder.itemView.deliveredTv.visibility = View.VISIBLE
                holder.itemView.pendingTv.visibility = View.GONE
                holder.itemView.nav_cust.visibility=View.GONE

                holder.itemView.navigation.visibility = View.GONE
                holder.itemView.deliveryStatus.visibility = View.GONE


            }
        }

        holder.itemView.nav_cust.setOnClickListener {

            if(data.get(position).status.equals("Order Ready for pickup"))
            {
                context.startActivity(Intent(context, NavigateCostumerActivity::class.java)
                        .putExtra("orderid", data.get(position)._id)

                )            }
            else if(data.get(position).status.equals("Out for delivery"))
            {
                context.startActivity(Intent(context, NavigateCloseActivity::class.java)
                        .putExtra("orderid", data.get(position)._id)
                        .putExtra("title", data.get(position).restaurantData.branchNameEn)


                )
            }
            else
            {
                context.startActivity(Intent(context, MapActivity::class.java)
                        .putExtra("orderid", data.get(position)._id)
                        .putExtra("lat", data.get(position).latitude)
                        .putExtra("long", data.get(position).longitude)
                        .putExtra("hide","hide"))
            }





        }

        holder.itemView.navigation.setOnClickListener {

            context.startActivity(Intent(context, MapActivity::class.java)
                    .putExtra("orderid", data.get(position)._id)
                    .putExtra("lat", data.get(position).latitude)
                    .putExtra("long", data.get(position).longitude)
            )










    }}


    interface FilterSelection {
        fun onPostionClick(title: String, id: String,lat:String,long:String)
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



    }

}






