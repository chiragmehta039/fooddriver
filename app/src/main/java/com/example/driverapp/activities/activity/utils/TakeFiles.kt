package com.example.driverapp.activities.activity.utils

import android.Manifest
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.provider.Settings
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.driverapp.activities.activity.ui.signupTwo.SignUpTwoActivity
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by mobua01 on 6/2/19.
 */
class TakeFiles  //   NO-ARG CONSTRUCTOR
    : AppCompatActivity() {
    private val TAG = "TakeFiles"

    //   START ACTIVITY FOR RESULT: REQUEST CODES
    private val PDF_REQ_CODE = 14
    private val DOC_REQ_CODE = 16

    //  REQUEST PERMISSIONS: REQUEST CODES
    private val REQ_CODE_ASK_ALL_PERMISSIONS = 20
    private val mediaDirectory =
        (Environment.getExternalStorageDirectory().toString() + File.separator + "Cause Finder"
                + File.separator + "Documents" + File.separator)
    private var currentPDFPath = ""
    private var currentDOCPath = ""
    private var pdfURI: Uri? = null
    private var docURI: Uri? = null
    private var wantWhat: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val receivedIntent = intent
        wantWhat = receivedIntent.getStringExtra(WANT)
        if (wantWhat == null) {
            wantWhat = WANT_PDF // BY DEFAULT WE PROVIDE PDF
        } else {
            wantWhat = receivedIntent.getStringExtra(WANT) // GIVEN BY PROGRAMMER
        }
        if (wantWhat != null) {
//          start CHECKING FOR RUNTIME PERMISSIONS
            doAllPermissionChecking()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do your stuff
            startActivity(Intent(this, SignUpTwoActivity::class.java))
        }
        return super.onKeyDown(keyCode, event)
    }

    //    METHOD:TO START TAKING PHOTOS via DISPATCHING CAMERA AND GALLERY INTENTS BASED ON fromWhere TYPE
    private fun startPickingFiles() {
        if (wantWhat != null) {
            if (wantWhat.equals(WANT_PDF, ignoreCase = true)) {
                dispatchPDFIntent()
            } else if (wantWhat.equals(WANT_DOCUMENT, ignoreCase = true)) {
                dispatchDOCIntent()
            }
        }
    }

    //    METHOD:TO DISPATCH GALLERY INTENT TO CHOOSE IMAGE FROM GALLERY
    private fun dispatchPDFIntent() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "application/pdf"
        try {
            startActivityForResult(intent, PDF_REQ_CODE)
        } catch (e: ActivityNotFoundException) {
            //alert user that file manager not working
            Toast.makeText(this@TakeFiles, "Unable to pick a pdf file.", Toast.LENGTH_SHORT).show()
            finish()
        }

//        if (intent.resolveActivity(getPackageManager()) != null) {
//
//            intent.setType("application/pdf");
//            try {
//                startActivityForResult(intent, PDF_REQ_CODE);
//            } catch (ActivityNotFoundException e) {
//                //alert user that file manager not working
//                Toast.makeText(TakeFiles.this, "Unable to pick a pdf file.", Toast.LENGTH_SHORT).show();
//            }
//
//        } else {
//            Toast.makeText(this, "No File Manager app found", Toast.LENGTH_SHORT).show();
//            this.finish();
//        }
    }

    //     METHOD:TO DISPATCH CAMERA INTENT TO CAPTURE IMAGE FROM CAMERA
    private fun dispatchDOCIntent() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        //        String[] mimeTypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
//                "application/msword","application/pdf"};
        //intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        try {
            startActivityForResult(intent, DOC_REQ_CODE)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this@TakeFiles, "Unable to pick a doc file.", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    //   METHOD: TO CREATE GALLERY IMAGE FILE COPY  & GET IMAGE FILE_PATH
    private fun savePDFFile(): String {
        var imagePath = ""
        val mediaStorageDir: File
        // Create an image fileName
        val s = SimpleDateFormat("yyyyMMdd_HHmmmss", Locale.getDefault())
        s.timeZone = TimeZone.getTimeZone("GMT")
        val timeStamp = s.format(Date())
        val imageFileName = "PDF_$timeStamp.pdf"

//        FOR PRIMARY EXTERNAL STORAGE (SHAREABLE) DATA path OF APP
        if (isExternalStorageAvailable) {
            mediaStorageDir = File(mediaDirectory)
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Failed to create directory")
            }
            val imgFile = File(mediaStorageDir.absolutePath + File.separator + imageFileName)
            imagePath = imgFile.absolutePath
        }
        currentPDFPath = imagePath
        Log.w(TAG, "currentPDFPath: $imagePath")
        return imagePath
    }

    //   METHOD: TO CREATE GALLERY IMAGE FILE COPY  & GET IMAGE FILE_PATH
    private fun saveDocFile(): String {
        var imagePath = ""
        val mediaStorageDir: File
        // Create an image fileName
        val s = SimpleDateFormat("yyyyMMdd_HHmmmss", Locale.getDefault())
        s.timeZone = TimeZone.getTimeZone("GMT")
        val timeStamp = s.format(Date())
        val imageFileName = "DOC_$timeStamp.doc"

//        FOR PRIMARY EXTERNAL STORAGE (SHAREABLE) DATA path OF APP
        if (isExternalStorageAvailable) {
            mediaStorageDir = File(mediaDirectory)
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Failed to create directory")
            }
            val imgFile = File(mediaStorageDir.absolutePath + File.separator + imageFileName)
            imagePath = imgFile.absolutePath
        }
        currentDOCPath = imagePath
        Log.w(TAG, "currentDOCPath: $imagePath")
        return imagePath
    }

    // Returns true if Primary External Storage is available
    private val isExternalStorageAvailable: Boolean
        private get() {
            val state = Environment.getExternalStorageState()
            return if (Environment.MEDIA_MOUNTED == state) {
                true
            } else {
                Toast.makeText(this, "PRIMARY EXTERNAL STORAGE NOT MOUNTED.", Toast.LENGTH_SHORT)
                    .show()
                false
            }
        }
    // 5.1 and above
    //             drive
    //MyProfileMemberFrag: uri : content://com.google.android.apps.docs.storage/document/acc%3D1%3Bdoc%3D6234
    //MyProfileMemberFrag: PDF filename: Les Finale Add One - Android.pdf
    //downloads
    //                 MyProfileMemberFrag: uri : content://com.android.providers.downloads.documents/document/3153
    //                 MyProfileMemberFrag: PDF filename: dummy.pdf
    //                    MyProfileMemberFrag: uri : content://com.estrongs.files/storage/emulated/0/Download/dummy.pdf
    //                    MyProfileMemberFrag: PDF filename: dummy.pdf
    //    TakeFiles: PDF URI: content://com.android.providers.downloads.documents/document/3153
    //    TakeFiles: PDF filename: dummy.pdf
    //    TakeFiles: currentPDFPath: /storage/emulated/0/Cause Finder/Documents/PDF_20171107_1001058.pdf
    //    TakeFiles: FROM PDF: true
    //                    4.4
    //downloads
    //                  MyProfileMemberFrag: uri : content://com.android.providers.downloads.documents/document/3559
    //                  MyProfileMemberFrag: PDF filename: DL3SBL9396.pdf
    //sd card
    //                  MyProfileMemberFrag: uri : content://com.android.externalstorage.documents/document/6335-3032%3Aintegration_formulas.pdf
    //                  MyProfileMemberFrag: PDF filename: integration_formulas.pdf
    ///*
    // GALLERY:
    // W/TakePhoto: CurrentPhotoPath: /storage/emulated/0/DCIM/100PINT/Pins/f206b39871ba10e38fd30d20d79cb6b7.jpg
    // W/TakePhoto: PhotoURI: content://media/external/images/media/134173
    // W/TakePhoto: imagePath: /storage/emulated/0/Skylist/Pictures/PIC_20170608_0700636.jpg
    //
    //CAMERA:
    //    (for API > Marshmallow+ ):
    // W/TakePhoto: CurrentPhotoPath: /storage/emulated/0/Skylist/Pictures/PIC_20170608_0702815.jpg
    // W/TakePhoto: PhotoURI: content://com.skylist.skylist.fileprovider/external_files/Skylist/Pictures/PIC_20170608_0702815.jpg
    //      (for API = Pre-Marshmallow : i.e. kitkat,lollipop):
    //    W/TakePhoto: CurrentPhotoPath: /storage/emulated/0/Skylist/Pictures/PIC_20170609_0503624.jpg
    //    W/TakePhoto: PhotoURI: file:///storage/emulated/0/Skylist/Pictures/PIC_20170609_0503624.jpg
    //    URI CREATION--->>
    //uri: FileProvider.getUriForFile(getActivity(),  BuildConfig.APPLICATION_ID + ".fileprovider", imgFile)
    //uri: content://com.skylist.skylist.fileprovider/external_files/Skylist/Pictures/PIC_20170608_0602815.jpg
    //
    //
    //uri: Uri.fromFile(imgFile)
    //uri: file:///storage/emulated/0/Skylist/Pictures/PIC_20170608_0602815.jpg
    //
    /*********** startActivityForResult() CALLBACK    */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PDF_REQ_CODE -> if (resultCode == RESULT_OK) {
                try {
                    pdfURI = data!!.data
                    val pdfFileName = getFileName(this@TakeFiles, pdfURI)
                    Log.w(TAG, "PDF URI: " + pdfURI.toString())
                    Log.w(TAG, "PDF filename: $pdfFileName")
                    if (pdfFileName!!.contains(".pdf")) {
                        savePDFFile()

//                    GIVE BACK RESULTING BITMAP, FILEPATH, ETC TO CALLING ACTIVITY
                        giveResultsBack(pdfURI, currentPDFPath, true)
                    } else {
                        currentPDFPath = ""
                        pdfURI = null
                        setResult(RESULT_CANCELED)
                        Toast.makeText(
                            this@TakeFiles,
                            "Chosen file is not of PDF Type!",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    currentPDFPath = ""
                    pdfURI = null
                    setResult(RESULT_CANCELED)
                    finish()
                }
            } else {
                currentPDFPath = ""
                pdfURI = null
                // Toast.makeText(TakeFiles.this, "You have not picked PDF.", Toast.LENGTH_SHORT).show();
                startActivity(Intent(this, SignUpTwoActivity::class.java))
            }
            DOC_REQ_CODE -> if (resultCode == RESULT_OK) {
                try {
                    docURI = data!!.data
                    val docFileName = getFileName(this@TakeFiles, docURI)
                    Log.w(TAG, "DOC URI: " + docURI.toString())
                    Log.w(TAG, "DOC filename: $docFileName")
                    if (docFileName!!.contains(".doc") || docFileName.contains(".docx")) {
                        saveDocFile()

//                    GIVE BACK RESULTING BITMAP, FILEPATH, ETC TO CALLING ACTIVITY
                        giveResultsBack(docURI, currentDOCPath, false)
                    } else if (docFileName.contains(".pdf")) {
                        savePDFFile()

//                    GIVE BACK RESULTING BITMAP, FILEPATH, ETC TO CALLING ACTIVITY
                        giveResultsBack(docURI, currentPDFPath, false)
                    } else {
                        currentDOCPath = ""
                        docURI = null
                        setResult(RESULT_CANCELED)
                        finish()
                        // Toast.makeText(TakeFiles.this, "Chosen file is not of DOC or DOCX Type!", Toast.LENGTH_SHORT).show();
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    currentDOCPath = ""
                    docURI = null
                    setResult(RESULT_CANCELED)
                    finish()
                }
            } else {
                currentDOCPath = ""
                docURI = null
                //Toast.makeText(TakeFiles.this, "You have not picked DOCx.", Toast.LENGTH_SHORT).show();
                finish()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun compressRecycle(path: String, isFromPDF: Boolean) {
        if (isFromPDF) //PDF compression
        {
            //Write file on disk, later decode this file stream in other Activity ,when retrieving image
            try {
                val inputStream = contentResolver.openInputStream(pdfURI!!)
                val outputStream: FileOutputStream
                outputStream = FileOutputStream(File(path))
                val buffer = ByteArray(1024)
                var length: Int
                /*copying the contents from input stream to
                 * output stream using read and write methods
                 */if (inputStream != null) {
                    while (inputStream.read(buffer).also { length = it } > 0) {
                        outputStream.write(buffer, 0, length)
                    }
                }

//              Cleanup and Closing the input/output file streams
                inputStream?.close()
                outputStream.flush()
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else  //DOC compression
        {
            //Write file on disk, later decode this file stream in other Activity ,when retrieving image
            try {
                val inputStream = contentResolver.openInputStream(docURI!!)
                val outputStream: FileOutputStream
                outputStream = FileOutputStream(File(path))
                val buffer = ByteArray(1024)
                var length: Int
                /*copying the contents from input stream to
                 * output stream using read and write methods
                 */if (inputStream != null) {
                    while (inputStream.read(buffer).also { length = it } > 0) {
                        outputStream.write(buffer, 0, length)
                    }
                }

//              Cleanup and Closing the input/output file streams
                inputStream?.close()
                outputStream.flush()
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    //  METHOD: TO GIVE BACK RESULTING BITMAP, FILEPATH, ETC TO CALLING ACTIVITY
    private fun giveResultsBack(photoUri: Uri?, currentPhotoPath: String, fromPDF: Boolean) {
        try {
            if (!currentPhotoPath.isEmpty() && photoUri != null) {
                Log.w(TAG, "FROM PDF: $fromPDF")
                if (fromPDF) {
                    compressRecycle(currentPhotoPath, true)
                } else {
                    compressRecycle(currentPhotoPath, false)
                }

                /*Pop the intent*/
                val resultIntent = Intent()
                val bundle = Bundle()
                bundle.putParcelable("uri", photoUri)
                bundle.putString("path", currentPhotoPath)
                resultIntent.putExtras(bundle)
                setResult(RESULT_OK, resultIntent)
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*************** checking MULTIPLE RUNTIME PERMISSION *** --STARTS here--  */
    private fun doAllPermissionChecking() {
        val permissionsNeededForNow: MutableList<String> = ArrayList()
        val permissionsList: MutableList<String> = ArrayList()
        if (!addPermission(
                permissionsList,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) permissionsNeededForNow.add("Read Storage")
        if (!addPermission(
                permissionsList,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) permissionsNeededForNow.add("Write Storage")


//        for Pre-Marshmallow the permissionsNeeded.size() will always be 0; , if clause don't run  Pre-Marshmallow
        if (permissionsList.size > 0) {
            if (permissionsNeededForNow.size > 0) {
                // Need Rationale
                var message = "You need to grant access to " + permissionsNeededForNow[0]
                for (i in 1 until permissionsNeededForNow.size) message =
                    message + ", " + permissionsNeededForNow[i]
                showMessageOKCancel(message, DialogInterface.OnClickListener { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@TakeFiles,
                        permissionsList.toTypedArray(),
                        REQ_CODE_ASK_ALL_PERMISSIONS
                    )
                })
                return
            }
            ActivityCompat.requestPermissions(
                this@TakeFiles,
                permissionsList.toTypedArray(),
                REQ_CODE_ASK_ALL_PERMISSIONS
            )
            return
        }

//        start doing things if all PERMISSIONS are Granted whensoever
//        for Marshmallow+ and Pre-Marshmallow both
        startPickingFiles()
    }

    ////    adding RUNTIME PERMISSION to permissionsList and checking If user has GRANTED Permissions or Not  /////
    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        // Marshmallow+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this@TakeFiles,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@TakeFiles,
                        permission
                    )
                ) return false
            }
        }
        // Pre-Marshmallow
        return true
    }

    //    Handle "Don't / Never Ask Again" when asking permission
    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@TakeFiles)
            .setMessage(message)
            .setPositiveButton("Ok", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    // *********** PERMISSION GRANTED FUNCTIONALITY ***********
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQ_CODE_ASK_ALL_PERMISSIONS -> {
                val permissionsMap: MutableMap<String, Int> = HashMap()
                // Initial
                permissionsMap[Manifest.permission.READ_EXTERNAL_STORAGE] =
                    PackageManager.PERMISSION_GRANTED
                permissionsMap[Manifest.permission.WRITE_EXTERNAL_STORAGE] =
                    PackageManager.PERMISSION_GRANTED

                // Fill with results
                var i = 0
                while (i < permissions.size) {
                    permissionsMap[permissions[i]] = grantResults[i]
                    i++
                }
                // Check for permissions
                if (permissionsMap[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    && permissionsMap[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                ) {

                    // All Permissions Granted at once
                    // do start recording for Marshmallow+ case
                    startPickingFiles()
                } else {
                    // Permission Denied
                    Toast.makeText(
                        this@TakeFiles,
                        "Storage Permission is denied. Allow it in App settings",
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts(
                        "package",
                        this@TakeFiles.applicationContext.packageName,
                        null
                    )
                    intent.data = uri
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    } /* ************** checking MULTIPLE RUNTIME PERMISSION **** --ENDS here-- ***********/

    companion object {
        var WANT = "want"
        var WANT_PDF = "want_pdf"
        var WANT_DOCUMENT = "want_docs"
        fun getFileName(context: Context, uri: Uri?): String? {
            var result: String? = null
            if (uri!!.scheme == "content") {
                val cursor = context.contentResolver.query(uri, null, null, null, null)
                try {
                    if (cursor != null && cursor.moveToFirst()) {
                        result =
                            cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    }
                } finally {
                    cursor?.close()
                }
            }
            if (result == null) {
                result = uri.lastPathSegment
            }
            return result
        }
    }
}