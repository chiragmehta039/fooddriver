package com.icontractor.location

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.icontractor.location.LocationClass

class LocationClass(private val mContext: Context) : LocationCallback(), LocationListener {
    var isGPSEnabled = false
    var isNetworkEnabled = false
    var canGetLocation = false
    var location: Location? = null
    var latitude = 0.0
    var longitude = 0.0
    protected var locationManager: LocationManager? = null
    private val m_Location: Location?
    var mActivity: Activity
    var updateLocationListener: UpdateLocationListener? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    interface UpdateLocationListener {
        fun onLocation(location: LocationResult?)
    }

    fun getlocation(): Location? {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //          }
            try {
                locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                isGPSEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
                isNetworkEnabled = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                } else {
                    val locationRequest = LocationRequest()
                    locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
                    locationRequest.interval = LongLocationInterval.toLong()
                    locationRequest.fastestInterval = FastestLocationInterval.toLong()
                    fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext)
                    fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, this, Looper.myLooper())
                    canGetLocation = true
                    if (isNetworkEnabled) {
//                        locationManager.requestLocationUpdates(
//                                LocationManager.NETWORK_PROVIDER,
//                                MIN_TIME_BW_UPDATES,
//                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//                        Log.d("Network", "Network Enabled");
                        if (locationManager != null) {
                            location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                            if (location != null) {
                                latitude = location!!.latitude
                                longitude = location!!.longitude
                            } else {
                                getlocation()
                                return null
                            }
                        }
                    }
                    if (isGPSEnabled) {
                        if (location == null) {
//                            locationManager.requestLocationUpdates(
//                                    LocationManager.GPS_PROVIDER,
//                                    MIN_TIME_BW_UPDATES,
//                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//                            Log.d("GPS", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager!!
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                if (location != null) {
                                    latitude = location!!.latitude
                                    longitude = location!!.longitude
                                } else {
                                    getlocation()
                                    return null
                                }
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return location
    }

    fun setUpdateLocationListener() {
        updateLocationListener = updateLocationListener
    }

    fun stopUsingGPS() {
//        if (locationManager != null) {
//            locationManager.removeUpdates(GPSTracker.this);
//        }
    }

    fun canGetLocation(): Boolean {
        return canGetLocation
    }

    override fun onLocationChanged(location: Location) {
        if (location != null) {
            latitude = location.latitude
            longitude = location.longitude
        }
        Log.w("testData", "location changed!")
    }

    override fun onProviderDisabled(arg0: String) {
        // TODO Auto-generated method stub
    }

    override fun onProviderEnabled(arg0: String) {
        // TODO Auto-generated method stub
    }

    override fun onStatusChanged(arg0: String, arg1: Int, arg2: Bundle) {
        // TODO Auto-generated method stub
    }

    override fun onLocationResult(locationResult: LocationResult) {
        super.onLocationResult(locationResult)
        if (updateLocationListener != null) {
            updateLocationListener!!.onLocation(locationResult)
            Log.w("testData", "location changed!")
        }
        Log.w("testData", "locatin send")
    }

    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings")

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings") { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent)
        }
        alertDialog.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        alertDialog.show()
    }

    companion object {
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 0 // 10 meters
        private const val MIN_TIME_BW_UPDATES: Long = 0 // 1 minute
        const val LongLocationInterval = 1000
        const val FastestLocationInterval = 1000
    }

    init {
        mActivity = mContext as Activity
        m_Location = getlocation()
    }
}