package com.example.driverapp.activities.activity.ui.signup

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.CheckDriverMobileAndEmailDataModel
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.DriverSignupDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class SignupViewModel : BaseViewModel() {
    var signUpData = MutableLiveData<DriverSignupDataModel>()
    var signUpDataCheck = MutableLiveData<CheckDriverMobileAndEmailDataModel>()

    fun signUpApi(context: Context, email: String, countryCode: String, mobileNumber: String, firstname: String, lastname: String,
                  password: String, drivingLicence: String, nationalIdentity: String, policeVerification: String, document: String,
                  vehicleType: String, vehicleBrand: String, vehicleModel: String, vehicleNumber: String, manufacturingDate: String,
                  vehicleLicence: String, vehicleDocument: String, deviceToken: String, deviceType: String, dutyStartTime: String,dutyEndTime: String,passport:String,dob:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverSignup(email,countryCode,mobileNumber,nationalIdentity,firstname,lastname,password,drivingLicence,"nationalIdentity",policeVerification,document,
                vehicleType,vehicleBrand,vehicleModel,vehicleNumber,manufacturingDate,vehicleLicence,vehicleDocument,deviceToken,deviceType,dutyStartTime,dutyEndTime,passport,dob,firstname)
                .subscribeOn(Schedulers.io())

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    signUpData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }


    fun checkDriverMobileAndEmailApi(context: Context,mobileNumber: String, email: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.checkDriverMobileAndEmail(email,mobileNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    signUpDataCheck.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
}