package com.example.driverapp.activities.activity.ui.settings

//import com.example.driverapp.activities.activity.ui.contact.ContactUsActivity
import Utils
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.ChangePasswordActivity
import com.example.driverapp.activities.activity.ui.resetPassword.ResetPasswordActivity
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.NOTIFICATION_STATUS
import com.example.driverapp.activities.activity.utils.Constants.Companion.TOKEN
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity(), View.OnClickListener{
    lateinit var settingsViewModel: SettingsViewModel
    private var notifyStatus = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        init()
        initControl()
    }



    override fun init() {
        notifyStatus = getSharedPreferences(BITEME, MODE_PRIVATE).getString(NOTIFICATION_STATUS, "")!!

        if (notifyStatus.equals("false", ignoreCase = true)) {
            togBtn1.setChecked(false)
        } else {
            togBtn1.setChecked(true)
        }

        settingsViewModel = ViewModelProvider(this).get(SettingsViewModel::class.java)

        settingsViewModel.drivernotifyData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        changePass.setOnClickListener(this)

        togBtn1.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(NOTIFICATION_STATUS, "true").apply()
                settingsViewModel.driverNotificationSettingApi(this,getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN, "")!!,"true")
            } else {
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(NOTIFICATION_STATUS, "false").apply()
                settingsViewModel.driverNotificationSettingApi(this,getSharedPreferences(BITEME, MODE_PRIVATE).getString(TOKEN, "")!!,"false")
            }
        })
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.backIv -> {
                onBackPressed()
            }

            R.id.changePass -> {
                val i = Intent(this, ChangePasswordActivity::class.java)
                i.putExtra("change", true)
                startActivity(i)
            }
        }
    }
}