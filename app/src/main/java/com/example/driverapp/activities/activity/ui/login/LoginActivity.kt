package com.example.driverapp.activities.activity.ui.login

import Utils
import Utils.Companion.checkMailAddress
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.forgotPassword.ForgotPassword
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.signup.SignupActivity
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.DEVICE_TOKEN
import com.example.driverapp.activities.activity.utils.Constants.Companion.DRIVER
import com.example.driverapp.activities.activity.utils.Constants.Companion.DUTY_STATUS
import com.example.driverapp.activities.activity.utils.Constants.Companion.IDS
import com.example.driverapp.activities.activity.utils.Constants.Companion.IMAGE
import com.example.driverapp.activities.activity.utils.Constants.Companion.NOTIFICATION_STATUS
import com.example.driverapp.activities.activity.utils.Constants.Companion.TOKEN
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.countryCodePicker
import kotlinx.android.synthetic.main.activity_login.show
import kotlinx.android.synthetic.main.activity_profile_two.*
import me.ibrahimsn.lib.Constants


class LoginActivity : BaseActivity(),View.OnClickListener, TextWatcher {
    lateinit var loginViewModel: LoginViewModel
    private var showcheck = false
    private var backButtonCount = 0
    var keyDel=0
    var phoneNumber : PhoneNumberFormattingTextWatcher?=null
    var watcher: TextWatcher ?=null
    var isListnerAdded :Boolean=false




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        initControl()

    //    phone.addTextChangedListener(PhoneNumberFormattingTextWatcher())




            }

    override fun onResume() {
        super.onResume()
//        phone.addTextChangedListener(object : TextWatcher {
//
//
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                phone.setOnKeyListener(object : View.OnKeyListener{
//                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
//                        if (keyCode == KeyEvent.KEYCODE_DEL) {
//                            keyDel = 1;
//                        }
//                        return false;
//                    }
//
//
//                })
//                     if(phone.text.length<2)
//                {
//                    keyDel=0
//                }
//                try{
//
//                        phone.text.toString().toLong()
//                    var cou=   countryCodePicker.selectedCountryCodeWithPlus
//                    if(cou.equals("+91"))
//                    {
//                        if(keyDel==0) {
//
//
//                                if (phone.text?.length == 3 ) {
//
//
//                                        phone.setText(phone.text.toString() + "-")
//
//                                        phone.setSelection(phone.text.length)
//                                    keyDel=1
//
//
//                                }
//
//
//
//
//
//                        }
//                    }
//                    else
//                    {
//                        phone.text.toString().toLong()
//                        if(keyDel==0) {
//
//
//                            if (phone.text?.length == 2 ) {
//
//
//                                phone.setText(phone.text.toString() + "-")
//
//                                phone.setSelection(phone.text.length)
//                                keyDel=1
//
//
//                            }
//
//
//
//                            }
//
//                    }
//
//                } catch (e : Exception){
//
//                }
//
//            }
//
//            override fun afterTextChanged(arg0: Editable) {
//            }
//
//            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
//            }
//        })
//
//                try{
//                    Log.e("isMobile","true")
//                    s.toString().replace(" - ","").replace(" ","").toLong()
//
//
//
//                    if(keyDel==0){
//                        if (s?.length == 2) {
//                            phone.setText("$s - ")
//                            phone.setSelection(phone.length())
//                        } else if (s?.length == 8) {
//                            phone.setText("$s ")
//                            phone.setSelection(phone.length())
//                            keyDel = 1;
//
//                        }
//                    }else{
//                        keyDel=0
//
//                    }
//                }catch (e: Exception){
//                    Log.e("isEmail","true")
//
//                    e.printStackTrace()
//                }









    }



        //  phone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
//        phone.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                phone.setOnKeyListener(object : View.OnKeyListener {
//                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
//                        return false
//                    }
//                })
//                    val len: Int = phone.getText().length
//                    if (len == 3) {
//                        phone.setText(phone.getText().toString() + "-")
//                        phone.setSelection(phone.getText().length)
//                    }
//
//            }
//
//            override fun afterTextChanged(arg0: Editable) {
//                // TODO Auto-generated method stub
//            }
//
//            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
//                // TODO Auto-generated method stub
//            }
//        })





    override fun init() {
        spannableText()
        phoneNumber=PhoneNumberFormattingTextWatcher()

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        loginViewModel.loginData.observe(this, Observer {
            if (it!!.status == 200) {
                showToast("User Login Successfully!")
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(TOKEN, it.data!!.jwtToken).apply()
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(NOTIFICATION_STATUS, it.data!!.notificationStatus).apply()
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(DUTY_STATUS, it.data!!.dutyStatus).apply()
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(IDS, it.data!!.id).apply()

                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(IMAGE, it.data!!.profilePic).apply()
                val i = Intent(this, HomeActivity::class.java)
                i.putExtra("login", true)
                startActivity(i)
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    fun mobileNo(mobileNo: CharSequence) : String? {
        return mobileNo.toString().replace("[^\\d]".toRegex(), "")
    }

    override fun initControl() {

        forgotPass.setOnClickListener(this)
        signupTv.setOnClickListener(this)
        login.setOnClickListener(this)
        show.setOnClickListener(this)
        watcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                if(s?.length!!>0){
                    try {
                        val long: Long = s.toString().toLong()
                        countryCodePicker.visibility = View.VISIBLE
                        imageView41.visibility = View.INVISIBLE
                        phone.removeTextChangedListener(watcher)
                        if(!isListnerAdded) {
                        isListnerAdded = true


                        phone.addTextChangedListener(phoneNumber)
                   }
                    }catch (e: Exception){
                        countryCodePicker.visibility = View.INVISIBLE
                        imageView41.visibility = View.VISIBLE
                    }

                }else{
                    countryCodePicker.visibility = View.VISIBLE
                    imageView41.visibility = View.INVISIBLE
                }



//
//                if(s.length>0){
//                    try{
//                        mobileNo(s)?:"".toLong()
//
//                        phone.removeTextChangedListener(watcher)
//                        countryCodePicker.visibility = View.VISIBLE
//                        imageView41.visibility = View.INVISIBLE
//                        if(!isListnerAdded) {
//                        isListnerAdded = true
//
//
//                        phone.addTextChangedListener(phoneNumber)
//                    }
//
//                    }catch (e:java.lang.Exception){
//                        showToast("yesss")
//                        countryCodePicker.visibility = View.INVISIBLE
//                        imageView41.visibility = View.VISIBLE
//                    }
//
//                }
//                else
//                {
//                    countryCodePicker.visibility = View.INVISIBLE
//                    imageView41.visibility = View.VISIBLE
//                }

            }
        }
        phone.addTextChangedListener(watcher)

    }
    fun mobileNo(mobileNo: EditText) : String? {
        return mobileNo.text.toString().replace("[^\\d]".toRegex(), "")
    }
    private fun createNumberFormat(number: String): String {
        var format = number.replace("(\\d)".toRegex(), Constants.KEY_DIGIT.toString())
        format = format.replace("(\\s)".toRegex(), Constants.KEY_SPACE.toString())
        return format
    }



    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.forgotPass -> {
                startActivity(Intent(this, ForgotPassword::class.java))
            }

            R.id.login -> {
                var  input: String = phone.getText().toString()
                if(input.contains("@"))
                {
                    input


                }
                else
                {
                 input= mobileNo(phone.getText().toString()).toString();
                }



                if (input.isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter the email or phone number!", 1, false)
                } else if (!onlyDigits(input,input.length)) {
                    if (!checkMailAddress(phone.getText().toString().trim())) {
                        Utils().getMessageDialog(this, "Invalid Email Address!", 0, false)
                    } else {
                        if (password1.text.toString().trim().isEmpty()) {
                            Utils().getMessageDialog(this, "Please enter password!", 1, false)
                        } else {
                            Log.d("token",getSharedPreferences(DRIVER, MODE_PRIVATE).getString(DEVICE_TOKEN, "")!!)
                            loginViewModel.loginApi(this, password1.text.toString().trim(), "", input, getSharedPreferences(DRIVER, MODE_PRIVATE).getString(DEVICE_TOKEN, "")!!, "Android")
                        }
                    }
                } else if (phone.text.toString().trim().length < 7)
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits." , 1, false)
                else if (phone.text.toString().trim().length > 15)
                    Utils().getMessageDialog(this, "Phone number must contain minimum 7 digits and maximum 15 digits.", 1, false)
                else if (password1.text.toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please enter password!", 1, false)
                }
                else if (password1.text.toString().trim().length < 6 && password1.text.toString().trim().length> 16)
                {
                    Utils().getMessageDialog(this, "Password must contain minimum 6 characters and maximum 16 characters", 1, false)

                }



                else {
                    loginViewModel.loginApi(this, password1.text.toString().trim(), countryCodePicker.selectedCountryCodeWithPlus, input.toString().trim(), getSharedPreferences(DRIVER, MODE_PRIVATE).getString(DEVICE_TOKEN, "")!!, "Android")
                }
            }
            R.id.show -> {
                if (showcheck) {
                    password1.setTransformationMethod(PasswordTransformationMethod.getInstance())
                    show.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hide))
                    show.setColorFilter(ContextCompat.getColor(this, R.color.colorGray1))
                    showcheck = false
                    password1.setSelection(password1.getText().toString().length)
                } else {
                    password1.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                    show.setColorFilter(ContextCompat.getColor(this, R.color.solidpink))
                    showcheck = true
                    password1.setSelection(password1.getText().toString().length)
                }
            }
        }
    }


    private fun spannableText() {
        val content = SpannableString("Don't have an account? Sign Up")
        content.setSpan(
                object : ClickableSpan() {
                    override fun onClick(vi: View) {
                        startActivity(Intent(this@LoginActivity, SignupActivity::class.java))
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = false
                    }
                },
                23, 30, 0
        )
        content.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.solidpink)),
                23,
                30,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        content.setSpan(StyleSpan(Typeface.BOLD), 23, 30, 0)
        signupTv.setText(content)
        signupTv.setMovementMethod(LinkMovementMethod.getInstance())
    }


    override fun onBackPressed() {
        if (backButtonCount >= 1) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            Toast.makeText(this, HomeActivity().EXIT_MSG, Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({
                backButtonCount = 0
            }, 2000)
            backButtonCount++
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        Log.e("input", "----->" + s.toString())
        if(s?.length!!>0){
            try {
                val long: Long = s.toString().toLong()
                countryCodePicker.visibility = View.VISIBLE
                imageView41.visibility = View.INVISIBLE
            }catch (e: Exception){
                countryCodePicker.visibility = View.VISIBLE
              //  imageView41.visibility = View.VISIBLE
            }

        }else{
            countryCodePicker.visibility = View.VISIBLE
            imageView41.visibility = View.INVISIBLE
        }
    }

    override fun afterTextChanged(s: Editable?) {
    }

    fun onlyDigits(str: String, n: Int):Boolean {
        // Traverse the string from
        // start to end
        var check1=true;
        for (i in 0 until n) {

            // Check if character is
            // digit from 0-9
            // then return true
            // else false
             if (str[i] >= '0'
                    && str[i] <= '9') {
                check1=true
            } else {
                check1=false
                break
            }
        }
        return check1
    }

}