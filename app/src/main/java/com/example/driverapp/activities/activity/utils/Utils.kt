import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

 class Utils {


    fun getMessageDialog(context: Context, msg: String, i: Int, b: Boolean): Dialog? {
        showCustomSnackBar(context,msg)
//        val dialog = Dialog(context, android.R.style.Theme_Black)
//        val finalMsg  =  msg
//        dialog.window!!.setFlags(
//                WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
//        )
//        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.common_msg_dialog)
//        dialog.setCancelable(false)
//        dialog.setCanceledOnTouchOutside(false)
//        val okButton = dialog.findViewById<Button>(R.id.okButton)
//        val messageTextView = dialog.findViewById<TextView>(R.id.messageTextView)
//        messageTextView.text = msg
//        okButton.setOnClickListener {
//            if (finalMsg.equals("Invalid Token", ignoreCase = true)) {
//                dialog.dismiss()
//                val intent = Intent(context, LoginActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
//                context.startActivity(intent)
//            }
//            dialog.dismiss()
//        }
//        dialog.show()
        return null
    }


     fun showCustomSnackBar(context: Context, message: String) {
         val snackbar= Snackbar.make((context as Activity).findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
         val snackBarView: View = snackbar.view
         snackBarView.setBackgroundColor(ContextCompat.getColor(context,R.color.colorred))
         snackbar.show()
         when (message){
             "Invalid Token"->{
                 val intent = Intent(context, LoginActivity::class.java)
                 intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                 context.startActivity(intent)
             }
             }
         }

     fun getMessageDialogWithOption(context: Context?, msg: String?): Dialog? {
         val dialog = Dialog(context!!, android.R.style.Theme_Black)
         dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
         dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
         dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
         dialog.setContentView(R.layout.layout_dialog_with_option)
         val noTv = dialog.findViewById<TextView>(R.id.noTv)
         val yesTv = dialog.findViewById<TextView>(R.id.yesTv)
         val messageTextView = dialog.findViewById<TextView>(R.id.day)
         messageTextView.text = msg
         noTv.setOnClickListener { dismissDialog(dialog) }
         yesTv.setOnClickListener { dismissDialog(dialog) }
         dialog.show()
         return dialog
     }

    fun getProgressDialog(context: Context?): Dialog {
        val pd = Dialog(context!!, android.R.style.Theme_Black)
        val view: View = LayoutInflater.from(context).inflate(R.layout.progress_layout, null)
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        pd.window!!.setBackgroundDrawableResource(R.color.transparent)
        pd.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pd.setContentView(view)
        pd.setCancelable(false)
        pd.show()
        return pd
    }

    fun dismissDialog(dialog: Dialog?) {
        if (dialog != null && dialog.isShowing) dialog.dismiss()
    }

    fun getDeviceWidth(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.x
    }

    fun getScreenWidth(activity: Activity): Int {
        val wm = activity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.x
    }

    fun getOutputMediaFile(context: Context): File {
        val mediaStorageDir: File
        mediaStorageDir = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "GKMIT"
            )
        } else {
            File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "GKMIT")
        }
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.d(“GKMIT”, "Oops! Failed create " + “GKMIT”+ " directory");
            }
        }
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        return File(mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg")
        //File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        //return mediaFile;
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 0) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    fun saveImageFile(context: Context, bmp: Bitmap): File {
        var bmp = bmp
        val file = context.filesDir
        val c = Calendar.getInstance()
        val df = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
        val formattedDate = df.format(c.time)
        val imageFile = File(file, "$formattedDate.png")
        val os: OutputStream
        try {
            bmp = Utils().getResizedBitmap(bmp, 300)
            os = FileOutputStream(imageFile)
            bmp.compress(Bitmap.CompressFormat.PNG, 60, os)
            os.flush()
            os.close()
        } catch (e: Exception) {
            Log.e(javaClass.simpleName, "Error writing bitmap", e)
        }
        return File(context.filesDir, "$formattedDate.png")
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver, inImage,
                SimpleDateFormat("ddmmyyhhmmss").format(Calendar.getInstance().time), null
        )
        return Uri.parse(path)
    }

    companion object {
        fun checkMailAddress(emailAddress: String?): Boolean {
            val pattern = Pattern.compile(
                    "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$",
                    Pattern.CASE_INSENSITIVE
            )
            val matcher = pattern.matcher(emailAddress)
            return matcher.find()
        }

        fun checkPhone(emailAddress: String?): Boolean {
            val pattern = Pattern.compile(
                "-?\\\\d+(\\\\.\\\\d+)?",
                Pattern.CASE_INSENSITIVE
            )
            val matcher = pattern.matcher(emailAddress)
            return matcher.find()
        }
    }
}