package com.example.driverapp.activities.activity.ui.todayOrder

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.adapter.TodayOrderAdapter
import kotlinx.android.synthetic.main.activity_todays.*

class TodaysActivity  : BaseActivity(), View.OnClickListener{
    var list:ArrayList<String>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todays)
        init()
        initControl()

    }



    override fun init() {
        todayOrder()

    }

    override fun initControl() {
        backIv.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.backIv->{
                onBackPressed()
            }



        }
    }

    fun todayOrder(){
        todaysrecvw.layoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        list?.add("1")
        list?.add("1")
        list?.add("1")
        todaysrecvw.adapter = TodayOrderAdapter(this!!)
    }
}