package com.example.driverapp.activities.activity.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;

import com.example.driverapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.icontractor.location.LocationClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class HomeMapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener
{

    private GoogleMap mMap;
    private TextView titleTextView,detectLocationTextView;
    private ImageView backImageView;
    private Button submitButton;
    private LocationClass locationClass;
    private TextView searchTextView;
    private ArrayList markerPoints;
    private final int PLACE_REQ_CODE=12,GETTING_ADDRESS=1,NOT_SERVE_THIS_AREA=2,HIDE_INFO_WINDOW=3,GETTING_SELECTED_CITY=4,CHECKING_CITY=5;
    Marker marker=null ;
    Geocoder geocoder;
    private Handler handler;
    private boolean isInsideCity=false;
    private GetAddress getAddress;
    private LatLng homeLatLong;
    private String cityName="";
    private String pinCode="";

    @SuppressLint("HandlerLeak")
    private void init()
    {
        titleTextView=findViewById(R.id.titleTextView);
        backImageView=findViewById(R.id.backImageView);
        submitButton=findViewById(R.id.submitButton);
        searchTextView=findViewById(R.id.searchTextView);
//        detectLocationTextView=findViewById(R.id.detectLocationTextView);
        locationClass=new LocationClass(this);
        markerPoints= new ArrayList();
        backImageView.setOnClickListener(this);
        submitButton.setOnClickListener(this);
//        detectLocationTextView.setOnClickListener(this);
        searchTextView.setOnClickListener(this);
        titleTextView.setText("Select Location");

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getAddress=new GetAddress();
        homeLatLong = new LatLng(locationClass.getLatitude(), locationClass.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(homeLatLong, 12.0f));
        marker = mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).title("Marker").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin)));

        handler=new Handler(Looper.myLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                if(msg.what==GETTING_ADDRESS)
                {
                    marker.setTitle("Getting address…");
                    marker.showInfoWindow();
                }
//                else if(msg.what==NOT_SERVE_THIS_AREA)
//                {
//                    marker.setTitle("Sorry we do not serve here yet…");
//                    marker.showInfoWindow();
//                }
                else if(msg.what==HIDE_INFO_WINDOW)
                {
                    marker.hideInfoWindow();
                }

            }
        };

        markerPoints.add(homeLatLong);
        geocoder = new Geocoder(HomeMapsActivity.this, Locale.getDefault());

        getAddress.execute(GETTING_SELECTED_CITY);

        GetAddress getSelectedCityAddress=new GetAddress();
        getSelectedCityAddress.execute();
        GetSelectedAddress getSelectedAddress=new GetSelectedAddress();
        getSelectedAddress.execute();

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

                if(marker==null)
                {
                    marker = mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).title("Marker")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin)));

                }
                else
                {
                    marker.setPosition(mMap.getCameraPosition().target);
                }
                handler.sendEmptyMessage(HIDE_INFO_WINDOW);

            }
        });



        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {


                if(getAddress.getStatus()!= AsyncTask.Status.RUNNING)
                {
                    getAddress=new GetAddress();
                    getAddress.execute(CHECKING_CITY);
                }else
                {
                    getAddress.cancel(true);
                    getAddress=new GetAddress();
                    getAddress.execute(CHECKING_CITY);
                }
            }
        });
    }


    private void setMarker()
    {
        try {
            LatLng latLng=marker.getPosition();
            List<Address> addressesList=geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);


            if(addressesList!=null&&!addressesList.isEmpty())
            {
                Address addresses = addressesList.get(0);

               /* if(addresses.getLocality()!=null&&addresses.getLocality().equalsIgnoreCase(selectedCity)||
                        addresses.getLocality().equalsIgnoreCase("Hail")||
                        addresses.getLocality().equalsIgnoreCase("حائل"))
                {
*/

                    searchTextView.setText(addresses.getAddressLine(0));
                    pinCode= addresses.getPostalCode();
                    isInsideCity=true;

/*
                }else
                {
                    isInsideCity=false;
                    searchTextView.setText(addresses.getLocality());
                    marker.setTitle("Sorry we do not serve here yet…");
                    marker.showInfoWindow();
                }*/



            }else
            {
                searchTextView.setText("");

            }



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
    }


    private void initCameraIdle() {
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {


                try {

                    LatLng latLng=marker.getPosition();
                    List<Address> addressesList=geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);

                    if(addressesList!=null&&!addressesList.isEmpty())
                    {
                        Address addresses = addressesList.get(0);

/*

                        if(addresses.getLocality()!=null && (addresses.getLocality().equalsIgnoreCase(selectedCity)||
                                addresses.getLocality().equalsIgnoreCase("Hail")||
                                addresses.getLocality().equalsIgnoreCase("حائل")))
                        {

*/


                            searchTextView.setText(addresses.getLocality());
                            pinCode= addresses.getPostalCode();


                        /*}else
                        {
                            marker.setTitle("We Don't serve outside the city!");
                            marker.showInfoWindow();
                        }*/
                    }else
                    {
                        searchTextView.setText("");
                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

                /*center = marker.get.getCameraPosition().target;
                getAddressFromLocation(center.latitude, center.longitude);*/
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.backImageView:
                finish();
                break;
            case R.id.submitButton:
                onSubmit();

                break;
//            case R.id.detectLocationTextView:
//
//                LatLng latLng = new LatLng(locationClass.getLatitude(), locationClass.getLongitude());
//
//                marker.setPosition(latLng);
//
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));
//
//                markerPoints.add(latLng);
//                break;

            case R.id.searchTextView:
                Places.initialize(HomeMapsActivity.this,getResources().getString(R.string.google_map_key));
                List<Place.Field> fields1 = Arrays.asList(Place.Field.LAT_LNG);
                List<Integer> filterTypes = new ArrayList<Integer>();
                Intent intent1 = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields1).build(HomeMapsActivity.this);
                intent1 .setType(String.valueOf(TypeFilter.ADDRESS));

                startActivityForResult(intent1, PLACE_REQ_CODE);
                break;
        }
    }


    private void onSubmit()
    {

        LatLng selectedLocation =  marker.getPosition();
        Intent intent=new Intent();
        intent.putExtra("address",searchTextView.getText().toString());
        intent.putExtra("lat",selectedLocation.latitude+"");
        intent.putExtra("long",selectedLocation.longitude+"");
        intent.putExtra("city",cityName);
        intent.putExtra("pincode",pinCode);
        this.setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                markerPoints.clear();
                LatLng selectedPos = place.getLatLng();
                markerPoints.add(selectedPos);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(selectedPos, 12.0f));
            }
        }
    }

    private class GetAddress extends AsyncTask<Integer, Void, String>
    {

        LatLng latLng;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            isInsideCity=false;
            latLng=marker.getPosition();
            handler.sendEmptyMessage(GETTING_ADDRESS);
        }

        @Override
        protected String doInBackground(Integer... AddressType) {
            String address="";

            try
            {

                List<Address> addressesList=geocoder.getFromLocation(latLng.latitude,latLng.longitude,10);

                if(addressesList!=null&&!addressesList.isEmpty())
                {


                    for(int i=0;i<addressesList.size();i++)
                    {
                        Address addresses = addressesList.get(i);
                        cityName=(addressesList.get(i).getLocality()==null?"":addressesList.get(i).getLocality());
                        pinCode= addresses.getPostalCode();
                        Log.w("testMap","size"+addressesList.size());

                        if(cityName!=null&&!cityName.isEmpty())
                        {
                            address=addresses.getAddressLine(0);
                            isInsideCity=true;
                            handler.sendEmptyMessage(HIDE_INFO_WINDOW);
                            Log.w("testMap","city found index"+i);

                            break;
                        }else
                        {
                            if(i==addressesList.size()-1)
                            {
                                addresses = addressesList.get(0);
                                pinCode= addresses.getPostalCode();
                                cityName=(addressesList.get(0).getLocality()==null?"":addressesList.get(0).getLocality());

                                address=addresses.getAddressLine(0);
                                pinCode= addresses.getPostalCode();
                                isInsideCity=true;
                                handler.sendEmptyMessage(HIDE_INFO_WINDOW);
                                Log.w("testMap","city found index"+0);

                            }
                        }


                    }
                    Log.w("testMap","City Name: "+cityName);

                }else
                {
                    handler.sendEmptyMessage(NOT_SERVE_THIS_AREA);

                    address="";
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return address;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            searchTextView.setText(s);
        }
    }

    private class GetSelectedAddress extends AsyncTask<Integer, Void, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Integer... AddressType) {
            String address="";
            try
            {
                List<Address> addressesList=geocoder.getFromLocation(homeLatLong.latitude,homeLatLong.longitude,10);
                for(int i=0;i<addressesList.size();i++)
                {
                    Address addresses = addressesList.get(i);
                    cityName=(addressesList.get(i).getLocality()==null?"":addressesList.get(i).getLocality());
                    pinCode= addresses.getPostalCode();

                    if(cityName!=null&&!cityName.isEmpty())
                    {
                        address=addresses.getAddressLine(0);
                        pinCode= addresses.getPostalCode();
                        break;
                    }else
                    {
                        if(i==addressesList.size()-1)
                        {
                            addresses = addressesList.get(0);
                            cityName=(addressesList.get(0).getLocality()==null?"":addressesList.get(0).getLocality());
                            address=addresses.getAddressLine(0);
                            pinCode= addresses.getPostalCode();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

            }

           /* if(AddressType[0]==GETTING_SELECTED_CITY)
            {
                selectedCity=address;
            }*/

            return address;
        }
        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            searchTextView.setText(s);
        }
    }
}
