package com.example.driverapp.activities.activity.ui.about

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import kotlinx.android.synthetic.main.activity_about_us.*

class AboutUsActivity : BaseActivity(),View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        init()
        initControl()
    }
    override fun init() {
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv->{
                onBackPressed()
            }

        }

    }

}