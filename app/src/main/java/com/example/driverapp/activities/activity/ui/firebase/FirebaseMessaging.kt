package com.example.driverapp.activities.activity.ui.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.driverapp.R
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.navCustomer.NavigateCostumerActivity
import com.example.driverapp.activities.activity.utils.NotificationUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject


class FirebaseMessaging : FirebaseMessagingService() {
    private var broadcaster: LocalBroadcastManager? = null


    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("testNotification", remoteMessage.data.toString())
        val msgMapss = remoteMessage.data

//        if(msgMapss["type"]!!.equals("welcome"))
//        {
//            sendNotifications(msgMapss["title"]!!, msgMapss["body"]!!)
//            Log.d("helolo","hello")
//
//        }

        if(msgMapss["type"]!!.toString().equals("driverOrderRequest"))
        {
            Log.d("yesss","yeahhh")

            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            }
            else {
                val msgMaps = remoteMessage.data

                var value: String? = msgMaps["data"]!!
                val jsonObject = JSONObject(value)

                val distance = jsonObject["distance"].toString()
                var restaurantName = jsonObject["restaurantName"].toString()
                val orderId = jsonObject["orderId"].toString()
                Log.d("orderidyeh", orderId)
                startService(Intent(this, MyService::class.java)
                        .putExtra("orderId", orderId)
                        .putExtra("restaurantName", restaurantName)
                        .putExtra("distance", distance))

            }
            val msgMap = remoteMessage.data


            sendNotification(msgMap["title"]!!, msgMap["body"]!!, msgMap["data"]!!)
        }

       else  if(msgMapss["type"]!!.toString().equals("readyForPickup"))


        {
            val msgMaps = remoteMessage.data
            Log.d("heyyyss","hellos")

            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            }
            else
            {
                val msgMapss = remoteMessage.data
                msgMapss["data"]!!.toString()







//                startService(Intent(this, MyHomeSwitchService::class.java)
//                        .putExtra("orderId",  msgMapss["data"]!!.toString()))


            }
            sendNotificationss(msgMaps["title"]!!, msgMaps["body"]!!,msgMaps["data"]!!)

//            sendNotifications(msgMaps["title"]!!, msgMaps["body"]!!)


        }
        else if(msgMapss["type"]!!.toString().equals("welcome"))


        {
            val msgMaps = remoteMessage.data
            Log.d("heyyyss","hellos")

            sendNotifications(msgMaps["title"]!!, msgMaps["body"]!!)


        }

        else
        {
            Log.d("yesss","yeahhh")

            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            }
            else {
                val msgMaps = remoteMessage.data

                var value: String? = msgMaps["data"]!!
                val jsonObject = JSONObject(value)

                val distance = jsonObject["distance"].toString()
                var restaurantName = jsonObject["restaurantName"].toString()
                val orderId = jsonObject["orderId"].toString()
                Log.d("orderidyeh", orderId)
                startService(Intent(this, MyService::class.java)
                        .putExtra("orderId", orderId)
                        .putExtra("restaurantName", restaurantName)
                        .putExtra("distance", distance))

            }
            val msgMap = remoteMessage.data


            sendNotification(msgMap["title"]!!, msgMap["body"]!!, msgMap["data"]!!)
        }

        Log.d("heyyy","hellos")









    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)


    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }


    private fun sendNotifications(title: String, messageBody: String?)
    {
        val i = Intent("com.biteMe")
        sendBroadcast(i)
        val intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val channelId = "bitMe_noti"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle(title + "")
                .setContentText(messageBody + "")
                .setAutoCancel(true)
                .setSound(/*Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.good_job)*/defaultSoundUri)
                .setVibrate(LongArray(0))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)
        //  .setFullScreenIntent(pendingIntent, false)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //val sound = Uri.parse(/*ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.good_job*/sou) //Here is FILE_NAME is the name of file that you want to play
            val sound=defaultSoundUri
            val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            val channel = NotificationChannel(channelId, "Notification", NotificationManager.IMPORTANCE_HIGH)
            channel.enableLights(true)
            channel.enableVibration(true)
            channel.vibrationPattern = LongArray(0)
            channel.setShowBadge(true)
            channel.enableLights(true)
            channel.enableVibration(true)
            channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
            channel.setSound(sound, attributes);
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(430, notificationBuilder.build())
    }

    private fun sendNotificationss(title: String, messageBody: String?,value: String?)

    {
        val intentBroadcast= Intent("COM.DATA")
        intentBroadcast.putExtra("id",value)
        sendBroadcast(intentBroadcast);

        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            val i = Intent("com.biteMe")
            sendBroadcast(i)
            val intent = Intent(this, NavigateCostumerActivity::class.java)
            intent.putExtra("orderid",value)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val channelId = "bitMe_noti"
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.appicon)
                    .setContentTitle(title + "")
                    .setContentText(messageBody + "")
                    .setAutoCancel(true)
                    .setSound(/*Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.good_job)*/defaultSoundUri)
                    .setVibrate(LongArray(0))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent)
            //  .setFullScreenIntent(pendingIntent, false)
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //val sound = Uri.parse(/*ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.good_job*/sou) //Here is FILE_NAME is the name of file that you want to play
                val sound=defaultSoundUri
                val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
                val channel = NotificationChannel(channelId, "Notification", NotificationManager.IMPORTANCE_HIGH)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.vibrationPattern = LongArray(0)
                channel.setShowBadge(true)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                channel.setSound(sound, attributes);
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(430, notificationBuilder.build())
        }

        else
        {

            val i = Intent("com.biteMe")
            sendBroadcast(i)
            val intent = Intent(this, NavigateCostumerActivity::class.java)
            intent.putExtra("orderid",value)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val channelId = "bitMe_noti"
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.appicon)
                    .setContentTitle(title + "")
                    .setContentText(messageBody + "")
                    .setAutoCancel(true)
                    .setSound(/*Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.good_job)*/defaultSoundUri)
                    .setVibrate(LongArray(0))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent)
            //  .setFullScreenIntent(pendingIntent, false)
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //val sound = Uri.parse(/*ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.good_job*/sou) //Here is FILE_NAME is the name of file that you want to play
                val sound=defaultSoundUri
                val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
                val channel = NotificationChannel(channelId, "Notification", NotificationManager.IMPORTANCE_HIGH)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.vibrationPattern = LongArray(0)
                channel.setShowBadge(true)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                channel.setSound(sound, attributes);
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(430, notificationBuilder.build())

        }



    }

    private fun sendNotification(title: String, messageBody: String?,value: String?)
    {
        val jsonObject = JSONObject(value)
        val distance = jsonObject["distance"].toString()
        val restaurantName = jsonObject["restaurantName"].toString()
        val orderId = jsonObject["orderId"].toString()
        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {


            val i = Intent("com.example.driverapp")
            sendBroadcast(i)
            val intent = Intent(this, HomeActivity::class.java)
                    .putExtra("orderId",orderId)
                    .putExtra("restaurantName",restaurantName)
                    .putExtra("distance",distance)


            val pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val channelId = "bitMe_noti"
            val sound= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bell);
            val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle(title + "")
                .setContentText(messageBody + "")
                .setAutoCancel(true)
                .setSound(
                    sound
                )
                .setVibrate(LongArray(0))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)
            //  .setFullScreenIntent(pendingIntent, false)
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val sound= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bell);
                val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
                val channel = NotificationChannel(
                    channelId,
                    "Notification",
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.vibrationPattern = LongArray(0)
                channel.setShowBadge(true)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                channel.setSound(sound, attributes);
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(430, notificationBuilder.build())

        }

        else
        {

            Log.d("hello", "hello")
            val i = Intent("com.example.driverapp")
            sendBroadcast(i)

            stopService(Intent(this, MyService::class.java))



            val intent = Intent(this, BReceiver::class.java)
                    .putExtra("orderId",orderId)
                    .putExtra("restaurantName",restaurantName)
                    .putExtra("distance",distance)


            broadcaster?.sendBroadcast(intent)
            val sound= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bell);
            val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()

            val pendingIntent = PendingIntent.getBroadcast(
                this,
                0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val channelId = "bitMe_noti"
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle(title + "")
                .setContentText(messageBody + "")
                .setAutoCancel(true)
                .setSound(/*Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.good_job)*/
                        sound
                )
                .setVibrate(LongArray(0))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)

            //  .setFullScreenIntent(pendingIntent, false)
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()

               val sound= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bell);
                //val sound = Uri.parse(/*ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.good_job*/sou) //Here is FILE_NAME is the name of file that you want to play

                val channel = NotificationChannel(
                    channelId,
                    "Notification",
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.vibrationPattern = LongArray(0)
                channel.setShowBadge(true)
                channel.enableLights(true)
                channel.enableVibration(true)
                channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                channel.setSound(sound, attributes);
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(430, notificationBuilder.build())
        }
        }




}