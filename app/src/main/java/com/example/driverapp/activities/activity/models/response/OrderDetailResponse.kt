package com.example.driverapp.activities.activity.models.response

data class OrderDetailResponse(
    val `data`: Data,
    val message: String,
    val status: Int
)

{
    data class Data(
            val brand: Brand,
            val orderData: OrderData,
            val restaurant: Restaurant,
            val userData: UserData
    )
    {
        data class OrderData(
                val __v: Int,
                val _id: String,
                val actualOrderDate: String,
                val actualOrderStatus: Boolean,
                val addNote: String,
                val address: String,
                val addressType: String,
                val areaId: String,
                val brandId: String,
                val buildingAndApart: String,
                val cancelStatus: Boolean,
                val commissionPer: Int,
                val createdAt: String,
                val currency: String,
                val day: Int,
                val deliveryCharge: Int,
                val deliveryDate: String,
                val deliveryTime: String,
                val discountAmount: Int,
                val driverAssign: Boolean,
                val driverId: String,
                val driverLat: String,
                val driverLong: String,
                val driverUnavailable: List<Any>,
                val etaTime: Int,
                val excepetdDeliveryTime: Int,
                val landmark: String,
                val latitude: String,
                val longitude: String,
                val loyalityRewardStatus: Boolean,
                val month: Int,
                val noCutlery: Boolean,
                val offerAmount: Int,
                val orderAcceptByDriverTime: String,
                val orderAcceptByRestroDuration: Int,
                val orderAcceptByRestroTime: String,
                val orderArriveTime: String,
                val orderData: List<OrderDataX>,
                val orderDate: String,
                val orderDeliveredTime: String,
                val orderNumber: String,
                val orderOutForDeliveryTime: String,
                val orderReceivedTime: String,
                val orderType: String,
                val paymentAmount: Int,
                val paymentDate: String,
                val paymentId: String,
                val paymentMode: String,
                val promocode: String,
                val ratingStatus: Boolean,
                val rejectOrderByDrivers: List<RejectOrderByDriver>,
                val restaurantId: String,
                val restroDeliveryTime: Int,
                val restroLat: String,
                val restroLong: String,
                val serviceType: String,
                val status: String,
                val subTotal: Int,
                val timezone: String,
                val total: Int,
                val updatedAt: String,
                val userCanCancelOrder: Boolean,
                val userId: String,
                val vatAmount: Int,
                val vatPer: Int,
                val year: Int
        )
        {
            data class OrderDataX(
                    val _id: String,
                    val actualAmount: Int,
                    val amountWithQuantuty: Int,
                    val choiceAmount: Int,
                    val extra: List<Extra>,
                    val price: Int,
                    val productData: ProductData,
                    val productId: String,
                    val quantity: Int
            )

        }

    }
}


data class Brand(
    val _id: String,
    val address: Any,
    val countryCode: String,
    val email: String,
    val latitude: String,
    val logo: String,
    val longitude: String,
    val mobileNumber: String,
    val storeName: String
)


data class Restaurant(
    val _id: String,
    val address: String,
    val avgRating: Int,
    val branchNameEn: String,
    val countryCode: String,
    val distance: Double,
    val email: String,
    val image: String,
    val latitude: String,
    val longitude: String,
    val mobileNumber: String
)

data class UserData(
    val _id: String,
    val countryCode: String,
    val firstName: String,
    val lastName: String,
    val mobileNumber: String,
    val profilePic: String
)


data class RejectOrderByDriver(
    val _id: String,
    val driverId: String
)

data class Extra(
    val categoryId: String,
    val choiceId: String,
    val price: Int,
    val quantity: Int
)

data class ProductData(
    val __v: Int,
    val _id: String,
    val adminVerifyStatus: String,
    val availabilityStatus: Boolean,
    val avgRating: Int,
    val branchId: String,
    val brandId: String,
    val category: List<Category>,
    val changeRequest: Boolean,
    val changeRequestApporve: String,
    val changeRequestId: String,
    val changeRequestMsg: String,
    val createdAt: String,
    val deleteStatus: Boolean,
    val description: String,
    val descriptionAr: String,
    val dietary: List<Dietary>,
    val index: Int,
    val menuAssignStatus: Boolean,
    val menuCategoryName: String,
    val menuId: String,
    val price: Int,
    val priceSelection: Boolean,
    val productImage: String,
    val productName: String,
    val productNameAr: String,
    val ratingByUsers: Int,
    val showDietary: List<String>,
    val status: String,
    val totalOrder: Int,
    val totalRating: Int,
    val type: String,
    val updatedAt: String
)

data class Category(
    val _id: String,
    val categoryNameAr: String,
    val categoryNameEn: String,
    val choice: List<Choice>,
    val max: Int,
    val min: Int
)

data class Dietary(
    val _id: String,
    val name: String
)

data class Choice(
    val _id: String,
    val name: String,
    val price: Int,
    val status: Boolean
)