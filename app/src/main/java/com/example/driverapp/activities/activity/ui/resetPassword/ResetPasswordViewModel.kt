package com.example.driverapp.activities.activity.ui.resetPassword

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.driverChangePasswordDataModel
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ResetPasswordViewModel : BaseViewModel() {

    var changepasswordData = MutableLiveData<driverChangePasswordDataModel>()

    fun driverChangePasswordApi(context: Context,token: String, oldPassword: String,newPassword: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverChangePassword(token,oldPassword,newPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    changepasswordData.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
}