package com.example.driverapp.activities.activity.webservices


import com.example.driverapp.activities.activity.constant.ApiConstant
import com.example.driverapp.activities.activity.models.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface ApiInterface {

    @FormUrlEncoded
    @POST("driverLogin")
    fun driverLogin(@Field("password") password: String,
            @Field("countryCode") countryCode: String,
            @Field("mobileNumber") mobileNumber: String,
            @Field("deviceToken") deviceToken: String,
            @Field("deviceType") deviceType: String): Observable<DriverLoginDataModel>


    @FormUrlEncoded
    @POST("checkDriverMobileAndEmail")
    fun checkDriverMobileAndEmail(@Field("email") email: String,
                    @Field("mobileNumber") mobileNumber: String): Observable<CheckDriverMobileAndEmailDataModel>


    @FormUrlEncoded
    @POST("getOrderDetail")
    fun getOrderDetail(@Field("driverId") driverId: String,
                                  @Field("orderId") orderId: String): Observable<OrderDetailResponse>

    @FormUrlEncoded
    @POST("getOrderList")
    fun getOrderList(@Field("driverId") driverId: String,
                                  @Field("orderType") orderType: String): Observable<OrderListResponse>


    @FormUrlEncoded
    @POST("updateOrderStatus")
    fun getOrderStatus(@Field("driverId") driverId: String,
                     @Field("orderId") orderId: String,
                       @Field("status") status: String): Observable<UpdateOderStatusResonse>


    @FormUrlEncoded
    @POST("driverSignup")
    fun driverSignup(@Field("email") email: String,
                    @Field("countryCode") countryCode: String,
                    @Field("mobileNumber") mobileNumber: String,
                     @Field("emirateNumber") emirateNumber:String,
                    @Field("firstName") firstName: String,
                    @Field("lastName") lastName: String,
                    @Field("password") password: String,
                    @Field("drivingLicence") drivingLicence: String,
                    @Field("nationalIdentity") nationalIdentity: String,
                    @Field("policeVerification") policeVerification: String,
                    @Field("document") document: String,
                    @Field("vehicleType") vehicleType: String,
                    @Field("vehicleBrand") vehicleBrand: String,
                    @Field("vehicleModel") vehicleModel: String,
                    @Field("vehicleNumber") vehicleNumber: String,
                    @Field("manufacturingDate") manufacturingDate: String,
                    @Field("vehicleLicence") vehicleLicence: String,
                    @Field("vehicleDocuments") vehicleDocuments: String,
                    @Field("deviceToken") deviceToken: String,
                    @Field("deviceType") deviceType: String,
                    @Field("dutyStartTime") dutyStartTime: String,
                    @Field("dutyEndTime") dutyEndTime: String,
                     @Field("passportNumber") passportNumber: String,
                     @Field("dob") dob : String,
                     @Field("username") username:String
    ): Observable<DriverSignupDataModel>


    @GET("getDriverNotificationList")
    fun getDriverNotificationList(@Header("Authorization") header: String):Observable<GetDriverNotificationListDataModel>

    @GET("getDriverNotificationCount")
    fun getDriverNotificationCount(@Header("Authorization") header: String):Observable<GetDriverNotificationCountDataModel>


    @GET("getDriverDetails")
    fun getDriverDetails(@Header("Authorization") header: String):Observable<GetDriverDetailsDataModel>


    @FormUrlEncoded
    @POST("acceptorder")
    fun orderAccept(@Field("driverId") driverId: String,
                                          @Field("latitude") latitude: String,
                    @Field("longitude") longitude: String,
                    @Field("orderId") orderId: String
                    ):Observable<OrderResponse>


    @FormUrlEncoded
    @POST("updateOrderStatus")
    fun updateOrderSatus(@Field("driverId") driverId: String,
                    @Field("orderId") latitude: String,
                    @Field("status") longitude: String
    ):Observable<UpdateOderStatusResonse>



    @FormUrlEncoded
    @POST("rejectorder")
    fun orderReject(@Field("driverId") driverId: String,
                    @Field("orderId") orderId: String
    ):Observable<OrderResponse>



    @FormUrlEncoded
    @POST("checkDriverMobilForForgotPassword")
    fun checkDriverMobilForForgotPassword(@Field("countryCode") countryCode: String,
                                  @Field("mobileNumber") mobileNumber: String):Observable<CheckDriverMobilForForgotPasswordDataModel>

    @FormUrlEncoded
    @POST("driverForgotPassword")
    fun driverForgotPassword(@Field("driverId") driverId: String,
                             @Field("password") password: String):Observable<DriverForgotPasswordDataModel>


    @FormUrlEncoded
    @POST("driverChangeEmail")
    fun driverChangeEmail(@Header("Authorization") header: String?,
                          @Field("email") email: String): Observable<DriverChangeEmailDataModel>

    @FormUrlEncoded
    @POST("checkDriverMobile")
    fun checkDriverMobile(@Header("countryCode") countryCode: String?,
                          @Field("mobileNumber") mobileNumber: String): Observable<CheckDriverMobileDataModel>

    @FormUrlEncoded
    @POST("driverChangeMobileNumber")
    fun driverChangeMobileNumber(@Header("Authorization") header: String?,
                          @Field("countryCode") countryCode: String,
                          @Field("mobileNumber") mobileNumber: String): Observable<DriverChangeMobileNumberDataModel>


    @FormUrlEncoded
    @POST("driverChangePassword")
    fun driverChangePassword(@Header("Authorization") header: String?,
                                 @Field("oldPassword") oldPassword: String,
                                 @Field("newPassword") newPassword: String): Observable<driverChangePasswordDataModel>


    @FormUrlEncoded
    @POST("driverDeleteNotification")
    fun driverDeleteNotification(@Header("Authorization") header: String?,
                          @Field("notificationId") notificationId: String): Observable<DriverDeleteNotificationDataModel>

    @GET("driverClearNotification")
    fun driverClearNotification(@Header("Authorization") header: String):Observable<DriverClearNotificationDataModel>


    @FormUrlEncoded
    @POST("driverNotificationSetting")
    fun driverNotificationSetting(@Header("Authorization") header: String?,
                                 @Field("notificationStatus") notificationStatus: String): Observable<DriverNotificationSettingDataModel>

    @FormUrlEncoded
    @POST("updateDutyStatus")
    fun updateDutyStatus(@Header("Authorization") header: String?,
                                  @Field("dutyStatus") dutyStatus: String): Observable<UpdateDutyStatusDataModel>

    @FormUrlEncoded
    @POST("updateLocation")
    fun updateLocation(@Header("Authorization") header: String?,
                         @Field("latitude") latitude: String,
                         @Field("longitude") longitude: String): Observable<UpdateLocationDataModel>


    @FormUrlEncoded
    @POST("updateVehicleDtails")
    fun updateVehicleDtails(@Header("Authorization") header: String?,
                             @Field("vehicleType") vehicleType: String,
                             @Field("vehicleBrand") vehicleBrand: String,
                            @Field("vehicleModel") vehicleModel: String,
                            @Field("vehicleNumber") vehicleNumber: String,
                            @Field("manufacturingDate") manufacturingDate: String,
                            @Field("vehicleLicence") vehicleLicence: String,
                            @Field("vehicleDocuments") vehicleDocuments: String): Observable<updateVehicleDtailsDataModel>


    @FormUrlEncoded
    @POST("driverUpdateDetails")
    fun driverUpdateDetails(@Header("Authorization") header: String?,
                            @Field("firstName") firstName: String,
                            @Field("lastName") lastName: String,
                            @Field("profilePic") profilePic: String,
                            @Field("address") address: String,
                            @Field("latitude") latitude: String,
                            @Field("longitude") longitude: String,
                            @Field("countryCode") countryCode: String,
                            @Field("mobileNumber") mobileNumber: String,
                            @Field("emirateNumber") emirateNumber:String,
                            @Field("passportNumber") passportNumber: String,
                            @Field("dob") dob : String,

                            @Field("email") email: String): Observable<DriverUpdateDetailsDataModel>


    @GET("driverLogout")
    fun driverLogout(@Header("Authorization") header: String?):Observable<DriverLogoutDataModel>

}