package com.example.driverapp.activities.activity.ui.vehicle

import Utils
import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.ui.signup.SignupViewModel
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.FilePath
import com.example.driverapp.activities.activity.utils.S3Bucket
import com.rento.imagepicker.FilePickUtils
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_vehicle_details.*
import kotlinx.android.synthetic.main.activity_vehicle_details.backIv
import kotlinx.android.synthetic.main.activity_vehicle_details.consName
import kotlinx.android.synthetic.main.activity_vehicle_details.nextTv
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class VehicleDetailsActivity : BaseActivity() , View.OnClickListener,DatePickerDialog.OnDateSetListener{
    lateinit var signupViewModel: SignupViewModel

    private lateinit var genderList: ArrayList<String>

    var dialog:Dialog?=null

    private val date: Date = Calendar.getInstance().time

    private val PICK_PDF_REQUEST = 1

    //storage permission code
    private val STORAGE_PERMISSION_CODE = 123


    //Uri to store the image uri
    private var filePath: String? = null
   // private var filePath: Uri? = null

    private val onFileChoose = FilePickUtils.OnFileChoose { fileUri, requestCode ->
        // here you will get captured or selected image<br>

        Log.e("image", "File= " + fileUri)

        imageFilePath = fileUri


        /*institute_profile_image()*/

    }
    var filePickUtils = FilePickUtils(this, onFileChoose)
    //   var location = Location("")
    var lifeCycleCallBackManager = filePickUtils.callBackManager
    companion object {
        var tv: TextView? = null
    }
    private var imageFilePath: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_details)
        init()
        initControl()

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = simpleDateFormat.parse(year.toString() + "-" + (month + 1) + "-" + dayOfMonth)
            val newDate : String= SimpleDateFormat("dd-MM-yyyy").format(date)
            manudate.setText(newDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

    }
    override fun init() {
      signupViewModel = ViewModelProvider(this).get(SignupViewModel::class.java)

        signupViewModel.signUpData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                showToast("User Registered Successfully!")
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(Constants.TOKEN, it.data!!.jwtToken).apply()
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(Constants.NOTIFICATION_STATUS, it.data!!.notificationStatus.toString()).apply()
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(Constants.DUTY_STATUS, it.data!!.dutyStatus.toString()).apply()
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(Constants.IMAGE, it.data!!.profilePic).apply()
                val i = Intent(this, LoginActivity::class.java)
                i.putExtra("vehicledetails", true)
                startActivity(i)
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        nextTv.setOnClickListener(this)
        consManufacturing.setOnClickListener(this)
        consName.setOnClickListener(this)
        consDocuments.setOnClickListener(this)
        backIv.setOnClickListener(this)

        reationSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                if (i != 0) {
                    vehicleType.setText(genderList.get(i))
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv -> {
                onBackPressed()
            }
            R.id.nextTv -> {
                if (validate()) {
                    dialogVerification()
                }
            }
            R.id.consManufacturing -> {
                dialogCalendar()
            }
//            R.id.consName->{
//                vehicleDetails()
//            }
            R.id.consDocuments -> {
                requestStoragePermission()
            }

            R.id.consName -> {
                GenderSpinner()
            }
        }
    }


    private fun validate(): Boolean {
        if (vehicleType.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please select Vehicle Type!", 0, false)
        }else if (vehiclelicence.getText().toString().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Licence Number!", 0, false)
        } else if (manudate.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Manufacturing Date!", 0, false)
        } else if (vehiclebrand.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Brand!", 0, false)
        } else if (vehiclenum.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Number!", 0, false)
        } else if (vehiclemodel.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter vehicle Model!", 0, false)
        }else if (filePath==null) {
            Utils().getMessageDialog(this, "Please select vehicle Documents!", 0, false)
        } else {
            return true
        }
        return false
    }

    
    fun dialogVerification() {
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.dialog_verification)
        dialog!!.window!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        )
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        dialog!!.nextTv.setOnClickListener {
            dialog!!.dismiss()
            val pathList = ArrayList<String>()
            pathList.add(File(filePath).absolutePath)
            S3Bucket(this, pathList, true) { uploadedUrl ->
                signupViewModel.signUpApi(this, intent.getStringExtra("email")!!, intent.getStringExtra("countrycodepicker")!!, intent.getStringExtra("phone")!!, intent.getStringExtra("name")!!, "", intent.getStringExtra("password")!!, intent.getStringExtra("drivinglisence")!!,
                        intent.getStringExtra("nationalidentity")!!, intent.getStringExtra("policeverification")!!, intent.getStringExtra("choosefile")!!, vehicleType.text.toString().trim(), vehiclebrand.text.toString().trim(),
                        vehiclemodel.text.toString().trim(), vehiclenum.text.toString().trim(), manudate.text.toString().trim(), vehiclelicence.text.toString().trim(), TextUtils.join(",", uploadedUrl!!), getSharedPreferences(Constants.DRIVER, MODE_PRIVATE).getString(Constants.DEVICE_TOKEN, "")!!, "Android", "05:00", "23:00"
                , intent.getStringExtra("passport")!!,intent.getStringExtra("datetv1")!!)
            }
        }
    }

    private fun GenderSpinner() {
        genderList = ArrayList<String>()
        genderList.add("Please select")
        genderList.add("Motorcycle")
        genderList.add("Light Vehicle")
        val reasonAdapter: ArrayAdapter<*> = object : ArrayAdapter<Any?>(this, R.layout.support_simple_spinner_dropdown_item, genderList as List<Any?>) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val textView = super.getView(position, convertView, parent) as TextView
                textView.setTextColor(Color.TRANSPARENT)
                return textView
            }
        }
        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        reationSpinner.setAdapter(reasonAdapter)
        reationSpinner.performClick()
    }

   /* fun dialogCalendar(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK,DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
            dateTv.setText("" + dayOfMonth + "/ " + monthOfYear + "/ " + year)
           // dateTv.setText("" + dayOfMonth + " " + MONTHS[monthOfYear] + ", " + year)

        }, year, month, day)

        dpd.show()
    }*/


    fun dialogCalendar(){
        val dialog = DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, setFormat("yyyy",date), setFormat("MM",date) - 1, setFormat("dd",date))
        dialog.datePicker.maxDate
        dialog.show()
    }
    private fun setFormat(key:String, date:Date): Int{
        return SimpleDateFormat(key).format(date).toInt()
    }







//    fun vehicleDetails(){
//        var popup =  PopupMenu(this,editText2 )
//        //Inflating the Popup using xml file
//        popup.getMenu().add("Motorcycle");
//        popup.getMenu().add("Light Vehicle");
//
//
//        //registering popup with OnMenuItemClickListener
//        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
//
//
//            override fun onMenuItemClick(item: MenuItem?): Boolean {
//                editText2.setText(item?.getTitle())
//                return true;
//            }
//        });
//
//        popup.show();//showing popup menu
//    }




    //Requesting permission
    private fun requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            showFileChooser()
        }
        else  {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    STORAGE_PERMISSION_CODE
            )
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }


        //And finally ask for the permission

    }


    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showFileChooser()
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, getString(R.string.oops_denied_permission), Toast.LENGTH_LONG)
                        .show()
            }
        }else{
            if (lifeCycleCallBackManager != null) {
                lifeCycleCallBackManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }



    //method to show file chooser
    private fun showFileChooser() {

        val intent = Intent(Intent.ACTION_GET_CONTENT)
        //intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.setType("*/*");

//        intent.type = "*/*"
//        val mimetypes = arrayOf(
//            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
//            "application/msword","application/pdf","application/psd"
//        )
        // intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)
        startActivityForResult(intent, PICK_PDF_REQUEST)
    }



    //handling the image chooser activity result
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            filePath =FilePath.getPath(this, Uri.parse(data.getDataString()))
           // filePath = data.data
            Log.e("Uri", "" + data.data);
          //  Log.e("FilePath",""+ FilePath.getPath(this,filePath!!))
            chooseFile.setText(File(filePath).name)
            //startActivity(Intent(this, UploadDesignActivity::class.java).putExtra("file", FilePath.getPath(this,filePath!!)))
        }else {
            if (lifeCycleCallBackManager != null) {
                lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

}