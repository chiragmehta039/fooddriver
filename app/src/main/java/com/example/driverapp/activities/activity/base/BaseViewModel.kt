package com.quiz.quizlok.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.driverapp.activities.activity.webservices.ApiClient
import com.example.driverapp.activities.activity.webservices.ApiInterface


abstract class BaseViewModel: ViewModel() {

    var mError = MutableLiveData<Throwable>()

    val apiInterface : ApiInterface by lazy {
        ApiClient.getApiClient()
    }

}



