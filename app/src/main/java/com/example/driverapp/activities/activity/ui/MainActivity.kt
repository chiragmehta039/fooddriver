package com.example.driverapp.activities.activity.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_vehicle_two.*
import kotlinx.android.synthetic.main.activity_vehicle_two.backIv

class MainActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        initControl()
    }

    override fun init() {
        Glide.with(this).load(intent.getStringExtra("image")).into(picIv)
    }

    override fun initControl() {
        cross.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.cross -> {
                finish()
            }
        }
    }
}