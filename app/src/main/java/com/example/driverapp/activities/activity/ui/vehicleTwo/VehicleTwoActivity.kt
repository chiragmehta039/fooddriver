package com.example.driverapp.activities.activity.ui.vehicleTwo

import Utils
import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.util.Base64.decode
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.MainActivity
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.S3Bucket
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_vehicle_two.*
import java.io.File
import java.lang.Byte.decode
import java.security.spec.PSSParameterSpec.DEFAULT
import java.util.*
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.util.Base64;
import com.example.driverapp.activities.activity.utils.GetPath
import com.example.driverapp.activities.activity.utils.showToast
import kotlinx.android.synthetic.main.activity_vehicle_details.*
import kotlinx.android.synthetic.main.activity_vehicle_two.backIv
import kotlinx.android.synthetic.main.activity_vehicle_two.editTv
import kotlinx.android.synthetic.main.activity_vehicle_two.nextTv
import java.text.ParseException
import java.text.SimpleDateFormat

class VehicleTwoActivity  : BaseActivity(), View.OnClickListener,DatePickerDialog.OnDateSetListener{
    private lateinit var genderList: ArrayList<String>
    lateinit var vehicleTwoViewModel: VehicleTwoViewModel
    private var file: File? = null
    private val date: Date = Calendar.getInstance().time

    private var captureMediaFile: File? = null
    private var img:String? = null
    private var pic:String? = null
    private val PERMISSION_CODE = 12
    private val CAMERA_REQUEST = 100
    private val GALLERY_PICTURE = 101
    private var isImageFitToScreen: Boolean = false
    private var bmp:Bitmap?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_two)
        vehicleprofile()
        init()
        initControl()
    }



    override fun init() {
        vehicleTwoViewModel = ViewModelProvider(this).get(VehicleTwoViewModel::class.java)


        vehicleTwoViewModel.getDriverDetailsApi(
            this, getSharedPreferences(
                Constants.BITEME,
                MODE_PRIVATE
            ).getString(Constants.TOKEN, "")!!
        )

        vehicleTwoViewModel.getdriverData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                vehicleType11.setText(it.data!!.vehicleType)
                editText.setText(it.data!!.vehicleLicence)
                manudateTv11111.setText(it.data!!.manufacturingDate)
                vehiclebrand1.setText(it.data!!.vehicleBrand)
                vehiclenumber.setText(it.data!!.vehicleNumber)
                vehiclemodel1.setText(it.data!!.vehicleModel)
                Glide.with(this).load(it.data!!.vehicleDocument).placeholder(R.drawable.mask_group_1).into(imageView20)
                img = it.data!!.vehicleDocument
                pic = it.data!!.vehicleDocument
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })


        vehicleTwoViewModel.updatedriverData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                showToast(it.message!!)
//                Utils().getMessageDialog(this, it.message!!, 0, false)
                vehicleprofile()
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        editTv.setOnClickListener(this)
        consManufacturing1.setOnClickListener(this)
        consName1.setOnClickListener(this)
        nextTv.setOnClickListener(this)
        imageView25.setOnClickListener(this)
        imageView20.setOnClickListener(this)

        reationSpinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                if (i != 0) {
                    vehicleType11.setText(genderList.get(i))
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.backIv -> {
                if (nextTv.visibility == View.VISIBLE) {
                    vehicleprofile()
                } else {
                    onBackPressed()
                }
            }
            R.id.editTv -> {
                vehicleeditprofile()
            }

            R.id.consManufacturing1 -> {
                dialogCalendar()
            }

            R.id.consName1 -> {
                GenderSpinner()
            }

            R.id.imageView20 -> {

                    startActivity(
                        Intent(this, MainActivity::class.java)
                            .putExtra("image", pic)
                    )
            }

            R.id.nextTv -> {
                if (validate()) {
                    if (file == null) {
                        vehicleTwoViewModel.updateVehicleDtailsApi(
                            this, getSharedPreferences(
                                Constants.BITEME,
                                MODE_PRIVATE
                            ).getString(Constants.TOKEN, "")!!,
                            vehicleType11.text.toString().trim(),
                            vehiclebrand1.text.toString().trim(),
                            vehiclemodel1.text.toString().trim(),
                            vehiclenumber.text.toString().trim(),
                            manudateTv11111.text.toString().trim(),
                            editText.text.toString().trim(), img!!
                        )
                    } else {
                        val pathList = ArrayList<String>()
                        pathList.add(file!!.absolutePath)
                        S3Bucket(this, pathList, true) { uploadedUrl ->
                            vehicleTwoViewModel.updateVehicleDtailsApi(
                                this, getSharedPreferences(
                                    Constants.BITEME,
                                    MODE_PRIVATE
                                ).getString(Constants.TOKEN, "")!!,
                                vehicleType11.text.toString().trim(),
                                vehiclebrand1.text.toString().trim(),
                                vehiclemodel1.text.toString().trim(),
                                vehiclenumber.text.toString().trim(),
                                manudateTv11111.text.toString().trim(),
                                editText.text.toString().trim(),
                                TextUtils.join(",", uploadedUrl!!)
                            )
                        }
                    }
                }
            }
            R.id.imageView25 -> {
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                            == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                            == PackageManager.PERMISSION_GRANTED)
                ) {
                    profileBottomLayout()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), PERMISSION_CODE
                        )
                    }
                }
            }
        }
    }


    private fun profileBottomLayout() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val sheetView: View = this.layoutInflater.inflate(R.layout.bottom_dialog, null)
        bottomSheetDialog.setContentView(sheetView)
        bottomSheetDialog.window!!.findViewById<View>(R.id.design_bottom_sheet).setBackgroundResource(
            android.R.color.transparent
        )
        val camIv = sheetView.findViewById<ImageView>(R.id.camIv)
        val gallIv = sheetView.findViewById<ImageView>(R.id.gallIv)
        val canTv = sheetView.findViewById<TextView>(R.id.canTv)
        gallIv.setOnClickListener {
            val intent = Intent()
            intent.setType("*/*")
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(intent, GALLERY_PICTURE)
            bottomSheetDialog.dismiss()
        }
        camIv.setOnClickListener {
            captureMediaFile = Utils().getOutputMediaFile(applicationContext)
            if (captureMediaFile != null) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)
                } else {
                    val photoUri = FileProvider.getUriForFile(
                        applicationContext,
                        applicationContext.packageName + ".provider",
                        captureMediaFile!!
                    )
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
                if (intent.resolveActivity(packageManager) != null) {
                    startActivityForResult(intent, CAMERA_REQUEST)
                } else {
                    Toast.makeText(this, "error no camera", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "file saving err", Toast.LENGTH_SHORT).show()
            }
            bottomSheetDialog.dismiss()
        }
        canTv.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.show()
    }

    private fun validate(): Boolean {
        if (vehicleType11.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please select Vehicle Type!", 0, false)
        }else if (editText.getText().toString().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Licence Number!", 0, false)
        } else if (manudateTv11111.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Manufacturing Date!", 0, false)
        } else if (vehiclebrand1.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Brand!", 0, false)
        } else if (vehiclenumber.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter Vehicle Number!", 0, false)
        } else if (vehiclemodel1.getText().toString().trim().isEmpty()) {
            Utils().getMessageDialog(this, "Please enter vehicle Model!", 0, false)
        } else {
            return true
        }
        return false
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    if (data != null) {
                         bmp = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                         pic = GetPath.getPath(this, Uri.parse(data.getDataString()))
                        Glide.with(this).load(bmp).into(imageView20)
                        setImage(bmp!!)
                    } else {
                        Utils().getMessageDialog(
                            this,
                            "Something Went Wrong.Please Try Again!",
                            0,
                            false
                        )
                    }
                } else {
                     bmp = BitmapFactory.decodeFile(captureMediaFile!!.getAbsolutePath())
                    Glide.with(this).load(bmp).into(imageView20)
                    pic = captureMediaFile!!.getAbsolutePath()
                    setImage(bmp!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            try {
                if (data != null) {
                    bmp = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                    Glide.with(this).load(bmp).into(imageView20)
                    setImage(bmp!!)
                    pic = GetPath.getPath(this, Uri.parse(data.getDataString()))
                } else {
                    Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
                }
            } catch (e: Exception) {
                Utils().getMessageDialog(this, "Something Went Wrong.Please Try Again!", 0, false)
            }
        }
        else {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                profileBottomLayout()
            } else if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) &&
                !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
            ) {
                goToSettings()
            } else {
                Utils().getMessageDialog(this, "This feature require these permissions!", 0, false)
            }
        }
    }

    private fun goToSettings() {
        val myAppSettings = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:$packageName")
        )
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(myAppSettings)
    }

    private fun setImage(bmp: Bitmap) {
        val thread = Thread {
            file = Utils().saveImageFile(this, bmp)
        }
        thread.run()
    }



    private fun vehicleeditprofile() {
        vehicleType11.setSelection(vehicleType11.getText().toString().trim().length)
        titleTv1.setText("Edit Vehicle Details")
        nextTv.visibility = View.VISIBLE
        editTv.visibility = View.GONE
        imageView25.visibility = View.VISIBLE
        vehicleType11.setEnabled(true)
        vehicleType11.setFocusableInTouchMode(true)
        editText.setEnabled(true)
        editText.setFocusableInTouchMode(true)
        manudateTv11111.setEnabled(true)
        manudateTv11111.setFocusableInTouchMode(true)
        vehiclebrand1.setEnabled(true)
        vehiclebrand1.setFocusableInTouchMode(true)
        vehiclenumber.setEnabled(true)
        vehiclenumber.setFocusableInTouchMode(true)
        vehiclemodel1.setEnabled(true)
        vehiclemodel1.setFocusableInTouchMode(true)
        consManufacturing1.isEnabled = true
    }


    private fun vehicleprofile() {
        titleTv1.setText("Vehicle Details")
        nextTv.visibility = View.GONE
        editTv.visibility = View.VISIBLE
        imageView25.visibility = View.GONE
        vehicleType11.setEnabled(false)
        editText.setEnabled(false)
        manudateTv11111.setEnabled(false)
        vehiclebrand1.setEnabled(false)
        vehiclenumber.setEnabled(false)
        vehiclemodel1.setEnabled(false)
        consManufacturing1.isEnabled = false
    }


    fun dialogCalendar(){
        val dialog = DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, setFormat("yyyy",date), setFormat("MM",date) - 1, setFormat("dd",date))
        dialog.datePicker.maxDate
        dialog.show()
    }
    private fun setFormat(key:String, date:Date): Int{
        return SimpleDateFormat(key).format(date).toInt()
    }
    private fun GenderSpinner() {
        genderList = ArrayList<String>()
        genderList.add("Please select")
        genderList.add("Motorcycle")
        genderList.add("Light Vehicle")
        val reasonAdapter: ArrayAdapter<*> = object : ArrayAdapter<Any?>(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            genderList as List<Any?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val textView = super.getView(position, convertView, parent) as TextView
                textView.setTextColor(Color.TRANSPARENT)
                return textView
            }
        }
        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        reationSpinner3.setAdapter(reasonAdapter)
        reationSpinner3.performClick()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = simpleDateFormat.parse(year.toString() + "-" + (month + 1) + "-" + dayOfMonth)
            val newDate : String= SimpleDateFormat("dd-MM-yyyy").format(date)
            manudateTv11111.setText(newDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

    }
}