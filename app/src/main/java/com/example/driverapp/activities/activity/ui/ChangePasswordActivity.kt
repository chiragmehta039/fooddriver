package com.example.driverapp.activities.activity.ui

import Utils
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.ui.forgotPassword.ForgotPasswordViewModel
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.ui.resetPassword.ResetPasswordViewModel
import com.example.driverapp.activities.activity.utils.Constants
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.activity_reset_password.backIv
import kotlinx.android.synthetic.main.activity_reset_password.confirmPasswordd
import kotlinx.android.synthetic.main.activity_reset_password.nextTv
import kotlinx.android.synthetic.main.activity_reset_password.passwordd
import kotlinx.android.synthetic.main.activity_reset_password.titleTv
import kotlinx.android.synthetic.main.activity_reset_password1.*

class ChangePasswordActivity : BaseActivity(), View.OnClickListener{
    lateinit var resetPasswordViewModel: ResetPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password1)
        init()
        initControl()

    }
    override fun init() {
        resetPasswordViewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)

        resetPasswordViewModel.changepasswordData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Toast.makeText(this,it.message!!,Toast.LENGTH_LONG).show()
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        backIv.setOnClickListener(this)
        nextTv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backIv -> {
                onBackPressed()
            }
            R.id.nextTv -> {
                if (confirmPasswordd.text.toString().trim().isEmpty() || passwordd.text.toString().trim().isEmpty() || passworddd.text.toString().trim().isEmpty()) {
                    Utils().getMessageDialog(this, "Please fill all details!", 1, false)
                } else if (passwordd.text.toString().length < 6)
                    Utils().getMessageDialog(this, "Please enter atleast 6 characters in Old Password!", 1, false)
                else if (passwordd.text.toString().length > 16)
                    Utils().getMessageDialog(this, "Password Maximum length should not be more than 16!!", 1, false)
                else if (passworddd.text.toString().length < 6)
                    Utils().getMessageDialog(this, "Please enter atleast 6 characters in New Password!", 1, false)
                else if (passworddd.text.toString().length > 16)
                    Utils().getMessageDialog(this, "Password Maximum length should not be more than 16!!", 1, false)
                else if (passworddd.text.toString().trim() != confirmPasswordd.text.toString().trim()) {
                    Utils().getMessageDialog(this, "New Password and Confirm password must be same!", 1, false)
                } else {
               resetPasswordViewModel.driverChangePasswordApi(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN,"")!!,passwordd.text.toString().trim(),confirmPasswordd.text.toString().trim())
                }
            }
        }
    }

}