package com.example.driverapp.activities.activity.ui.map

import Utils
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.driverapp.activities.activity.models.response.DriverLoginDataModel
import com.example.driverapp.activities.activity.models.response.OrderDetailResponse
import com.example.driverapp.activities.activity.models.response.UpdateOderStatusResonse
import com.example.driverapp.activities.activity.utils.ErrorUtil
import com.quiz.quizlok.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MapViewModel : BaseViewModel() {

    var loginData = MutableLiveData<DriverLoginDataModel>()
    var orderdetail = MutableLiveData<OrderDetailResponse>()
    var orderstatusresponse=MutableLiveData<UpdateOderStatusResonse>()


    fun loginApi(context: Context, password: String, countryCode: String, mobileNumber: String, deviceToken: String,deviceType: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.driverLogin(password, countryCode, mobileNumber, deviceToken, deviceType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    loginData.value = it
                }, {
                   Utils().dismissDialog(progressDialog)
                           ErrorUtil.handlerGeneralError(context, it)
                })
    }

    fun getOrderStatusAPi(context: Context,driverid:String,orderid:String,staus:String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.updateOrderSatus(driverid,orderid,staus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderstatusresponse.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }




    fun getOrderDetail(context: Context, driverId: String, orderId: String) {
        val progressDialog = Utils().getProgressDialog(context)
        apiInterface.getOrderDetail(driverId, orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Utils().dismissDialog(progressDialog)
                    orderdetail.value = it
                }, {
                    Utils().dismissDialog(progressDialog)
                    ErrorUtil.handlerGeneralError(context, it)
                })
    }
}