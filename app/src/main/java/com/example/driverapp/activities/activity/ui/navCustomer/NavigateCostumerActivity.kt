package com.example.driverapp.activities.activity.ui.navCustomer

import Utils
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.models.response.OrderDetailResponse
import com.example.driverapp.activities.activity.models.response.OrderListResponse
import com.example.driverapp.activities.activity.ui.adapter.HomeAdapter
import com.example.driverapp.activities.activity.ui.adapter.NavigateAdapter
import com.example.driverapp.activities.activity.ui.home.HomeActivity
import com.example.driverapp.activities.activity.ui.map.MapViewModel
import com.example.driverapp.activities.activity.ui.navigateClose.NavigateCloseActivity
import com.example.driverapp.activities.activity.utils.Constants
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_navigate_costumer.*

class NavigateCostumerActivity : BaseActivity(), View.OnClickListener   {
    var list:ArrayList<String>?=null
    lateinit var viewModel: MapViewModel
    var orderid:String=""
    var lists= java.util.ArrayList<OrderDetailResponse.Data.OrderData.OrderDataX>()

    private var broadcastObserver : BroadcastReceiver?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigate_costumer)
        nextTv.setEnabled(false)
        if(intent.hasExtra("orderid"))
        {
            orderid=  intent.getStringExtra("orderid").toString()!!
            viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
            viewModel.getOrderDetail(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,orderid)
            getDetailResponse()

        }

        init()
        initControl()

        broadcastObserver= object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.hasExtra("id")!!)
                {
                      showToast(intent.getStringExtra("id"))
                    viewModel.getOrderDetail(this@NavigateCostumerActivity,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,intent.getStringExtra("id").toString())

                }
                // api
            }

        }


    }


    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastObserver, IntentFilter("COM.DATA"))
    }

    fun getDetailResponse()
    {
        viewModel.orderdetail.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                tv_name.setText(it.data.userData.firstName+" "+it.data.userData.lastName)
                tv_add.setText(it.data.orderData.address)
                lists.clear()
                lists.addAll(it.data.orderData.orderData)
                if(it.data.orderData.status.equals("Delivered"))
                {
                    startActivity(Intent(this,HomeActivity::class.java))
                }
               else if(it.data.orderData.status.equals("Order Ready for pickup"))
                {
                    nextTv.setEnabled(true)

                }
                customerList(it.data.orderData.paymentMode)


            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

    }



    override fun init() {
        customerList("")


    }

    override fun initControl() {
        nextTv.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.nextTv->{
                startActivity(Intent(this,NavigateCloseActivity::class.java)
                    .putExtra("orderid", intent.getStringExtra("orderid").toString()!!)
                    .putExtra("lat", intent.getStringExtra("lat").toString()!!)
                    .putExtra("long", intent.getStringExtra("long").toString()!!)
                    .putExtra("title",intent.getStringExtra("title").toString()!!)
                )
            }



        }
    }


    fun customerList(id:String){

        navigateRecVw.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        navigateRecVw.adapter = NavigateAdapter(this!!,lists,id)
    }

    override fun onDestroy() {
        super.onDestroy()

        unregisterReceiver(broadcastObserver)
    }



}