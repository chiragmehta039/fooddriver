package com.example.driverapp.activities.activity.ui.home

import Utils
import android.Manifest
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.constraintlayout.motion.widget.Debug.getLocation
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.driverapp.R
import com.example.driverapp.activities.activity.base.BaseActivity
import com.example.driverapp.activities.activity.models.response.OrderListResponse
import com.example.driverapp.activities.activity.ui.about.AboutUsActivity
import com.example.driverapp.activities.activity.ui.adapter.HomeAdapter
import com.example.driverapp.activities.activity.ui.contact.ContactUsActivity
import com.example.driverapp.activities.activity.ui.login.LoginActivity
import com.example.driverapp.activities.activity.ui.map.MapActivity
import com.example.driverapp.activities.activity.ui.notification.NotificationActivity1
import com.example.driverapp.activities.activity.ui.performance.MyPerformanceActivity
import com.example.driverapp.activities.activity.ui.profile.GPSTracker
import com.example.driverapp.activities.activity.ui.profile.ProfileTwoActivity
import com.example.driverapp.activities.activity.ui.settings.SettingsActivity
import com.example.driverapp.activities.activity.ui.termsCondition.TermsAndConditionsActivity
import com.example.driverapp.activities.activity.ui.vehicleTwo.VehicleTwoActivity
import com.example.driverapp.activities.activity.utils.Constants
import com.example.driverapp.activities.activity.utils.Constants.Companion.BITEME
import com.example.driverapp.activities.activity.utils.Constants.Companion.DELAY_FOR_NEXT_SCREEN
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.dialog_burgerking.*
import kotlinx.android.synthetic.main.dialog_reject.*
import java.util.*

class HomeActivity : BaseActivity() , View.OnClickListener, HomeAdapter.FilterSelection{
    private lateinit var genderList: ArrayList<String>
    var dialog:Dialog?=null
    var list:ArrayList<String>?=null
    var lists=ArrayList<OrderListResponse.Data>()
    var titles=""

    var idd=""
    var names=""
    var distances=""
    private var broadcastObserver : BroadcastReceiver?=null


    var clickCheck=""
    lateinit var homeViewModel: HomeViewModel
    private var notifyStatus = ""
    private val REQUEST_LOCATION = 100
    private lateinit var locationClass: GPSTracker
    private var backButtonCount = 0
    var EXIT_MSG = "Press the back button once again to close the application."
    private val REQUEST_CHECK_SETTINGS = 101
    var adapter :HomeAdapter ? = null

    var type:String="active"
    var types:String="accept"
    var orderid:String=""


    var ids:String=""

    private var Lats =""
    private var Longs=""

    private val uploadedListener: UploadedListener? = null

    interface UploadedListener {
        fun setimage(image: ImageView)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        init()
        initControl()
        setImage()
        getDetailResponse()

        broadcastObserver= object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.hasExtra("id")!!)
                {
                    showToast(intent.getStringExtra("id"))
                    homeViewModel.getOrderList(this@HomeActivity, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")


                }
                // api
            }

        }




        if(intent.hasExtra("close"))
{
    setLayout(R.id.past)
}
        else if(intent.hasExtra("ready"))
        {
            setLayout(R.id.active)
            adapter=  lists?.let { HomeAdapter(this!!, type, it, this,1) }
            homeRecView.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            homeRecView.adapter = adapter!!

        }

        //   dialogBurgerking()


        broadcastObserver= object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.hasExtra("id")!!)
                {
                    homeViewModel.getOrderList(this@HomeActivity, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")

                }
                // api
            }

        }





    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastObserver, IntentFilter("COM.DATA"))

    }





fun userDetailResponse()
{
    homeViewModel.getdriverData.observe(this, androidx.lifecycle.Observer {
        if (it!!.status == 200) {


//            Glide.with(this).load(it.data!!.profilePic).placeholder(R.drawable.profilebig).into(imageView10)
//            tv_namess.setText(it.data!!.firstName)
//            tv_namee.setText("@" + it.data!!.username)
            ids = it.data!!.id.toString()

            //       img = it.data!!.profilePic

        } else {
            Utils().getMessageDialog(this, it.message!!, 0, false)
        }
    })

}
    fun userDataResponse()
    {
        homeViewModel.orderstatusresponse.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                if(titles.equals("Delivered"))
                {
                    setLayout(R.id.past)
                }

//                homeViewModel.getOrderList(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,"Running")


                //       img = it.data!!.profilePic

            } else {
//                homeViewModel.getOrderList(this,getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!,"Running")

            }
        })

        homeViewModel.mError.observe(this, androidx.lifecycle.Observer {
            lists?.clear()



            adapter = lists?.let { HomeAdapter(this!!, type, it, this,0) }
            homeRecView.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            homeRecView.adapter = adapter!!
            //Handle the error accordingly
        })

        homeViewModel.mError.observe(this, androidx.lifecycle.Observer {

            homeViewModel.getOrderList(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")

        })

    }


    fun userListResposne()
    {
        homeViewModel.getOrderListData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {

                if (it.data.size > 0) {
                    textView.visibility = View.GONE
                    homeRecView.visibility = View.VISIBLE
                    lists?.clear()

                    lists?.addAll(it.data)

                    adapter = lists?.let { HomeAdapter(this!!, type, it, this,0) }
                    homeRecView.layoutManager =
                            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

                    homeRecView.adapter = adapter!!

                } else {
                    textView.visibility = View.VISIBLE
                    homeRecView.visibility = View.GONE
                    lists?.clear()


                    adapter = lists?.let { HomeAdapter(this!!, type, it, this!!,0) }
                    homeRecView.layoutManager =
                            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

                    homeRecView.adapter = adapter!!
                }


                //       img = it.data!!.profilePic

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
        homeViewModel.mError.observe(this, androidx.lifecycle.Observer {
            lists?.clear()



            adapter = lists?.let { HomeAdapter(this!!, type, it, this,0) }
            homeRecView.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            homeRecView.adapter = adapter!!

        })

    }
    fun setImage() {
        Glide.with(this).load(getSharedPreferences(BITEME, MODE_PRIVATE).getString(Constants.IMAGE, "")).placeholder(R.drawable.profile).into(imageView10)
        uploadedListener?.setimage(imageView10)
    }


    fun checkLocationPermission(): Boolean {
        var ret = true
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED) {
            ret = false
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION
            )
        }
        return ret
    }

    override fun onResume() {
        super.onResume()
        if (getSharedPreferences(BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!.isEmpty()) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }


        homeViewModel.getDriverDetailsApi(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.TOKEN, "")!!)
        userDetailResponse()

     homeViewModel.getOrderList(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")
        homeViewModel.getDriverNotificationCountApi(
                this, getSharedPreferences(
                Constants.BITEME,
                MODE_PRIVATE
        ).getString(Constants.TOKEN, "")!!)
        userDataResponse()
        userListResposne()


        if (checkLocationPermission()) {
            locationClass = GPSTracker(this)
            homeViewModel.updateLocationApi(
                    this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(
                    Constants.TOKEN,
                    ""
            )!!, locationClass.latitude.toString() + "", locationClass.longitude.toString() + ""
            )

//            )
        }
    }

    override fun init() {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)


        notifyStatus = getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(
                Constants.DUTY_STATUS,
                ""
        )!!

        if (notifyStatus.equals("false", ignoreCase = true)) {
            togBtn1.setChecked(false)
        } else {
            togBtn1.setChecked(true)
        }

        if(intent.getStringExtra("orderId")!=null)
        {
            var or:String=intent.getStringExtra("orderId").toString()!!
            var rname:String=intent.getStringExtra("restaurantName").toString()!!

            var distance:String=intent.getStringExtra("distance").toString()!!
            idd=or
            names=rname
            distances=distance
            homeViewModel.getOrderDetail(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, or)


         //   homeViewModel.getOrderDetail(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, intent.getStringExtra("orderId").toString()!!)

        //    dialogBurgerking(or, rname, distance)

        }
        else
        {

        }

//        if(intent.getBooleanExtra("close", false)){
//            textView.visibility=View.GONE
//            homeRecView.visibility=View.VISIBLE
//            setLayout(R.id.past)
//        }else {
//            Handler(Looper.getMainLooper()).postDelayed({
//                dialogBurgerking()
//            }, DELAY_FOR_NEXT_SCREEN)
//        }

        homeViewModel.orderRejectData

        homeViewModel.notifyData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                if (it.data != null && !it.data!!.equals("0", true)) {
                    textView2!!.text = it!!.data
                } else {
                    textView2!!.text = "0"

                }

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })


        homeViewModel.dutystatusData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })


        homeViewModel.logoutData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                val intent1 = Intent(this, LoginActivity::class.java)
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent1)
                getSharedPreferences(BITEME, MODE_PRIVATE).edit().putString(Constants.TOKEN, "")
                        .apply()

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

        homeViewModel.locationData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
        homeViewModel.getOrderListsData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                startActivity(Intent(this, MapActivity::class.java)
                        .putExtra("orderid", it.data.get(0)._id)
                        .putExtra("lat", it.data.get(0).latitude.toString())
                        .putExtra("long", it.data.get(0).longitude.toString())
                )

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

        homeViewModel.orderACeeptData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                if (types.equals("active")) {
                    homeViewModel.getOrderLists(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")

                } else {

                }

                Utils().getMessageDialog(this, it.message!!, 0, false)

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

        homeViewModel.orderRejectData.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Utils().getMessageDialog(this, it.message!!, 0, false)

            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })
    }

    override fun initControl() {
        drawerIv.setOnClickListener(this)
        closeIv.setOnClickListener(this)
        profile.setOnClickListener(this)
        myPerformance.setOnClickListener(this)
        logout1.setOnClickListener(this)
        about.setOnClickListener(this)
        help.setOnClickListener(this)
        active.setOnClickListener(this)
        past.setOnClickListener(this)
        rejected.setOnClickListener(this)
        contact.setOnClickListener(this)
        passwordd.setOnClickListener(this)
        vehicleDetail.setOnClickListener(this)
        notify.setOnClickListener(this)

        togBtn1.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(
                        Constants.DUTY_STATUS,
                        "true"
                ).apply()
                homeViewModel.updateDutyStatusApi(
                        this, getSharedPreferences(
                        Constants.BITEME,
                        MODE_PRIVATE
                ).getString(Constants.TOKEN, "")!!, "true"
                )
            } else {
                getSharedPreferences(Constants.BITEME, MODE_PRIVATE).edit().putString(
                        Constants.DUTY_STATUS,
                        "false"
                ).apply()
                homeViewModel.updateDutyStatusApi(
                        this, getSharedPreferences(
                        Constants.BITEME,
                        MODE_PRIVATE
                ).getString(Constants.TOKEN, "")!!, "false"
                )
            }
        })


    }

    override fun onClick(v: View?) {

        when(v?.id){

            R.id.drawerIv -> {
                checkDrawerOpen()
            }

            R.id.notify -> {
                startActivity(Intent(this, NotificationActivity1::class.java))
            }

            R.id.closeIv -> {
                closeDrawer()
            }
            R.id.profile -> {
                startActivity(Intent(this, ProfileTwoActivity::class.java))
            }
            R.id.myPerformance -> {
                startActivity(Intent(this, MyPerformanceActivity::class.java))

            }
            R.id.logout1 -> {
                val dialog = Utils().getMessageDialogWithOption(
                        this,
                        "Are you sure you want to logout?"
                )
                val yesTv = dialog!!.findViewById<TextView>(R.id.yesTv)
                yesTv.setOnClickListener {
                    dialog.dismiss()
                    homeViewModel.driverLogoutApi(
                            this, getSharedPreferences(
                            Constants.BITEME,
                            MODE_PRIVATE
                    ).getString(Constants.TOKEN, "")!!
                    )
                }
            }
            R.id.about -> {
                startActivity(Intent(this, AboutUsActivity::class.java))
            }
            R.id.help -> {
                startActivity(Intent(this, TermsAndConditionsActivity::class.java))
            }
            R.id.contact -> {
                startActivity(Intent(this, ContactUsActivity::class.java))
            }
            R.id.passwordd -> {
                startActivity(Intent(this, SettingsActivity::class.java))
            }
            R.id.vehicleDetail -> {
                startActivity(Intent(this, VehicleTwoActivity::class.java))
            }
            R.id.active -> {
                setLayout(R.id.active)
            }
            R.id.past -> {
                setLayout(R.id.past)
            }
            R.id.rejected -> {
                setLayout(R.id.rejected)
            }
        }
    }
    fun checkDrawerOpen() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            (drawerLayout.closeDrawer(GravityCompat.START))

        } else {
            (drawerLayout.openDrawer(GravityCompat.START))
        }
    }

    fun closeDrawer(){
        (drawerLayout.closeDrawer(GravityCompat.START))



    }


    private fun setLayout(id: Int) {
        active!!.setTextColor(ContextCompat.getColor(this, R.color.colorBlack))
        past!!.setTextColor(ContextCompat.getColor(this, R.color.colorBlack))
        rejected!!.setTextColor(ContextCompat.getColor(this, R.color.colorBlack))
        active!!.background = ContextCompat.getDrawable(this, R.drawable.greystrokerectagle)
        past!!.background = ContextCompat.getDrawable(this, R.drawable.greystrokerectagle)
        rejected!!.background = ContextCompat.getDrawable(this, R.drawable.greystrokerectagle)
        if (id == R.id.active) {
            active!!.background = ContextCompat.getDrawable(this, R.drawable.pinkstrokerectagle)
            active!!.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            clickCheck="active"
            type="active"
            lists.clear()
            lists?.clear()



            adapter=  lists?.let { HomeAdapter(this!!, type, it, this,0) }
            homeRecView.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            homeRecView.adapter = adapter!!
           homeViewModel.getOrderList(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Running")



        }else if (id == R.id.past) {
            past!!.background = ContextCompat.getDrawable(this, R.drawable.pinkstrokerectagle)
            past!!.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            clickCheck="past"
            type="past"
            homeViewModel.getOrderList(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Delivered")

        } else if (id == R.id.rejected) {
            rejected!!.background = ContextCompat.getDrawable(this, R.drawable.pinkstrokerectagle)
            rejected!!.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            clickCheck="rejected"
            type="rejected"


            homeViewModel.getOrderList(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, "Rejected")

        }


    }
    fun getDetailResponse() {
        homeViewModel.orderdetail.observe(this, androidx.lifecycle.Observer {
            if (it!!.status == 200) {
                Lats = it.data.orderData.driverLat
                Longs = it.data.orderData.driverLong

                if(it.data.orderData.driverAssign)
                {
                    Utils().getMessageDialog(this, "The order has already been accepted by some other driver", 0, false)

                }
                else
                {
                    dialogBurgerking(idd,names,distances)

                }


            } else {
                Utils().getMessageDialog(this, it.message!!, 0, false)
            }
        })

    }



    fun dialogBurgerking(id: String, name: String, distance: String) {
        orderid=id
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.dialog_burgerking)
        dialog!!.window!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        )
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val tv_res = dialog!!.findViewById<TextView>(R.id.tv_res)
        val tv_km=dialog!!.findViewById<TextView>(R.id.tv_km)
        tv_res.setText(name)
        var someValue = distance
        var doubleVal = someValue.toDouble()
        var data=doubleVal.toInt()
        tv_km.setText(data.toString() + " " + "km")




        dialog!!.acceptTv.setOnClickListener {
             types="active"

            textView.visibility=View.GONE
            homeRecView.visibility=View.VISIBLE
            homeViewModel.getOrderAccept(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, locationClass.latitude.toString() + "", locationClass.longitude.toString(), orderId = id)


//            setLayout(R.id.active)
            dialog!!.dismiss()
        }
        dialog!!.rejectTv.setOnClickListener {
            types="reject"

            homeViewModel.getOrderReject(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, orderId = id)

            showDialog2()
            dialog!!.dismiss()
        }
    }

    private fun showDialog2() {
        val dialog = Dialog(this, android.R.style.Theme_Black)
        dialog.window!!.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
        )
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_reject)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        dialog.fullServiceTv.setOnClickListener {
            dialog.dismiss()
            Handler(Looper.getMainLooper()).postDelayed({
//                dialogBurgerking()
            }, DELAY_FOR_NEXT_SCREEN)
        }
        dialog.selectReason.setOnClickListener {
            GenderSpinner(dialog.reationSpinner2)
        }

        dialog.reationSpinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
            }
            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        dialog.show()
    }

    private fun GenderSpinner(relation: Spinner) {
        genderList = java.util.ArrayList<String>()
        genderList.add("Reason 1")
        genderList.add("Reason 2")
        val reasonAdapter: ArrayAdapter<*> = object : ArrayAdapter<Any?>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                genderList as List<Any?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val textView = super.getView(position, convertView, parent) as TextView
                textView.setTextColor(Color.TRANSPARENT)
                return textView
            }
        }
        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        relation.setAdapter(reasonAdapter)
        relation.performClick()
    }

    override fun onBackPressed() {
        if (backButtonCount >= 1) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            Toast.makeText(this, EXIT_MSG, Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({
                backButtonCount = 0
            }, 2000)
            backButtonCount++
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (REQUEST_CHECK_SETTINGS == requestCode) {
            if (RESULT_OK == resultCode) {
                getLocation()
            } else if (RESULT_CANCELED == resultCode) {
                Toast.makeText(this, "Please enable gps!", Toast.LENGTH_SHORT).show()
                if (locationClass.location != null) {
                    if (checkLocationPermission()) {
                        getLocation()
                    }
                }
            } else {
            }
        }
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String?>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION) {
            var isPermissionDenied = false
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    isPermissionDenied = true
                }
            }
            if (isPermissionDenied) {
                Toast.makeText(this, "You need to allow the permission", Toast.LENGTH_SHORT).show()
            } else {
                enableGps()
            }
        }
    }


    protected fun enableGps() {
        val locationRequest = LocationRequest.create().setInterval(10000).setFastestInterval((10000 / 2).toLong()).setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY
        )
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        LocationServices.getSettingsClient(this)
            .checkLocationSettings(builder.build())
            .addOnSuccessListener(this) { response: LocationSettingsResponse? -> getLocation() }
            .addOnFailureListener(this) { ex: Exception? ->
                if (ex is ResolvableApiException) {
                    try {
                        ex.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                    } catch (sendEx: IntentSender.SendIntentException) {
                    }
                }
            }
    }



    override fun onPostionClick(title: String, id: String, lat: String, long: String) {
        if(title.equals("On the Way"))
        {
            startActivity(Intent(this, MapActivity::class.java)
                    .putExtra("orderid", id)
                    .putExtra("lat", lat)
                    .putExtra("long", long))


        }
        else
        {
            if(!title.isEmpty())
            {
                titles=title
                homeViewModel.getOrderStatusAPi(this, getSharedPreferences(Constants.BITEME, MODE_PRIVATE).getString(Constants.IDS, "")!!, id, title)

            }
        }    }

}